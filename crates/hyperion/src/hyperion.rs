// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::ENOENT;
use common::constants::ESRCH;
use common::constants::OVERFLOW_ERR_CODE;
use common::constants::S_IRGRP;
use common::constants::S_IROTH;
use common::constants::S_IRUSR;
use common::constants::S_IWUSR;
use common::error::ExitCode;
use common::error;
use common::fs::CreateFile;
use common::fs::LockOperation;
use common::ret;
use common::stderrln;
use common::sys::getuid;
use common::time::HumanTime;

use crate::Options;

use std::fmt;
use std::fs;
use std::io::Write;
use std::io;
use std::os::fd::AsFd;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;
use std::process;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;
use std::thread;
use std::time;

/// Custom type for specifying log type.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
enum LogLevel {
	/// An error occurred.
	Error,

	/// Generic / General / Debug information.
	Notice,
}

impl fmt::Display for LogLevel {
	#[inline]
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		String::from(match self { LogLevel::Error => "ERROR", LogLevel::Notice => "NOTICE" }).fmt(f)
	}
}

/// Write program status into the log [`File`] or to the standard error stream
/// if an error is encountered.
///
/// [`File`]: std::fs::File
macro_rules! log {
	($logfile: expr, $loglevel: expr, $($info: tt)*) => {
		if $logfile.write_all(format! { "[{}] [{}] {}\n", HumanTime::now().to_utc_string(), $loglevel, format_args! { $($info)* } }.as_bytes()).is_err() {
			stderrln! { "[{}] [{}] {}", HumanTime::now().to_utc_string(), $loglevel, format_args! { $($info)* } };
		}
	};
}

// Validate the input arguments of `change_oom_score()`.
fn input_validation(pids: Vec<String>, hyperion: Options) -> common::Result<(Vec<String>, i16)> {
	if pids.is_empty() && !hyperion.all {
		ret! { ExitCode::Failure, "hyperion: missing PID operand" };
	}

	let oom_score = hyperion.oom_score.ok_or_else(|| error! {
		ExitCode::Failure, "hyperion: missing OOM score operand"
	})?;

	if oom_score.is_negative() && getuid() != 0 {
		ret! { ExitCode::Failure, "hyperion: cannot set negative OOM scores as a non-root user" };
	}

	for pid in &pids {
		if let Err(error) = pid.parse::<u32>() {
			ret! { ExitCode::Failure, "hyperion: failed to parse PID '{}': {}", pid, error };
		}
	}

	if hyperion.service.is_some() && hyperion.delay.unwrap_or(1000) < 1000 {
		stderrln! { "hyperion: `--delay` values lower than 1000ms may result in unusually high CPU usage" };
	}

	Ok((pids, oom_score))
}

// Fetch the PIDs of all running processes.
fn get_all_procs() -> io::Result<Vec<String>> {
	let mut process_list = Vec::with_capacity(1024);
	
	// Skip non-process directories.
	for dirent in fs::read_dir("/proc")? {
		if let Ok(pid) = dirent?.file_name().to_string_lossy().parse::<u32>() {
			process_list.push(format! { "{}", pid });
		}
	}

	if process_list.is_empty() {
		return Err(io::Error::from_raw_os_error(ESRCH));
	}

	Ok(process_list)
}

// Modify the OOM scores of all required processes.
fn modify_procs<L: AsFd + Write>(mut logfile: L, hyperion: Options, mut process_list: Vec<String>, oom_score: i16) -> common::Result<()> {
	if hyperion.all {
		process_list.extend(get_all_procs().map_err(|error| error! {
			ExitCode::Failure, "hyperion: failed to fetch process statistics from '/proc': {}", error
		})?);
	}

	let mut updated: u64 = 0;

	for pid in &process_list {
		let oomfile = &Path::new("/proc").join(pid).join("oom_score_adj");

		match fs::File::options().write(true).create(false).truncate(true).open(oomfile) {
			Err(error) => log! {
				logfile, LogLevel::Error, "Failed to open '{}' - {}", oomfile.display(), error
			},
			Ok(mut fildes) => match fildes.write_all(format! { "{}", oom_score }.as_bytes()) {
				Err(error) => log! {
					logfile, LogLevel::Error, "Failed to update '{}' to '{}' - {}", oomfile.display(), oom_score, error
				},
				Ok(()) => updated = updated.checked_add(1).ok_or_else(|| error! {
					OVERFLOW_ERR_CODE, "hyperion: fatal error: Integer overflow occured while counting processes"
				})?,
			},
		}
	}

	log! { logfile, LogLevel::Notice, "Updated ({} of {}) processes", updated, process_list.len() };
	Ok(())
}

// Create service directory and required files during startup.
fn start_service(directory: &Path, oom_score: i16) -> common::Result<(fs::File, fs::File)> {
	if let Err(error) = fs::DirBuilder::new().recursive(true).create(directory) {
		ret! { ExitCode::Failure, "hyperion: failed to create service directory '{}': {}", directory.display(), error };
	}

	if let Err(error) = fs::remove_file(directory.join("hyperion.exit")) {
		if error.raw_os_error() != Some(ENOENT) {
			ret! { ExitCode::Failure, "hyperion: failed to remove '{}': {}", directory.join("hyperion.exit").display(), error };
		}
	}

	let (uid, logfile, pidfile) = (getuid(), &directory.join("hyperion.log"), &directory.join("hyperion.pid"));

	// Create PID file.
	let mut pidfildes = CreateFile::new().fchown(Some(uid), Some(uid)).flock(LockOperation::ExclusiveNonBlocking).fchmod(fs::Permissions::from_mode(S_IRUSR | S_IWUSR)).create(pidfile).map_err(|error| error! {
		ExitCode::Failure, "hyperion: failed to create PID file '{}': {}", pidfile.display(), error
	})?;

	// Write process id into locked PID file.
	if let Err(error) = pidfildes.write_all(format! { "{}", process::id() }.as_bytes()) {
		ret! { ExitCode::Failure, "hyperion: failed to write process ID into '{}': {}", pidfile.display(), error };
	}

	// Create log file.
	let mut logfildes = CreateFile::new().fchown(Some(uid), Some(uid)).flock(LockOperation::ExclusiveNonBlocking).fchmod(fs::Permissions::from_mode(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)).create(logfile).map_err(|error| error! {
		ExitCode::Failure, "hyperion: failed to create log file '{}': {}", pidfile.display(), error
	})?;

	// Write program version into locked log file.
	log! { logfildes, LogLevel::Notice, "hyperion {}", env! { "CARGO_PKG_VERSION" } };

	// Write program input parameters into log file.
	log! { logfildes, LogLevel::Notice, "OOM_SCORE: {}, PID: {}, UID: {}", oom_score, process::id(), uid };

	// Write service directory into log file.
	log! { logfildes, LogLevel::Notice, "Service path: '{}'", directory.display() };

	Ok((pidfildes, logfildes))
}

#[doc(hidden)]
#[inline]
pub fn change_oom_score(pids: Vec<String>, hyperion: Options) -> common::Result<()> {
	let (pids, oom_score) = input_validation(pids, hyperion)?;

	match hyperion.service {
		Some(directory) => {
			let ((mut _pidfile, mut logfile), main_thread, exitfile) = (start_service(directory, oom_score)?, thread::current(), directory.join("hyperion.exit"));

			// Check for 'hyperion.exit'.
			static EXIT_CHECK: AtomicBool = AtomicBool::new(false);

			let service = thread::Builder::new().spawn(move || loop {
				match fs::symlink_metadata(&exitfile) {
					Err(_) => {
						thread::sleep(time::Duration::from_millis(1000));
						continue;
					},
					Ok(_) => {
						EXIT_CHECK.store(true, Ordering::Relaxed);
						main_thread.unpark();
						break;
					},
				}
			}).map_err(|error| error! {
				ExitCode::Failure, "hyperion: failed to spawn service thread: {}", error
			})?;

			while !EXIT_CHECK.load(Ordering::Relaxed) {
				modify_procs(&mut logfile, hyperion, pids.clone(), oom_score)?;
				thread::park_timeout(time::Duration::from_millis(hyperion.delay.unwrap_or(1000)));
			}

			// Log exit file detection.
			log! { logfile, LogLevel::Notice, "Detected '{}'", directory.join("hyperion.exit").display() };

			// Write exit notice to log file.
			log! { logfile, LogLevel::Notice, "Exiting..." };

			if let Err(error) = service.join() {
				ret! { ExitCode::Failure, "hyperion: failed to join service thread: {:?}", error };
			}
		},
		None => {
			let mut logfile = io::stderr();

			// Write program version to stderr.
			log! { logfile, LogLevel::Notice, "hyperion {}", env! { "CARGO_PKG_VERSION" } };

			// Write program input parameters to stderr.
			log! { logfile, LogLevel::Notice, "OOM_SCORE: {}, PID: {}, UID: {}", oom_score, process::id(), getuid() };

			// Show standard mode notice.
			log! { logfile, LogLevel::Notice, "Running in standard mode" };

			modify_procs(logfile, hyperion, pids, oom_score)?;
		},
	}

	Ok(())
}
