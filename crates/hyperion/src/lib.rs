// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod hyperion;

use common::cli::getopt;
use common::cli::HasArg;
use common::cli::version;
use common::error::ExitCode;
use common::error;
use common::ret;
use common::stdoutln;

use hyperion::change_oom_score;

use std::ffi::OsString;
use std::mem;
use std::path::Path;

#[doc(hidden)]
pub const HELP: &str =
"hyperion - Modify the OOM killer score of the specified PIDs.

Usage:
    hyperion OPTION... PID...

Options:
    -a, --all
          Update the OOM killer score of all available processes.

    -d, --delay <TIME>
          Specify the TIME delay in milliseconds, between each attempt to modify
          the OOM killer score of the specified PIDs. This option is ignored, if
          `hyperion` is not running in service mode.

    -h, --help
          Display this help message and exit.

    -o, --oom-score <SCORE>
          Change the OOM killer score of the specified PID to SCORE, where SCORE
          is an integer between -1000 and 1000.

    -s, --service <DIRECTORY>
          Run `hyperion` in service mode and monitor the specified DIRECTORY for
          modification. `hyperion` and its service thread will be terminated, if
          the file 'hyperion.exit' is detected.

    -V, --version
          Display version information and exit.

You can report bugs by opening a new issue at <https://codeberg.org/lanre/trondheim/>
or by sending an email to <koladeolanrewaju@tutanota.com>.";

/// Struct used to process supported options in `hyperion`.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[doc(hidden)]
pub struct Options<'hyperion> {
	/// `-a` | `--all`.
	pub all: bool,

	/// `-d` | `--delay`.
	pub delay: Option<u64>,

	/// `-o` | `--oom-score`.
	pub oom_score: Option<i16>,

	/// `-s` | `--service`.
	pub service: Option<&'hyperion Path>,
}

#[doc(hidden)]
#[inline]
pub fn try_main(user_input: Vec<OsString>) -> common::Result<()> {
	const OPTIONS: &[(&str, &str, HasArg)] = &[
		( "--all",       "-a", HasArg::False ),
		( "--delay",     "-d", HasArg::True  ),
		( "--help",      "-h", HasArg::False ),
		( "--oom-score", "-o", HasArg::True  ),
		( "--service",   "-s", HasArg::True  ),
		( "--version",   "-V", HasArg::False ),
	];

	let ((options, arguments), mut hyperion, mut service) = (getopt(user_input, OPTIONS, "hyperion")?, Options::default(), String::new());

	for (option, mut argument) in options {
		match option.as_str() {
			"--all" | "-a" => {
				hyperion.all = true;
			},
			"--delay" | "-d" if hyperion.delay.is_some() => ret! {
				ExitCode::Failure, "hyperion: `{}` cannot be specified multiple times", option
			},
			"--delay" | "-d" => hyperion.delay = Some(argument.parse::<u64>().map_err(|error| error! {
				ExitCode::Failure, "hyperion: failed to parse '{}' for `{}`: {}", argument, option, error
			})?),
			"--help" | "-h" => {
				stdoutln! { "{}", HELP };
				return Ok(());
			},
			"--oom-score" | "-o" if hyperion.oom_score.is_some() => ret! {
				ExitCode::Failure, "hyperion: `{}` cannot be specified multiple times", option
			},
			"--oom-score" | "-o" => match argument.parse::<i16>() {
				Err(error) => ret! {
					ExitCode::Failure, "hyperion: failed to parse '{}' for `{}`: {}", argument, option, error
				},
				Ok(result) if !(-1000..=1000).contains(&result) => ret! {
					ExitCode::Failure, "hyperion: invalid integer '{}' specified for `{}`", argument, option
				},
				Ok(result) => {
					hyperion.oom_score = Some(result);
				},
			},
			"--service" | "-s" if !service.is_empty() => ret! {
				ExitCode::Failure, "hyperion: `{}` cannot be specified multiple times", option
			},
			"--service" | "-s" => {
				service = mem::take(&mut argument);
			},
			"--version" | "-V" => {
				stdoutln! { "{}", version("hyperion", env! { "CARGO_PKG_VERSION" }) };
				return Ok(());
			},
			_ => ret! {
				ExitCode::Failure, "hyperion: fatal error: Unhandled option `{}`", option
			},
		}
	}

	if !service.is_empty() {
		hyperion.service = Some(Path::new(&service));
	}

	change_oom_score(arguments, hyperion)
}
