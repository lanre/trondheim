// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::ffi::c_int;

/// Argument list too long.
pub const E2BIG: c_int = 7;

/// Permission denied.
pub const EACCES: c_int = 13;

/// Try again.
pub const EAGAIN: c_int = 11;

/// Bad file number.
pub const EBADF: c_int = 9;

/// Device or resource busy.
pub const EBUSY: c_int = 16;

/// Quota exceeded.
pub const EDQUOT: c_int = 122;

/// File exists.
pub const EEXIST: c_int = 17;

/// Bad address.
pub const EFAULT: c_int = 14;

/// Interrupted system call.
pub const EINTR: c_int = 4;

/// Invalid argument.
pub const EINVAL: c_int = 22;

/// Is a directory.
pub const EISDIR: c_int = 21;

/// Too many symbolic links encountered.
pub const ELOOP: c_int = 40;

/// Too many open files.
pub const EMFILE: c_int = 24;

/// Too many links.
pub const EMLINK: c_int = 31;

/// File name too long.
pub const ENAMETOOLONG: c_int = 36;

/// No such device.
pub const ENODEV: c_int = 19;

/// No such file or directory.
pub const ENOENT: c_int = 2;

/// Required key not available.
pub const ENOKEY: c_int = 126;

/// Out of memory.
pub const ENOMEM: c_int = 12;

/// No space left on device.
pub const ENOSPC: c_int = 28;

/// Invalid system call number.
pub const ENOSYS: c_int = 38;

/// Not a directory.
pub const ENOTDIR: c_int = 20;

/// Directory not empty.
pub const ENOTEMPTY: c_int = 39;

/// Operation not supported.
pub const ENOTSUP: c_int = EOPNOTSUPP;

/// No such device or address.
pub const ENXIO: c_int = 6;

/// Operation not supported on transport endpoint.
pub const EOPNOTSUPP: c_int = 95;

/// Value too large for defined data type.
pub const EOVERFLOW: c_int = 75;

/// Operation not permitted.
pub const EPERM: c_int = 1;

/// Broken pipe.
pub const EPIPE: c_int = 32;

/// Math result not representable.
pub const ERANGE: c_int = 34;

/// Read-only file system.
pub const EROFS: c_int = 30;

/// No such process.
pub const ESRCH: c_int = 3;

/// Text file busy.
pub const ETXTBSY: c_int = 26;

/// Operation would block.
pub const EWOULDBLOCK: c_int = EAGAIN;

/// Cross-device link.
pub const EXDEV: c_int = 18;
