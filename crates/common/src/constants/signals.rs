// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::ffi::c_int;

/// Abort signal received from **`abort()`**.
pub const SIGABRT: c_int = 6;

/// Resume process if stopped.
pub const SIGCONT: c_int = 18;

/// Interrupt signal received from keyboard.
pub const SIGINT: c_int = 2;

/// Kill signal.
pub const SIGKILL: c_int = 9;

/// Quit signal received from keyboard.
pub const SIGQUIT: c_int = 3;

/// Invalid memory reference.
pub const SIGSEGV: c_int = 11;

/// Stop process.
pub const SIGSTOP: c_int = 19;

/// Termination signal.
pub const SIGTERM: c_int = 15;

/// Broken pipe (write to pipe with no readers).
pub const SIGPIPE: c_int = 13;
