// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

type Mode = u32;

/// File type constant of a block-oriented device file.
pub const S_IFBLK: Mode = 0o060_000;

/// File type constant of a character-oriented device file.
pub const S_IFCHR: Mode = 0o020_000;

/// File type constant of a directory.
pub const S_IFDIR: Mode = 0o040_000;

/// File type constant of a FIFO or pipe.
pub const S_IFIFO: Mode = 0o010_000;

/// File type constant of a symbolic link.
pub const S_IFLNK: Mode = 0o120_000;

/// Bitmask for extracting file type constant from a mode value.
pub const S_IFMT: Mode = 0o0_170_000;

/// File type constant of a regular file.
pub const S_IFREG: Mode = 0o100_000;

/// File type constant of a unix domain socket.
pub const S_IFSOCK: Mode = 0o140_000;

/// Read permission bit for the group owner of the file.
pub const S_IRGRP: Mode = 0o0040;

/// Read permission bit for other users (not in group).
pub const S_IROTH: Mode = 0o0004;

/// Read permission bit for the owner of the file.
pub const S_IRUSR: Mode = 0o0400;

/// Group owner has read, write and execute permission i.e ([`S_IRGRP`] | [`S_IWGRP`] | [`S_IXGRP`]).
pub const S_IRWXG: Mode = 0o0070;

/// Other users (not in group) have read, write and execute permission i.e ([`S_IROTH`] | [`S_IWOTH`] | [`S_IXOTH`]).
pub const S_IRWXO: Mode = 0o0007;

/// Owner has read, write and execute permission i.e ([`S_IRUSR`] | [`S_IWUSR`] | [`S_IXUSR`]).
pub const S_IRWXU: Mode = 0o0700;

/// Set-group-ID on execute bit.
pub const S_ISGID: Mode = 0o002_000;

/// Set-user-ID on execute bit.
pub const S_ISUID: Mode = 0o004_000;

/// Sticky bit.
pub const S_ISVTX: Mode = 0o001_000;

/// Write permission bit for the group owner of the file.
pub const S_IWGRP: Mode = 0o0020;

/// Write permission bit for other users (not in group).
pub const S_IWOTH: Mode = 0o0002;

/// Write permission bit for the owner of the file.
pub const S_IWUSR: Mode = 0o0200;

/// Execute permission bit for the group owner of the file.
pub const S_IXGRP: Mode = 0o0010;

/// Execute permission bit for other users (not in group).
pub const S_IXOTH: Mode = 0o0001;

/// Execute permission bit for the owner of the file.
pub const S_IXUSR: Mode = 0o0100;
