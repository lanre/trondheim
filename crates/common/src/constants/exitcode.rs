// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::error::ExitCode;

/// Command line parsing errors.
pub const GETOPT_ERR_CODE: ExitCode = ExitCode::Other(2);

/// An integer overflow was detected.
pub const OVERFLOW_ERR_CODE: ExitCode = ExitCode::Other(10);

/// A race condition was detected.
pub const RACE_ERR_CODE: ExitCode = ExitCode::Other(11);

/// Errors were encountered during recursion.
pub const REC_ERR_CODE: ExitCode = ExitCode::Other(4);

/// Same file operation.
pub const SF_ERR_CODE: ExitCode = ExitCode::Other(7);

/// Source file removal failed.
pub const SRC_ERR_CODE: ExitCode = ExitCode::Other(9);

/// Program behavior was changed due to user input.
pub const USER_ERR_CODE: ExitCode = ExitCode::Other(5);
