// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::ffi::c_int;
use std::ffi::c_long;

/// Allow an empty relative **pathname** and operate on **dirfd** directly.
pub const AT_EMPTY_PATH: c_int = 0x1000;

/// Special value for **dirfd** used to represent the current working directory.
pub const AT_FDCWD: c_int = -100;

/// Follow symbolic links.
pub const AT_SYMLINK_FOLLOW: c_int = 0x400;

/// Do not follow symbolic links.
pub const AT_SYMLINK_NOFOLLOW: c_int = 0x100;

/// Reduce size without leaving holes.
pub const FALLOC_FL_COLLAPSE_RANGE: c_int = 0x08;

/// Increase size without overwriting data.
pub const FALLOC_FL_INSERT_RANGE: c_int = 0x20;

/// Preserve size.
pub const FALLOC_FL_KEEP_SIZE: c_int = 0x01;

/// Deallocate range.
pub const FALLOC_FL_PUNCH_HOLE: c_int = 0x02;

/// Unshare shared blocks.
pub const FALLOC_FL_UNSHARE_RANGE: c_int = 0x40;

/// Convert range to zeroes.
pub const FALLOC_FL_ZERO_RANGE: c_int = 0x10;

/// Place an exclusive lock on a file.
pub const LOCK_EX: c_int = 2;

/// Make a non-blocking request to **flock()**.
pub const LOCK_NB: c_int = 4;

/// Place a shared lock on a file.
pub const LOCK_SH: c_int = 1;

/// Remove file lock.
pub const LOCK_UN: c_int = 8;

/// Set close_on_exec.
pub const O_CLOEXEC: c_int = 0o2_000_000;

#[cfg(any(target_arch = "arm", target_arch = "aarch64"))]
/// File must be a directory.
pub const O_DIRECTORY: c_int = 0o40_000;

#[cfg(not(any(target_arch = "arm", target_arch = "aarch64")))]
/// File must be a directory.
pub const O_DIRECTORY: c_int = 0o0_200_000;

#[cfg(any(target_arch = "arm", target_arch = "aarch64"))]
/// Enable support for large files.
pub const O_LARGEFILE: c_int = 0o400_000;

#[cfg(not(any(target_arch = "arm", target_arch = "aarch64")))]
/// Enable support for large files.
pub const O_LARGEFILE: c_int = 0o0_100_000;

/// Do not update file last access time.
pub const O_NOATIME: c_int = 0o1_000_000;

#[cfg(any(target_arch = "arm", target_arch = "aarch64"))]
/// Do not follow symbolic links in trailing components.
pub const O_NOFOLLOW: c_int = 0o100_000;

#[cfg(not(any(target_arch = "arm", target_arch = "aarch64")))]
/// Do not follow symbolic links in trailing components.
pub const O_NOFOLLOW: c_int = 0o400_000;

/// Open in non blocking mode.
pub const O_NONBLOCK: c_int = 0o0_004_000;

/// Obtain file descriptor without opening file.
pub const O_PATH: c_int = 0o10_000_000;

/// Discard freed swap pages before reuse.
pub const SWAP_FLAG_DISCARD: c_int = 0x10000;

/// Perform a single discard over the entire swap area at swapon time.
pub const SWAP_FLAG_DISCARD_ONCE: c_int = 0x20000;

/// Perform discards as swap pages are freed.
pub const SWAP_FLAG_DISCARD_PAGES: c_int = 0x40000;

/// Set swap priority as higher than default.
pub const SWAP_FLAG_PREFER: c_int = 0x8000;

/// Bitmask for specifying custom swap priority.
pub const SWAP_FLAG_PRIO_MASK: c_int = 0x7fff;

/// Bitflag for specifying custom swap priority.
pub const SWAP_FLAG_PRIO_SHIFT: c_int = 0;

/// Set time stamp to current time.
pub const UTIME_NOW: c_long = (1 << 30) - 1;

/// Ignore setting time stamp.
pub const UTIME_OMIT: c_long = (1 << 30) - 2;
