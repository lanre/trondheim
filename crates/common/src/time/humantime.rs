// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::ops::Neg;
use std::ops::Rem;
use std::time::SystemTime;
use std::time::UNIX_EPOCH;

/// A human readable UTC representation of time.
///
/// Create a new [`HumanTime`] instance from an existing [`SystemTime`] using
/// [`HumanTime::from_systemtime`], or from an arbitrary [`i64`] second with
/// [`HumanTime::from_raw_seconds`].
///
/// HumanTime can be used to represent timestamps from before the UNIX epoch.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct HumanTime {
	/// The day, expressed in relation to the specified month.
	day: u8,

	/// The current hour, expressed in relation to the day.
	hour: u8,

	/// The current minute, expressed in relation to the hour.
	minute: u8,

	/// The current month, expressed in relation to the year.
	month: u8,

	/// The current second, expressed in relation to the minute.
	second: u8,

	/// The current year, expressed in relation to the UNIX epoch.
	year: i64,
}

impl HumanTime {
	/// Returns the days contained in the [`HumanTime`] as a [`u8`].
	#[inline]
	#[must_use]
	pub const fn day(&self) -> u8 {
		self.day
	}

	/// Create a new [`HumanTime`] instance from an arbitrary [`i64`].
	#[inline]
	#[must_use]
	pub fn from_raw_seconds(seconds: i64) -> HumanTime {
		HumanTime::_from_raw_seconds(i128::from(seconds))
	}

	#[expect(clippy::arithmetic_side_effects)]
	#[expect(clippy::cast_possible_truncation)]
	fn _from_raw_seconds(seconds: i128) -> HumanTime {
		// Time elapsed since or before the UNIX epoch as days.
		let (day, seconds) = (seconds.div_euclid(86_400), seconds.rem_euclid(86_400));

		// Time elapsed since or before the UNIX epoch in years.
		let year = (day * 400).div_euclid(146_097) + 1970;

		// Shift the Unix epoch to 0000-03-01.
		let epoch_offset = day + ((1970 * 146_097) / 400) - 31 - 28;

		// Number of days elapsed in the current era.
		let day_of_era = epoch_offset.rem_euclid(146_097);

		// Number of years elapsed in the current era.
		let year_of_era = (day_of_era - (day_of_era / 1460) + (day_of_era / 36524) - (day_of_era / 146_096)) / 365;

		// Number of days elapsed in the current year.
		let day_of_year = day_of_era - ((365 * year_of_era) + (year_of_era / 4) - (year_of_era / 100));

		// Number of months elapsed in the current year.
		let month_of_year = ((5 * day_of_year) + 2) / 153;

		HumanTime {
			day:    (day_of_year - (((153 * month_of_year) + 2) / 5) + 1) as u8,
			hour:   (seconds / 3600) as u8,
			minute: (seconds / 60).rem(60) as u8,
			month:  if month_of_year < 10 { month_of_year + 3 } else { month_of_year - 9 } as u8,
			second: seconds.rem(60) as u8,
			year:   year as i64,
		}
	}

	/// Create a new [`HumanTime`] instance from a [`SystemTime`] struct.
	#[inline]
	#[must_use]
	pub fn from_systemtime(time: SystemTime) -> HumanTime {
		HumanTime::_from_raw_seconds(match time.duration_since(UNIX_EPOCH) {
			Err(time) => {
				i128::from(time.duration().as_secs()).neg()
			},
			Ok(time) => {
				i128::from(time.as_secs())
			}
		})
	}

	/// Returns the hours contained in the [`HumanTime`] as a [`u8`].
	#[inline]
	#[must_use]
	pub const fn hour(&self) -> u8 {
		self.hour
	}

	/// Returns the minutes contained in the [`HumanTime`] as a [`u8`].
	#[inline]
	#[must_use]
	pub const fn minute(&self) -> u8 {
		self.minute
	}

	/// Returns the months contained in the [`HumanTime`] as a [`u8`].
	#[inline]
	#[must_use]
	pub const fn month(&self) -> u8 {
		self.month
	}

	/// Returns a new [`HumanTime`] instance corresponding to the current
	/// system time.
	#[inline]
	#[must_use]
	pub fn now() -> HumanTime {
		HumanTime::from_systemtime(SystemTime::now())
	}

	/// Returns the seconds contained in the [`HumanTime`] as a [`u8`].
	#[inline]
	#[must_use]
	pub const fn second(&self) -> u8 {
		self.second
	}

	/// Convert [`HumanTime`] to a human readable UTC [`String`].
	///
	/// Returns the time contained in the [`HumanTime`] as a [`String`] in
	/// the format **YYYY-MM-DD HH:MM:SS +0000**.
	///
	/// # Example
	///
	/// ```rust
	/// use common::time::HumanTime;
	///
	/// assert_eq! { "1960-04-01 22:34:23 +0000", HumanTime::from_raw_seconds(-307675537).to_utc_string() };
	/// assert_eq! { "2025-01-12 17:43:21 +0000", HumanTime::from_raw_seconds(1736703801).to_utc_string() };
	/// ```
	#[inline]
	#[must_use]
	pub fn to_utc_string(&self) -> String {
		format! { "{:04}-{:02}-{:02} {:02}:{:02}:{:02} +0000", self.year(), self.month(), self.day(), self.hour(), self.minute(), self.second() }
	}

	/// Returns the year contained in the [`HumanTime`] as an [`i64`] integer.
	#[inline]
	#[must_use]
	pub const fn year(&self) -> i64 {
		self.year
	}
}
