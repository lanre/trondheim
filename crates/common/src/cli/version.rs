// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/// Returns formatted program version and license info as a [`String`].
///
/// # Example
///
/// ```rust
/// use common::cli::version;
/// use common::stdoutln;
///
/// // Name of the current program set at build time.
/// const NAME: &str = env! { "CARGO_PKG_NAME" };
///
/// // Version of the current program set at build time.
/// const VERSION: &str = env! { "CARGO_PKG_VERSION" };
///
/// stdoutln! { "{}", version(NAME, VERSION) };
/// ```
#[inline]
#[must_use]
pub fn version<N: AsRef<str>, V: AsRef<str>>(pkg_name: N, pkg_version: V) -> String {
	format! { "{} {}\n\
	Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju.\n\
	License GPLv3+: GNU GPL version 3 or later <https://www.gnu.org/licenses/gpl.html>.\n\
	This is free software: you are free to change and redistribute it.\n\
	There is NO WARRANTY, to the extent permitted by law.\n\n\
	Written by Kolade Ayomide Olanrewaju.", pkg_name.as_ref(), pkg_version.as_ref() }
}
