// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::GETOPT_ERR_CODE;
use crate::constants::OVERFLOW_ERR_CODE;
use crate::error;
use crate::ret;

use std::ffi::OsString;

/// Flags used with [`getopt`] to specify if an option requires an argument.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum HasArg {
	/// Option requires no argument.
	False,

	/// Option requires an optional argument.
	Optional,

	/// Option requires an argument.
	True,
}

/// Return type for [`getopt`].
type GetoptResult = crate::Result<(Vec<(String, String)>, Vec<String>)>;

/// A fast and simple command line parser used by **trondheim** and all its built in programs.
///
/// Returns a tuple, `(Vec<(String, String)>, Vec<String>)` with the first element containing the parsed options
/// and their required arguments, and the latter containing arguments e.g filenames.
///
/// # Features
///
/// * Options with optional arguments, [`HasArg::Optional`].
/// * Options with mandatory arguments, [`HasArg::True`].
/// * Options without arguments, [`HasArg::False`].
/// * `--` to mark the end of options.
/// * `=` to seperate options from values, e.g `--color=always`, `-c=always`.
/// * Spaces to seperate options from values, e.g `--color always`, `-c always`.
/// * Unseperated short options, e.g `-calways` -> `-c always`.
/// * Combined short options, e.g `-abc` -> `-a, -b, -c`.
/// * Single hypen long options, e.g `-exec` are unsupported.
/// * Every option must be explicitly declared, to support `--num` as an abbreviated
///   form of `--number`, both must be declared as program **options**.
/// * Options with multiple arguments are unsupported, e.g `program --file FILE1,FILE2,FILE3`
///   will be parsed as `("--file", "FILE1,FILE2,FILE3")`.
///
/// # Errors
///
/// This function will return an [`Error`] with an error message and a [`GETOPT_ERR_CODE`]
/// if it unable to parse arguments, or it encounters an invalid or unrecognized option, or if
/// the input arguments to this function are invalid.
///
/// # Example
///
/// ```no_run
/// use common::cli::getopt;
/// use common::cli::HasArg;
/// use common::stderrln;
///
/// use std::env::args_os;
/// use std::ffi::OsString;
/// use std::process::ExitCode;
/// use std::process::Termination;
///
/// const HELP: &str =
/// "test-program - usage guide for getopt
///
/// Usage:
///    test-program [OPTION...]
///
/// Options:
///    -c, --color
///          Does something with color.
///
///    -e
///          Does something.
///
///        --help
///          Display this help message and exit.
///
///        --version
///          Display version information and exit.
///
///    -p, --pwr-level
///          Does something with power levels.
///
///    -v, --verbose
///          Show what is being done. Errors will always be printed to the standard
///          error stream, even when this option is not specified.
///
/// You can report bugs by opening a new issue at <https://link.to.git.repo/>";
///
/// // Program name; will be expanded at compile time.
/// const PKG_NAME: &str = env! { "CARGO_PKG_NAME" };
///
/// // long options, short options, has arguments.
/// const OPTIONS: &[(&str, &str, HasArg)] = &[
///         ( "",            "-e", HasArg::False    ),
///         ( "--color",     "-c", HasArg::Optional ),
///         ( "--help",      "",   HasArg::False    ),
///         ( "--pwr-level", "-p", HasArg::True     ),
///         ( "--verbose",   "-v", HasArg::False    ),
///         ( "--version",   "",   HasArg::False    ),
/// ];
///
/// fn main() -> ExitCode {
///     // Collect user input from command line.
///     let user_input = args_os().collect::<Vec<OsString>>();
///
///     // `options` contains parsed options and their provided argmuents. e.g [("--pwr-level", "20"), ("--help", "")...
///     // `arguments` contains remaining arguments e.g ["file1", ...
///     let (options, arguments) = match getopt(user_input, OPTIONS, PKG_NAME) {
///         Err(error) => {
///             return error.report() // will print an error message and return `GETOPT_ERR_CODE`.
///         },
///         Ok(result) => {
///             result
///         },
///     };
///
///     for (option, argument) in options {
///         match option.as_str() {
///             "-e" => {
///                 // Your code here.
///             },
///             "--color" | "-c" => {
///                 // Your code here.
///             },
///             "--help" => {
///                 // Your code here.
///             },
///             "--pwr-level" | "-p" => {
///                 // Your code here.
///             },
///             "--verbose" | "-v" => {
///                 // Your code here.
///             },
///             "--version" => {
///                 // Your code here.
///             },
///             // Cannot be reached since all declared `options` have been handled.
///             _ => {
///                 stderrln! { "{}: fatal error: Unhandled option `{}`", PKG_NAME, option };
///                 return ExitCode::FAILURE;
///             },
///         }
///     }
///
///     // No arguments were provided to the program.
///     if arguments.is_empty() {
///         stderrln! { "{}: missing command line arguments", PKG_NAME };
///         return ExitCode::FAILURE; // or `ExitCode::from(u8)`.
///     }
///
///     // The rest of your code here.
///
///     ExitCode::SUCCESS
/// }
/// ```
///
/// [`Error`]: crate::error::Error
#[inline]
pub fn getopt<N: AsRef<str>>(user_input: Vec<OsString>, options: &[(&str, &str, HasArg)], program: N) -> GetoptResult {
	let (program, user_input) = (program.as_ref(), input_validation(user_input, options, program.as_ref())?);

	// Split user input at the index of `--`, if it was specified.
	let (arg_slice, pos_args, mut optlist, mut args) = match user_input.split_at_checked(user_input.iter().position(|a| a.as_str() == "--").unwrap_or(user_input.len())) {
		None => ret! {
			GETOPT_ERR_CODE, "{}: fatal error: Failed to parse user input {:?}", program, user_input
		},
		Some((arg_slice, pos_args)) => {
			(arg_slice, pos_args, Vec::with_capacity(user_input.len()), arg_slice.to_vec())
		},
	};

	for option in arg_slice {
		if option.starts_with('-') && option.trim_start_matches('-').is_empty() {
			ret! { GETOPT_ERR_CODE, "{}: unrecognized option `{}`", program, option };
		}
		else if option.starts_with("--") && option.contains('=') {
			parse_longopt(option, options, &mut optlist, &mut args, program)?;
		}
		else if option.starts_with("--") {
			parse_alt_longopt(option, options, &mut optlist, &mut args, program)?;
		}
		else if option.starts_with('-') && option.contains('=') {
			parse_shortopt(option, options, &mut optlist, &mut args, program)?;
		}
		else if option.starts_with('-') {
			parse_alt_shortopt(option, options, &mut optlist, &mut args, program)?;
		}
	}

	// Recombine positional arguments -- `pos_args[0]` will contain `--`.
	if let Some(pos_args) = pos_args.get(1..) {
		args.extend(pos_args.to_vec());
	}

	Ok((optlist, args))
}

// Validate input arguments for `getopt()`.
fn input_validation(user_input: Vec<OsString>, options: &[(&str, &str, HasArg)], program: &str) -> crate::Result<Vec<String>> {
	// Calling `getopt()` with an empty `options[]` will still work --  the program will recognize no options.
	// However it is more efficient to just return an error if options are specified.
	//
	// Example
	//
	// let user_input = args_os().collect::<Vec<OsString>>();
	//
	// if user_input.len() > 1 {
	// 	``` your error here ```
	// }
	debug_assert! { !options.is_empty(), "Empty `options[]` detected: `options` should not be empty" };

	// Error messages from this program will be formatted incorrectly if no package name is provided.
	debug_assert! { !program.is_empty(), "Empty `program` detected: `program` should not be empty" };

	#[cfg(debug_assertions)]
	for (longopt, shortopt, _) in options {
		// long `options` must either begin with two hypen `--` or must be declared as an empty `&str`.
		debug_assert! { longopt.is_empty() || longopt.strip_prefix("--").is_some(), "Invalid program long option '{}' detected", longopt };

		// short `options` must either begin with one hypen `-` or must be declared as an empty `&str`.
		debug_assert! { shortopt.is_empty() || (shortopt.strip_prefix('-').is_some() && shortopt.strip_prefix("--").is_none()), "Invalid program short option '{}' detected", shortopt };
	}

	match user_input.get(1..) {
		None => ret! {
			GETOPT_ERR_CODE, "{}: fatal error: Failed to detect any input from the command line", program
		},
		Some(user_input) => {
			Ok(user_input.iter().map(|input| String::from(input.to_string_lossy())).collect())
		},
	}
}

// Parse long options in the form `--option=argument`.
fn parse_longopt(option: &str, options: &[(&str, &str, HasArg)], optlist: &mut Vec<(String, String)>, args: &mut Vec<String>, program: &str) -> crate::Result<()> {
	let (unparsedopt, argument) = option.split_once('=').ok_or_else(|| error! { GETOPT_ERR_CODE, "{}: fatal error: Failed to parse `{}`", program, option })?;

	for (longopt, _, optarg) in options.iter().copied() {
		if longopt == unparsedopt && (optarg == HasArg::True || optarg == HasArg::Optional) && argument.is_empty() {
			ret! { GETOPT_ERR_CODE, "{}: an argument is required for `{}` but none was supplied", program, longopt };
		}
		else if longopt == unparsedopt && (optarg == HasArg::True || optarg == HasArg::Optional) {
			optlist.push((String::from(longopt), String::from(argument)));
			args.iter().position(|c| c == option).map(|index| args.remove(index)).ok_or_else(|| error! { GETOPT_ERR_CODE, "{}: fatal error: Failed to parse `{}`", program, option })?;
			return Ok(());
		}
		else if longopt == unparsedopt && optarg == HasArg::False {
			ret! { GETOPT_ERR_CODE, "{}: `{}` does not accept any arguments but '{}' was supplied", program, longopt, argument };
		}
	}

	ret! { GETOPT_ERR_CODE, "{}: unrecognized option `{}`", program, option };
}

// Parse long options in the form `--option argument`.
fn parse_alt_longopt(option: &str, options: &[(&str, &str, HasArg)], optlist: &mut Vec<(String, String)>, args: &mut Vec<String>, program: &str) -> crate::Result<()> {
	let (opt_index, arg_index) = match args.iter().position(|c| c == option) {
		None => ret! {
			GETOPT_ERR_CODE, "{}: fatal error: Failed to parse `{}`", program, option
		},
		Some(index) => {
			(index, index.checked_add(1).ok_or_else(|| error! { OVERFLOW_ERR_CODE, "{}: fatal error: Integer overflow occured while parsing `{}`", program, option })?)
		},
	};

	for (longopt, _, optarg) in options.iter().copied() {
		if longopt == option {
			match args.get(arg_index) {
				None if optarg == HasArg::True => ret! {
					GETOPT_ERR_CODE, "{}: an argument is required for `{}` but none was supplied", program, longopt
				},
				Some(argument) if (argument.is_empty() || argument.starts_with('-')) && optarg == HasArg::True => ret! {
					GETOPT_ERR_CODE, "{}: an argument is required for `{}` but none was supplied", program, longopt
				},
				Some(argument) if !argument.is_empty() && !argument.starts_with('-') && optarg != HasArg::False => {
					optlist.push((String::from(longopt), String::from(argument)));
					drop(args.drain(opt_index..=arg_index));
					return Ok(());
				},
				None | Some(_) => {
					optlist.push((String::from(longopt), String::new()));
					args.remove(opt_index);
					return Ok(());
				},
			}
		}
	}

	ret! { GETOPT_ERR_CODE, "{}: unrecognized option `{}`", program, option };
}

// Parse short options in the form `-o=argument`.
fn parse_shortopt(option: &str, options: &[(&str, &str, HasArg)], optlist: &mut Vec<(String, String)>, args: &mut Vec<String>, program: &str) -> crate::Result<()> {
	let mut unparsed = option.trim_start_matches('-');

	'mainloop: for letter in option.trim_start_matches('-').chars() {
		let (unparsedchecker, unparsedopt) = (unparsed, format! { "-{}", letter });

		'innerloop: for (_, shortopt, optarg) in options.iter().copied() {
			if shortopt == unparsedopt && (optarg == HasArg::True || optarg == HasArg::Optional) {
				match unparsed.strip_prefix(letter) {
					None => ret! {
						GETOPT_ERR_CODE, "{}: fatal error: Failed to parse `{}`", program, option
					},
					Some(argument) => {
						let argument = unparsed.strip_prefix(&format! { "{}=", letter }).map_or(argument, |stripped| stripped);

						if argument.is_empty() {
							ret! { GETOPT_ERR_CODE, "{}: an argument is required for `{}` but none was supplied", program, shortopt };
						}

						optlist.push((String::from(shortopt), String::from(argument)));
						break 'mainloop;
					},
				}
			}
			else if shortopt == unparsedopt && optarg == HasArg::False {
				if let Some(argument) = unparsed.strip_prefix(&format! { "{}=", letter }) {
					ret! { GETOPT_ERR_CODE, "{}: `{}` does not accept any arguments but '{}' was supplied", program, shortopt, argument };
				}

				optlist.push((String::from(shortopt), String::new()));
				unparsed = unparsed.strip_prefix(letter).ok_or_else(|| error! { GETOPT_ERR_CODE, "{}: fatal error: Failed to parse `{}`", program, option })?;
				break 'innerloop;
			}
		}

		if unparsed == unparsedchecker {
			ret! { GETOPT_ERR_CODE, "{}: invalid option -- `{}`", program, letter };
		}
	}

	args.iter().position(|c| c == option).map(|index| args.remove(index)).ok_or_else(|| error! { GETOPT_ERR_CODE, "{}: fatal error: Failed to parse `{}`", program, option })?;
	Ok(())
}

// Parse short options in the form `-o argument`.
fn parse_alt_shortopt(option: &str, options: &[(&str, &str, HasArg)], optlist: &mut Vec<(String, String)>, args: &mut Vec<String>, program: &str) -> crate::Result<()> {
	let (mut unparsed, (opt_index, arg_index)) = (option.trim_start_matches('-'), match args.iter().position(|c| c == option) {
		None => ret! {
			GETOPT_ERR_CODE, "{}: fatal error: Failed to parse `{}`", program, option
		},
		Some(index) => {
			(index, index.checked_add(1).ok_or_else(|| error! { OVERFLOW_ERR_CODE, "{}: fatal error: Integer overflow occured while parsing `{}`", program, option })?)
		}
	});

	'mainloop: for letter in option.trim_start_matches('-').chars() {
		let (unparsedchecker, unparsedopt) = (unparsed, format! { "-{}", letter });

		'innerloop: for (_, shortopt, optarg) in options.iter().copied() {
			if shortopt == unparsedopt && (optarg == HasArg::True || optarg == HasArg::Optional) {
				let argument = unparsed.strip_prefix(letter).ok_or_else(|| error! { GETOPT_ERR_CODE, "{}: fatal error: Failed to parse `{}`", program, option })?;

				if !argument.is_empty() {
					optlist.push((String::from(shortopt), String::from(argument)));
					break 'mainloop;
				}

				match args.get(arg_index) {
					None if optarg == HasArg::True => ret! {
						GETOPT_ERR_CODE, "{}: an argument is required for `{}` but none was supplied", program, shortopt
					},
					Some(argument) if (argument.is_empty() || argument.starts_with('-')) && optarg == HasArg::True => ret! {
						GETOPT_ERR_CODE, "{}: an argument is required for `{}` but none was supplied", program, shortopt
					},
					Some(argument) if !argument.is_empty() && !argument.starts_with('-') => {
						optlist.push((String::from(shortopt), String::from(argument)));
						drop(args.drain(opt_index..=arg_index));
						return Ok(());
					},
					None | Some(_) => {
						optlist.push((String::from(shortopt), String::new()));
						break 'mainloop;
					},
				}
			}
			else if shortopt == unparsedopt && optarg == HasArg::False {
				optlist.push((String::from(shortopt), String::new()));
				unparsed = unparsed.strip_prefix(letter).ok_or_else(|| error! { GETOPT_ERR_CODE, "{}: fatal error: Failed to parse `{}`", program, option })?;
				break 'innerloop;
			}
		}

		if unparsed == unparsedchecker {
			ret! { GETOPT_ERR_CODE, "{}: invalid option -- `{}`", program, letter };
		}
	}

	args.remove(opt_index);
	Ok(())
}
