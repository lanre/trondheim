// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod macros;

/// APIs for processing / handling user input.
pub mod cli;

/// Platform specific compile-time constants / flags used with system calls.
pub mod constants;

/// Interfaces for working with Errors.
pub mod error;

/// APIs for manipulating the contents of the local filesystem.
pub mod fs;

/// APIs for retrieving information about the current platform / system.
pub mod sys;

/// Human friendly temporal quantification.
pub mod time;

/// Result type used across **trondheim** and its built-in programs.
pub type Result<T> = std::result::Result<T, crate::error::Error>;
