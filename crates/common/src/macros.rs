// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/// Convenience macro for creating a new [`CString`](std::ffi::CString) from a [`Path`].
///
/// # Errors
///
/// This macro will return an [`io::Error`](std::io::Error), if the specified [`Path`] is empty or if
/// the path contains any null bytes.
///
/// [`Path`]: std::path::Path
#[macro_export]
macro_rules! cstring {
	($path: expr) => {{
		let path = $path.as_ref().as_os_str();

		if path.is_empty() {
			return Err(std::io::Error::from_raw_os_error($crate::constants::ENOENT));
		}

		std::ffi::CString::new(path.as_bytes()).map_err(|_| std::io::Error::from_raw_os_error($crate::constants::EINVAL))?
	}};
}

/// Convenience macro for constructing a new [`Error`] instance.
///
/// [`Error`]: crate::error::Error
#[macro_export]
macro_rules! error {
	($code: expr, $($msg: tt)*) => {
		$crate::error::Error::new($code, format! { $($msg)* })
	};
}

/// Convenience macro for returning an [`Error`] to the current function.
///
/// [`Error`]: crate::error::Error
#[macro_export]
macro_rules! ret {
	($error: expr) => {
		return Err($crate::error::Error::new($error.exitcode(), $error.message()))
	};
	($code: expr, $($msg: tt)*) => {
		return Err($crate::error::Error::new($code, format! { $($msg)* }))
	};
}

/// Convenience macro for executing system calls.
///
/// # Safety
///
/// Running a system call is inherently unsafe. It is the caller's
/// responsibility to ensure safety and proper usage of this macro.
#[macro_export]
macro_rules! syscall {
	($function: ident(), $ret: ty) => {
		unsafe extern "C" {
			fn $function() -> $ret;
		}

		unsafe { $function() }
	};
	($function: ident($($types: expr),*), $ret: ty) => {{
		unsafe extern "C" {
			fn $function(...) -> $ret;
		}

		#[allow(clippy::macro_metavars_in_unsafe)]
		let result = unsafe { $function($($types),*) };

		if result == -1 {
			Err(std::io::Error::last_os_error())
		}
		else {
			Ok(result)
		}
	}};
	($function: ident($($types: expr),*)) => {{
		unsafe extern "C" {
			fn $function(...) -> c_int;
		}

		#[allow(clippy::macro_metavars_in_unsafe)]
		if unsafe { $function($($types),*) } == -1 {
			Err(std::io::Error::last_os_error())
		}
		else {
			Ok(())
		}
	}};
}

/// Print messages to the standard error.
///
/// This macro uses the same syntax as [`eprint`] and will lock the standard error and
/// standard output on each call to avoid output interleaving. Errors encountered
/// while printing are silently ignored.
#[macro_export]
macro_rules! stderr {
	($($fmt: tt)*) => {{
		let (output, stdout) = (format! { $($fmt)* }, std::io::stdout().lock());
		drop(($crate::syscall! { write($crate::constants::STDERR_FILENO, output.as_ptr().cast::<std::ffi::c_void>(), output.len()), isize }, stdout));
	}};
}

/// Print messages to the standard error, with a newline.
///
/// This macro uses the same syntax as [`eprintln`] and will lock the standard error and
/// standard output on each call to avoid output interleaving. Errors encountered
/// while printing are silently ignored.
#[macro_export]
macro_rules! stderrln {
	($($fmt: tt)*) => {
		$crate::stderr! { "{}\n", format_args! { $($fmt)* } }
	};
}

/// Print messages to the standard output.
///
/// This macro uses the same syntax as [`print`] and will lock the standard output
/// and standard error on each call to avoid output interleaving. Errors encountered
/// while printing are silently ignored.
#[macro_export]
macro_rules! stdout {
	($($fmt: tt)*) => {{
		let (output, stderr) = (format! { $($fmt)* }, std::io::stderr().lock());
		drop(($crate::syscall! { write($crate::constants::STDOUT_FILENO, output.as_ptr().cast::<std::ffi::c_void>(), output.len()), isize }, stderr));
	}};
}

/// Print messages to the standard output, with a newline.
///
/// This macro uses the same syntax as [`println`] and will lock the standard output
/// and standard error on each call to avoid output interleaving. Errors encountered
/// while printing are silently ignored.
#[macro_export]
macro_rules! stdoutln {
	($($fmt: tt)*) => {
		$crate::stdout! { "{}\n", format_args! { $($fmt)* } }
	};
}

/// Convenience macro for testing built-in programs, intended for internal use only.
///
/// # Panics
///
/// This macro will panic if any error is encountered while trying to run the program.
#[macro_export]
macro_rules! testbin {
	($bin: expr) => {
		std::process::Command::new($bin).output().unwrap()
	};
	($bin: expr, $($command: tt)*) => {
		std::process::Command::new($bin).args(format! { $($command)* }.split(' ')).output().unwrap()
	};
}
