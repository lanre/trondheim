// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::EINTR;
use crate::constants::EISDIR;
use crate::constants::EOVERFLOW;
use crate::constants::ENOENT;

use std::ffi::c_void;
use std::ffi::OsString;
use std::fs;
use std::io;
use std::os::fd::AsRawFd;
use std::os::unix::ffi::OsStringExt;
use std::path::Path;
use std::str::FromStr;
use std::string::FromUtf8Error;

/// Struct returned by [`readfile`] for effective file content handling.
#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct ReadFile(Vec<u8>);

impl ReadFile {
	/// Returns a reference to the bytes contained in the [`ReadFile`].
	///
	/// # Examples
	///
	/// ```rust
	/// use common::fs::readfile;
	///
	/// use std::fs;
	/// use std::io::Write;
	/// use std::io;
	/// use std::os::fd::AsRawFd;
	///
	/// fn main() -> io::Result<()> {
	///     // Read `/bin/bash` into memory and return a reference to the data.
	///     let bytes = readfile("/bin/bash")?;
	///     let bytes = bytes.as_raw_bytes();
	///
	///     // Open the destination in write-only mode.
	///     let mut fildes = fs::File::options().write(true).open("/dev/null")?;
	///
	///     fildes.write_all(bytes)?;
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn as_raw_bytes(&self) -> &[u8] {
		&self.0
	}

	/// Consumes the [`ReadFile`] and returns the inner [`Vec<u8>`], allowing it
	/// to be used to write to a file or stream, without copying or reallocating
	/// memory.
	///
	/// # Examples
	///
	/// ```rust
	/// use common::fs::readfile;
	/// use common::syscall;
	///
	/// use std::ffi::c_void;
	/// use std::fs;
	/// use std::io;
	/// use std::os::fd::AsRawFd;
	///
	/// fn main() -> io::Result<()> {
	///     // Read `/bin/bash` into memory and return the `ReadFile` inner vector.
	///     let buffer = readfile("/bin/bash")?.into_inner();
	///
	///     // The file is empty, don't bother calling `write()`.
	///     if buffer.is_empty() {
	///        return Ok(());
	///     }
	///
	///     // Open the destination in write-only mode.
	///     let fildes = fs::File::options().write(true).open("/dev/null")?;
	///
	///     // SAFETY: The buffer is not empty and buffer.len() is guaranteed to be <= buffer.capacity().
	///     let written = syscall! { write(fildes.as_raw_fd(), buffer.as_ptr().cast::<c_void>(), buffer.len()), isize }?;
	///
	///     // The buffer was written completely to the destination.
	///     assert_eq! { written as usize, buffer.len() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn into_inner(self) -> Vec<u8> {
		self.0
	}

	/// Parse the content of the file into the specified type.
	///
	/// Internally, this method will convert the [`ReadFile`] into a lossy [`String`],
	/// with the leading and trailing whitespaces removed, before attempting to parse
	/// to the specified type.
	///
	/// # Errors
	///
	/// This method will return an [`Error`], if it is unable to parse the file content
	/// as the specific type.
	///
	/// # Examples
	///
	/// ```rust
	/// use common::fs::readfile;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let file = readfile("/sys/class/power_supply/BAT0/capacity")?;
	///     assert! { file.parse::<i32>().is_ok() };
	///     Ok(())
	/// }
	/// ```
	///
	/// [`Error`]: std::str::FromStr::Err
	#[inline]
	pub fn parse<T: FromStr>(&self) -> Result<T, <T as FromStr>::Err> {
		String::from_utf8_lossy(self.as_raw_bytes()).trim().parse::<T>()
	}

	/// Return the content of the [`ReadFile`] as a lossy [`String`].
	///
	/// This method will strip whitespaces from the start and end of the String, and all
	/// invalid UTF-8 sequences will be replaced with U+FFFD **�**.
	///
	/// # Examples
	///
	/// ```rust
	/// use common::fs::readfile;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let file = readfile("/proc/meminfo")?.to_lossy_string();
	///     assert! { file.contains("MemTotal:") };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn to_lossy_string(&self) -> String {
		String::from_utf8_lossy(self.as_raw_bytes()).trim().to_owned()
	}

	/// Convert the [`ReadFile`] to an [`OsString`].
	///
	/// # Examples
	///
	/// ```rust
	/// use common::fs::readfile;
	/// use common::stdoutln;
	///
	/// use std::fs;
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{:?}", readfile("/proc/cpuinfo")?.to_os_string() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn to_os_string(self) -> OsString {
		OsString::from_vec(self.into_inner())
	}

	/// Convert the [`ReadFile`] to a [`String`].
	///
	/// # Errors
	///
	/// This method will return an [`Error`], if the [`ReadFile`] contains invalid UTF-8
	/// characters.
	///
	/// # Examples
	///
	/// ```rust
	/// use common::fs::readfile;
	/// use common::stdoutln;
	///
	/// use std::fs;
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let file = readfile("/proc/cpuinfo")?;
	///     assert! { file.to_string().is_ok() };
	///     Ok(())
	/// }
	/// ```
	///
	/// [`Error`]: std::string::FromUtf8Error
	#[inline]
	pub fn to_string(self) -> Result<String, FromUtf8Error> {
		String::from_utf8(self.into_inner())
	}
}

/// Convenience function for reading the contents of a file. On success, this function will
/// return a [`ReadFile`] instance, containing the bytes read from the file.
///
/// # Errors
///
/// This function will return an [`io::Error`], if an error is encountered while reading the
/// specified file.
///
/// # Example
///
/// ```no_run
/// use common::fs::readfile;
/// use common::stdoutln;
///
/// use std::io;
///
/// fn main() -> io::Result<()> {
///     stdoutln! { "{}", readfile("/path/to/file")?.to_lossy_string() };
///     Ok(())
/// }
/// ```
#[inline]
pub fn readfile<P: AsRef<Path>>(file: P) -> io::Result<ReadFile> {
	if file.as_ref().as_os_str().is_empty() {
		return Err(io::Error::from_raw_os_error(ENOENT));
	}

	// Open the file in read only mode.
	let fildes = fs::File::options().read(true).open(file)?;

	let metadata = fildes.metadata()?;

	// `read()` cannot read from directories, avoid unneccessary allocations.
	if metadata.file_type().is_dir() {
		return Err(io::Error::from_raw_os_error(EISDIR));
	}

	// Allocate memory for the read buffer.
	let mut buffer: Vec<u8> = Vec::new();

	#[expect(clippy::cast_possible_truncation)]
	buffer.try_reserve(if !metadata.file_type().is_file() || metadata.len() == 0 { 32768 } else { metadata.len().saturating_add(1024) as usize })?;

	loop {
		// The total size of unused allocated memory.
		let capacity = buffer.capacity().checked_sub(buffer.len()).ok_or_else(|| io::Error::from_raw_os_error(EOVERFLOW))?;

		// Use the read syscall directly.
		match crate::syscall! { read(fildes.as_raw_fd(), buffer.spare_capacity_mut().as_mut_ptr().cast::<c_void>(), capacity), isize } {
			Err(error) if error.raw_os_error() == Some(EINTR) => {
				continue;
			},
			Err(error) => {
				return Err(error);
			},
			Ok(read) => {
				#[expect(clippy::cast_sign_loss)]
				let read = read as usize;

				// All the data read so far.
				let data = buffer.len().checked_add(read).ok_or_else(|| io::Error::from_raw_os_error(EOVERFLOW))?;

				// The buffer is full or is too small, allocate more memory.
				if data > buffer.capacity() || read == capacity {
					buffer.try_reserve(buffer.capacity().saturating_mul(4))?;
				}

				// SAFETY: buffer[..data] is guaranteed to be initialized.
				unsafe { buffer.set_len(data) };

				// EOF has been reached. Return read content.
				if read == 0 {
					return Ok(ReadFile(buffer));
				}
			},
		}
	}
}
