// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::AT_FDCWD;
use crate::constants::AT_SYMLINK_NOFOLLOW;
use crate::constants::EOVERFLOW;
use crate::constants::UTIME_OMIT;
use crate::cstring;
use crate::syscall;

use std::ffi::c_int;
use std::io;
use std::os::unix::ffi::OsStrExt;
use std::path::Path;
use std::time::SystemTime;
use std::time::UNIX_EPOCH;

/// Flags used with [`utimensat`].
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct UtimeFlags(c_int);

#[expect(non_upper_case_globals)]
impl UtimeFlags {
	/// Follow encountered symbolic links.
	pub const Follow: UtimeFlags = UtimeFlags(0);

	/// Do not follow encountered symbolic links.
	pub const NoFollow: UtimeFlags = UtimeFlags(AT_SYMLINK_NOFOLLOW);
}

/// Structure used to represent time intervals since or before the Unix epoch.
#[repr(C)]
#[derive(Debug)]
struct timespec {
	/// File times in seconds.
	tv_sec: i64,

	/// Nanoseconds since tv_sec.
	tv_nsec: i64,
}

impl timespec {
	/// Create a new timespec instance from a [`SystemTime`]
	fn new(input: Option<SystemTime>) -> Option<timespec> {
		const NANOS_PER_SEC: i64 = 1_000_000_000;

		if let Some(time) = input {
			return Some(match time.duration_since(UNIX_EPOCH) {
				Err(time) => timespec {
					tv_sec: {
						i64::try_from(time.duration().as_secs()).ok()?.checked_neg()?
					},
					tv_nsec: {
						if time.duration().subsec_nanos() == 0 {
							i64::from(time.duration().subsec_nanos())
						}
						else {
							NANOS_PER_SEC.checked_sub(i64::from(time.duration().subsec_nanos()))?
						}
					},
				},
				Ok(time) => timespec {
					tv_sec: {
						i64::try_from(time.as_secs()).ok()?
					},
					tv_nsec: {
						i64::from(time.subsec_nanos())
					},
				},
			});
		}

		Some(timespec { tv_sec: 0, tv_nsec: UTIME_OMIT })
	}
}

/// Safe wrapper for **utimensat()** for changing file timestamps with nanosecond precision.
///
/// # Errors
///
/// This function will return an [`io::Error`] if the specified [`Path`] contains multiple
/// null bytes, it may also return an error, if the user lacks permissions to change the
/// timestamps on the underlying file. It may also return an error in other os-specific,
/// unspecified cases.
///
/// # Example
///
/// ```no_run
/// use common::fs::UtimeFlags;
/// use common::fs::utimensat;
///
/// use std::io;
/// use std::time::SystemTime;
///
/// fn main() -> io::Result<()> {
///     // To update both access time and modification time to the current time.
///     utimensat("source_file", Some(SystemTime::now()), Some(SystemTime::now()), UtimeFlags::NoFollow)?;
///
///     // To set only the access time to the current time.
///     utimensat("source_file", Some(SystemTime::now()), None, UtimeFlags::NoFollow)?;
///
///     // To set only the modification time to the current time.
///     utimensat("source_file", None, Some(SystemTime::now()), UtimeFlags::NoFollow)?;
///
///     // Leave both access time and modification time unchanged.
///     utimensat("source_file", None, None, UtimeFlags::NoFollow)
/// }
/// ```
#[inline]
pub fn utimensat<P: AsRef<Path>>(path: P, atime: Option<SystemTime>, mtime: Option<SystemTime>, flag: UtimeFlags) -> io::Result<()> {
	let times = [
		timespec::new(atime).ok_or_else(|| io::Error::from_raw_os_error(EOVERFLOW))?,
		timespec::new(mtime).ok_or_else(|| io::Error::from_raw_os_error(EOVERFLOW))?,
	];

	syscall! { utimensat(AT_FDCWD, cstring! { path }.as_ptr(), times.as_ptr(), flag.0) }
}
