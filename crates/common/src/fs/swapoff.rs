// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::cstring;
use crate::syscall;

use std::ffi::c_int;
use std::io;
use std::os::unix::ffi::OsStrExt;
use std::path::Path;

/// Safe wrapper for **swapoff()**, to stop swapping to a file or block device.
///
/// # Errors
///
/// This function will return an [`io::Error`] if the specified [`Path`] does not exist. It may
/// also return an error in other os-specific unspecified cases.
///
/// # Example
///
/// ```no_run
/// use common::fs::swapoff;
///
/// use std::io;
///
/// fn main() -> io::Result<()> {
///     swapoff("/path/to/swapfile")
/// }
/// ```
#[inline]
pub fn swapoff<P: AsRef<Path>>(path: P) -> io::Result<()> {
	syscall! { swapoff(cstring! { path }.as_ptr()) }
}
