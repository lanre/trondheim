// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::ENOENT;
use crate::fs::expand_path;

use std::fs;
use std::io;
use std::os::unix::fs::MetadataExt;
use std::path::Path;

/// Trait extension for comparing file [`Metadata`].
///
/// [`Metadata`]: std::fs::Metadata
trait CompareMetadata {
	/// Compare device and inode number.
	fn compare(&self, other: &Self) -> bool;

	/// Fallback to comparing general metadata fields.
	fn compare_fallback(&self, other: &Self) -> bool;
}

impl CompareMetadata for fs::Metadata {
	fn compare(&self, other: &fs::Metadata) -> bool {
		(self.dev(), self.ino()) == (other.dev(), other.ino())
	}

	fn compare_fallback(&self, other: &fs::Metadata) -> bool {
		self.atime() == other.atime() &&
		self.ctime() == other.ctime() &&
		self.gid()   == other.gid()   &&
		self.ino()   == other.ino()   &&
		self.mode()  == other.mode()  &&
		self.mtime() == other.mtime() &&
		self.size()  == other.size()  &&
		self.uid()   == other.uid()
	}
}

/// Trait extension for comparing file [`Path`]s.
pub trait IsSameFile {
	/// Returns a [`bool`] indicating if **source** and **destination** refer to the same file or directory.
	///
	/// # Errors
	///
	/// This function will return an [`io::Error`], if **source** does not exist, or is un[`stat`]able, or if
	/// there is insufficient permissions to access **destination**.
	///
	/// # Example
	///
	/// ```rust
	/// use common::fs::IsSameFile;
	///
	/// use std::path::Path;
	///
	/// let destination = "/../../proc/../proc/../proc/cpuinfo";
	/// assert! { matches! { Path::new("/proc/cpuinfo").is_samefile(destination), Ok(true) } };
	/// ```
	///
	/// [`stat`]: std::fs::metadata
	fn is_samefile<P: AsRef<Path>>(&self, destination: P) -> io::Result<bool>;
}

impl IsSameFile for Path {
	#[inline]
	fn is_samefile<P: AsRef<Path>>(&self, destination: P) -> io::Result<bool> {
		// Call `lstat()` on `source` before proceeding.
		let (source_metadata, destination) = (self.symlink_metadata()?, destination.as_ref());

		match fs::symlink_metadata(destination) {
			Ok(destination_metadata) => {
				// Since `source` and `destination` both exist, check if both files are hardlinks to the same
				// file or if they are on a case insensitive filesystem.
				if source_metadata.compare(&destination_metadata) || expand_path(destination).starts_with(expand_path(self)) {
					return Ok(true);
				}

				// Fallback to comparing general metadata values. None of these are guaranteed to be
				// unique across filesystems, but the chances of false positives are *very* slim.
				Ok(source_metadata.compare_fallback(&destination_metadata))
			},
			Err(error) if error.raw_os_error() != Some(ENOENT) => {
				// Most likely a permission error.
				Err(error)
			},
			Err(_) => {
				// Since `source` exists and `destination` does not, they are not hardlinks to the same file
				// nor are they on a case insensitive file system.
				fs::DirBuilder::new().recursive(true).create(destination)?;

				let destination_metadata = fs::symlink_metadata(destination)?;

				// Ensure it is a directory that is being `stat()`ed.
				if !destination_metadata.file_type().is_dir() {
					return Err(io::Error::other("Race condition detected"));
				}

				if source_metadata.compare(&destination_metadata) || expand_path(destination).starts_with(expand_path(self)) {
					fs::remove_dir(destination)?;
					return Ok(true);
				}

				fs::remove_dir(destination)?;
				Ok(source_metadata.compare_fallback(&destination_metadata))
			},
		}
	}
}

/// Returns a [`bool`] indicating if **source** and **destination** refer to the same file or directory.
///
/// This is an alias to [`Path::is_samefile`].
///
/// # Errors
///
/// This function will return an [`io::Error`], if **source** does not exist, or is un[`stat`]able, or if
/// there is insufficient permissions to access **destination**.
///
/// # Example
///
/// ```rust
/// use common::fs::is_samefile;
///
/// let destination = "/../../proc/../proc/../proc/cpuinfo";
/// assert! { matches! { is_samefile("/proc/cpuinfo", destination), Ok(true) } };
/// ```
///
/// [`stat`]: std::fs::metadata
#[inline]
pub fn is_samefile<P: AsRef<Path>, Q: AsRef<Path>>(source: P, destination: Q) -> io::Result<bool> {
	source.as_ref().is_samefile(destination)
}
