// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::AT_FDCWD;
use crate::constants::AT_SYMLINK_NOFOLLOW;
use crate::cstring;
use crate::syscall;

use std::ffi::c_int;
use std::fs;
use std::io;
use std::os::unix::ffi::OsStrExt;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;

/// Flags used with [`fchmodat`].
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct FchmodFlags(c_int);

#[expect(non_upper_case_globals)]
impl FchmodFlags {
	/// Follow encountered symbolic links.
	pub const Follow: FchmodFlags = FchmodFlags(0);

	/// Do not follow encountered symbolic links. This option is not implemented on all
	/// platforms and will return [`EOPNOTSUPP`] where it is unsupported.
	///
	/// [`EOPNOTSUPP`]: crate::constants::EOPNOTSUPP
	pub const NoFollow: FchmodFlags = FchmodFlags(AT_SYMLINK_NOFOLLOW);
}

/// Safe wrapper for **fchmodat()** for changing the permissions of a file.
///
/// # Errors
///
/// This function will return an [`io::Error`] if the specified [`Path`] does not exist, contains multiple null
/// bytes or is empty. It will also return an error if the user lacks sufficient permissions to change the
/// attributes of the specified file. It may also return an error in other os-specific unspecified cases.
///
/// # Example
///
/// ```no_run
/// use common::fs::fchmodat;
/// use common::fs::FchmodFlags;
///
/// use std::fs;
/// use std::io;
/// use std::os::unix::fs::PermissionsExt;
///
/// fn main() -> io::Result<()> {
///     let permission = fs::Permissions::from_mode(0o644);
///
///     // To change file permission to -rw-r--r--. This will follow symbolic
///     // links, modifying the target file.
///     fchmodat("/path/to/file", permission.clone(), FchmodFlags::Follow)?;
///
///     // To change file permission to -rw-r--r--. This will not follow symbolic
///     // links, modifying `file` itself. This option is only available on some
///     // operating systems and will return a `ENOTSUP` or `EOPNOTSUPP` on Linux
///     // for instance.
///     fchmodat("/path/to/file", permission, FchmodFlags::NoFollow)
/// }
/// ```
#[inline]
pub fn fchmodat<P: AsRef<Path>>(path: P, permission: fs::Permissions, flag: FchmodFlags) -> io::Result<()> {
	syscall! { fchmodat(AT_FDCWD, cstring! { path }.as_ptr(), permission.mode(), flag.0) }
}
