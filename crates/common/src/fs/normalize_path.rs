// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::path::Component;
use std::path::Path;
use std::path::PathBuf;

/// Normalize a specified [`Path`].
///
/// Resolve path components like `.` and `..` without accessing the underlying
/// filesystem. This function does not follow symbolic links.
///
/// # Example
///
/// ```rust
/// use common::fs::normalize_path;
///
/// use std::path::PathBuf;
///
/// let source = "/sys/class/hwmon/../hwmon/../hwmon/./../../class/hwmon";
/// assert_eq! { PathBuf::from("/sys/class/hwmon"), normalize_path(source) };
/// ```
#[inline]
#[must_use]
pub fn normalize_path<P: AsRef<Path>>(path: P) -> PathBuf {
	let (path, ospath) = (path.as_ref(), path.as_ref().as_os_str());

	if ospath.is_empty() {
		return PathBuf::new();
	}

	let mut fullpath = PathBuf::with_capacity(ospath.len());

	for component in path.components() {
		match component {
			Component::CurDir | Component::Prefix(_) => {
			},
			Component::Normal(_) | Component::RootDir => {
				fullpath.push(component);
			},
			Component::ParentDir => match fullpath.components().last() {
				None | Some(Component::CurDir | Component::ParentDir) => {
					fullpath.push(component);
				},
				Some(Component::Normal(_)) => {
					fullpath.pop();
				},
				Some(Component::RootDir | Component::Prefix(_)) => {
				},
			},
		}
	}

	if fullpath.as_os_str().is_empty() {
		fullpath.push(Component::CurDir);
	}

	fullpath
}
