// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::EINVAL;
use crate::constants::FALLOC_FL_COLLAPSE_RANGE;
use crate::constants::FALLOC_FL_INSERT_RANGE;
use crate::constants::FALLOC_FL_KEEP_SIZE;
use crate::constants::FALLOC_FL_PUNCH_HOLE;
use crate::constants::FALLOC_FL_UNSHARE_RANGE;
use crate::constants::FALLOC_FL_ZERO_RANGE;
use crate::syscall;

use std::ffi::c_int;
use std::fs;
use std::io;
use std::os::fd::AsRawFd;

/// Flags used with [`Fallocate`].
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct FallocFlags(c_int);

#[expect(non_upper_case_globals)]
impl FallocFlags {
	/// [`FALLOC_FL_COLLAPSE_RANGE`], remove a range from a file without leaving holes.
	pub const CollapseRange: FallocFlags = FallocFlags(FALLOC_FL_COLLAPSE_RANGE);

	/// Allocate disk space within a range of a file, only increasing the file size if
	/// **offset + len** is greater than file size.
	pub const DefaultMode: FallocFlags = FallocFlags(0);

	/// [`FALLOC_FL_INSERT_RANGE`], insert space within the file size without overwriting
	/// any existing data.
	pub const InsertRange: FallocFlags = FallocFlags(FALLOC_FL_INSERT_RANGE);

	/// [`FALLOC_FL_KEEP_SIZE`], same as [`DefaultMode`] but the preserve file size even
	/// when the specified **offset + len** is greater than the file size.
	///
	/// [`DefaultMode`]: crate::fs::FallocFlags::DefaultMode
	pub const KeepSize: FallocFlags = FallocFlags(FALLOC_FL_KEEP_SIZE);

	/// [`FALLOC_FL_PUNCH_HOLE`] | [`FALLOC_FL_KEEP_SIZE`], deallocate space within a range
	/// from a file, preserving the file size even if **offset + len** is greater than
	/// the file size.
	pub const PunchHoleKeepSize: FallocFlags = FallocFlags(FALLOC_FL_PUNCH_HOLE | FALLOC_FL_KEEP_SIZE);

	/// [`FALLOC_FL_UNSHARE_RANGE`], unshare shared blocks within a file range without
	/// overwriting any existing data.
	pub const UnshareRange: FallocFlags = FallocFlags(FALLOC_FL_UNSHARE_RANGE);

	/// [`FALLOC_FL_UNSHARE_RANGE`] | [`FALLOC_FL_KEEP_SIZE`], unshare shared blocks within
	/// a file range, preserving file size and without overwriting any existing data.
	pub const UnshareRangeKeepSize: FallocFlags = FallocFlags(FALLOC_FL_UNSHARE_RANGE | FALLOC_FL_KEEP_SIZE);

	/// [`FALLOC_FL_ZERO_RANGE`], convert a range from a file to zeros preferably without
	/// issuing data IO.
	pub const ZeroRange: FallocFlags = FallocFlags(FALLOC_FL_ZERO_RANGE);

	/// Same as [`ZeroRange`] but preserve file size, i.e [`FALLOC_FL_ZERO_RANGE`] | [`FALLOC_FL_KEEP_SIZE`].
	///
	/// [`ZeroRange`]: crate::fs::FallocFlags::ZeroRange
	pub const ZeroRangeKeepSize: FallocFlags = FallocFlags(FALLOC_FL_ZERO_RANGE | FALLOC_FL_KEEP_SIZE);
}

/// **fallocate()** extension for [`File`].
///
/// [`File`]: std::fs::File
pub trait Fallocate {
	/// Manipulate file space.
	///
	/// # Errors
	///
	/// This function will return an [`io::Error`] if **len** or **offset** is less than 0,
	/// if interrupted with a signal, if **len + offset** exceeds the maximum file
	/// size supported by the filesystem. It may also return an [`io::Error`] in other
	/// os-specific, unspecified cases.
	///
	/// # Example
	///
	/// ```no_run
	/// use common::fs::Fallocate;
	/// use common::fs::FallocFlags;
	///
	/// use std::fs;
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     // Create empty file.
	///     let file = fs::File::options().write(true).create(true).open("/path/to/file")?;
	///
	///     let size = 1024 << 20; // 1 GiB as bytes.
	///
	///     // Create 1 GiB sparse file.
	///     file.fallocate(FallocFlags::DefaultMode, 0, size)?;
	///
	///     // Same as above but preserve size at 1 GiB.
	///     file.fallocate(FallocFlags::KeepSize, 0, size * 4)?;
	///
	///     // Fill empty blocks with zeroes and increase size by 1 GiB.
	///     file.fallocate(FallocFlags::ZeroRange, 0, size * 2)?;
	///
	///     // Fill empty blocks with zeroes but preserve size as 2 GiB.
	///     file.fallocate(FallocFlags::ZeroRangeKeepSize, 0, size * 3)?;
	///
	///     // Reduce file size back to 1 GiB.
	///     file.fallocate(FallocFlags::CollapseRange, 0, size)?;
	///
	///     // Increase file to 3 GiB by adding hole.
	///     file.fallocate(FallocFlags::InsertRange, 0, size * 3)?;
	///
	///     // Make holes in file, but preserve file size.
	///     file.fallocate(FallocFlags::PunchHoleKeepSize, 0, size * 4)?;
	///
	///     // Unshare any shared blocks while preserving size.
	///     file.fallocate(FallocFlags::UnshareRangeKeepSize, 0, size * 12)?;
	///
	///     // Unshare any shared blocks.
	///     file.fallocate(FallocFlags::UnshareRange, 0, size)
	/// }
	/// ```
	fn fallocate(&self, mode: FallocFlags, offset: i64, len: i64) -> io::Result<()>;
}

impl Fallocate for fs::File {
	#[inline]
	fn fallocate(&self, mode: FallocFlags, offset: i64, len: i64) -> io::Result<()> {
		if offset < 0 || len <= 0 {
			return Err(io::Error::from_raw_os_error(EINVAL));
		}

		syscall! { fallocate(self.as_raw_fd(), mode.0, offset, len) }
	}
}
