// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::EEXIST;
use crate::constants::ELOOP;
use crate::constants::O_NOFOLLOW;
use crate::fs::Flock;
use crate::fs::LockOperation;

use std::fs;
use std::io;
use std::os::unix::fs::fchown;
use std::os::unix::fs::OpenOptionsExt;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;

/// This struct exposes the ability to configure how a file is created, if there
/// is an advisory lock on the open [`File`], the file permissions and the user ID
/// and group ID of the file.
///
/// To use CreateFile, first call CreateFile::new, then chain method calls to
/// enable the desired features. Call CreateFile::create last, passing the [`Path`]
/// of the file to create.
///
/// [`File`]: std::fs::File
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct CreateFile {
	/// The group ID to create the file with.
	gid: Option<u32>,

	/// Specify what type of advisory file lock should be placed on the open [`File`].
	locktype: Option<LockOperation>,

	/// The file permissions to create the file with.
	mode: Option<u32>,

	/// The user ID to create the file with.
	uid: Option<u32>,
}

impl CreateFile {
	/// Convenience function for creating new files.
	///
	/// This method will consume the [`CreateFile`] instance and return a read / write [`File`],
	/// with an optional advisory file lock,
	///
	/// # Errors
	///
	/// This method will return an [`io::Error`], if an error is encountered while creating the
	/// specified file, e.g if the specified **file** already exists as a symbolic link, or in other
	/// os-specific, unspecified cases.
	///
	/// # Example
	///
	/// ```rust
	/// use common::constants::S_IFREG;
	/// use common::constants::S_IRUSR;
	/// use common::constants::S_IWUSR;
	/// use common::fs::CreateFile;
	/// use common::fs::Flock;
	/// use common::fs::LockOperation;
	/// use common::fs::ReadFile;
	///
	/// use std::fs;
	/// use std::io::Read;
	/// use std::io::Seek;
	/// use std::io::Write;
	/// use std::io;
	/// use std::os::unix::fs::PermissionsExt;
	/// use std::path::Path;
	/// use std::thread;
	///
	/// fn main() -> io::Result<()> {
	///     let file = Path::new("CreateFile_create_test_file");
	///
	///     // Create a new file with 0o600 permission bits, lock the file and return the file descriptor.
	///     let mut fildes = CreateFile::new().flock(LockOperation::ExclusiveNonBlocking).fchmod(fs::Permissions::from_mode(S_IRUSR | S_IWUSR)).create(file)?;
	///     assert_eq! { fildes.metadata()?.permissions().mode(), S_IFREG | 0o600 };
	///
	///     // Write message to file.
	///     fildes.write_all("file created successfully".as_bytes())?;
	///
	///     // Return cursor to the beginning of the stream.
	///     fildes.rewind()?;
	///
	///     // Allocate memory for read buffer.
	///     let mut buffer = Vec::new();
	///     buffer.try_reserve(128)?;
	///
	///     // Read message from file.
	///     fildes.read_to_end(&mut buffer)?;
	///     assert_eq! { String::from_utf8_lossy(&buffer), String::from("file created successfully") };
	///
	///     // Since the file was locked, no other threads can gain a lock on it, until the file is
	///     // either explicitly unlocked or the file descriptor goes out of scope and is dropped.
	///     thread::spawn(move || assert_eq! {
	///          fs::File::options().read(true).open(file).unwrap().flock(LockOperation::ExclusiveNonBlocking).unwrap_err().kind(), io::ErrorKind::WouldBlock
	///     }).join().unwrap();
	///
	///     // Creating a new file will truncate the existing file length to 0. This will run
	///     // successfully even with the lock still existing on the file.
	///     assert_eq! { CreateFile::new().create(file)?.metadata()?.len(), 0 };
	///
	///     // The lock still exists, but since it is an advisory file lock, other threads can still
	///     // write to the file, if they do not try to acquire a lock first.
	///     thread::spawn(move || assert_eq! {
	///          CreateFile::new().flock(LockOperation::ExclusiveNonBlocking).create(file).unwrap_err().kind(), io::ErrorKind::WouldBlock
	///     }).join().unwrap();
	///
	///    // Clean up test file.
	///    fs::remove_file(file)
	/// }
	/// ```
	///
	/// [`File`]: std::fs::File
	#[inline]
	pub fn create<P: AsRef<Path>>(&self, file: P) -> io::Result<fs::File> {
		let fildes = fs::File::options().read(true).write(true).create(true).truncate(self.locktype.is_none()).custom_flags(O_NOFOLLOW).open(file).map_err(|error| {
			if error.raw_os_error() == Some(ELOOP) { io::Error::from_raw_os_error(EEXIST) } else { error }
		})?;

		if let Some(locktype) = self.locktype {
			// Acquire an advisory file lock.
			fildes.flock(locktype)?;

			// Truncate file length to 0.
			fildes.set_len(0)?;
		}

		// Change file ownership.
		if self.uid.is_some() || self.gid.is_some() {
			fchown(&fildes, self.uid, self.gid)?;
		}

		// Change file permissions.
		if let Some(permissions) = self.mode {
			fildes.set_permissions(fs::Permissions::from_mode(permissions))?;
		}

		Ok(fildes)
	}

	/// Sets the file permission bits of the [`File`].
	///
	/// This method allows configuring the permission / mode bits the [`File`] will be created with.
	///
	/// # Examples
	///
	/// ```rust
	/// use common::constants::S_IFMT;
	/// use common::constants::S_IFREG;
	/// use common::constants::S_IWUSR;
	/// use common::fs::CreateFile;
	///
	/// use std::fs;
	/// use std::io;
	/// use std::os::unix::fs::PermissionsExt;
	///
	/// fn main() -> io::Result<()> {
	///     // Will create the file in owner write-only mode -- 0o200.
	///     let fildes = CreateFile::new().fchmod(fs::Permissions::from_mode(S_IWUSR)).create("newfile_createfile_test_fchmod")?;
	///     assert_eq! { fildes.metadata()?.permissions().mode() & !S_IFMT, 0o200 };
	///     assert_eq! { fildes.metadata()?.permissions().mode(), S_IFREG | 0o200 };
	///
	///     fs::remove_file("newfile_createfile_test_fchmod")?;
	///     Ok(())
	/// }
	/// ```
	///
	/// [`File`]: std::fs::File
	#[inline]
	pub fn fchmod(&mut self, permission: fs::Permissions) -> &mut CreateFile {
		self.mode = Some(permission.mode());
		self
	}

	/// Change the owner ID and group ID of the [`File`].
	///
	/// Specifying either the **uid** or **gid** as [`None`] will leave it unchanged.
	/// Specifying both as [`None`] is the same as not calling this method. Calling
	/// this method may require elevated system privileges on certain operating systems.
	///
	/// # Examples
	///
	/// ```no_run
	/// use common::fs::CreateFile;
	///
	/// use std::fs;
	/// use std::io;
	/// use std::os::unix::fs::MetadataExt;
	///
	/// fn main() -> io::Result<()> {
	///     // Will change only the user ID. Will fail with `EPERM` if the calling process does not have `CAP_CHOWN` privilege".
	///     let fildes = CreateFile::new().fchown(Some(2000), None).create("newfile_createfile_test_fchown")?;
	///     assert_eq! { fildes.metadata()?.gid(), 1000 };
	///     assert_eq! { fildes.metadata()?.uid(), 2000 };
	///
	///     // Will change only the group ID.
	///     let fildes = CreateFile::new().fchown(None, Some(2000)).create("newfile_createfile_test_fchown")?;
	///     assert_eq! { fildes.metadata()?.gid(), 2000 };
	///     assert_eq! { fildes.metadata()?.uid(), 1000 };
	///
	///     fs::remove_file("newfile_createfile_test_fchown")?;
	///     Ok(())
	/// }
	/// ```
	///
	/// [`File`]: std::fs::File
	#[inline]
	pub fn fchown(&mut self, uid: Option<u32>, gid: Option<u32>) -> &mut CreateFile {
		(self.gid, self.uid) = (gid, uid);
		self
	}

	/// Acquire an advisory lock on the [`File`].
	///
	/// If this option is enabled, the file will be created with an advisory lock, of the
	/// the specified **locktype**. This method allows specifying [`Unlock`](crate::fs::LockOperation::Unlock)
	/// or [`UnlockNonBlocking`](crate::fs::LockOperation::UnlockNonBlocking) as the **locktype**,
	/// so existing locks can be removed before creating the [`File`].
	///
	/// # Examples
	///
	/// ```rust
	/// use common::fs::CreateFile;
	/// use common::fs::LockOperation;
	///
	/// use std::fs;
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     // Will fail with `EWOULDBLOCK` if a lock already exist on the file.
	///     let fildes = CreateFile::new().flock(LockOperation::ExclusiveNonBlocking).create("newfile_createfile_test_flock")?;
	///     assert_eq! { fildes.metadata()?.len(), 0 };
	///
	///     fs::remove_file("newfile_createfile_test_flock")?;
	///     Ok(())
	/// }
	/// ```
	///
	/// [`File`]: std::fs::File
	#[inline]
	pub fn flock(&mut self, locktype: LockOperation) -> &mut CreateFile {
		self.locktype = Some(locktype);
		self
	}

	/// Create a new [`CreateFile`] instance, with the default options.
	///
	/// # Examples
	///
	/// ```no_run
	/// use common::fs::CreateFile;
	/// use common::fs::LockOperation;
	///
	/// let mut file = CreateFile::new();
	/// file.flock(LockOperation::Shared);
	///
	/// let fildes = file.create("newfile_createfile_test");
	/// ```
	#[inline]
	#[must_use]
	pub fn new() -> CreateFile {
		CreateFile::default()
	}
}
