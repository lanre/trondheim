// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::fs::normalize_path;

use std::fs::canonicalize;
use std::marker::Copy;
use std::path::Path;
use std::path::PathBuf;

/// Canonicalize a specified [`Path`] or fallback to path normalization if any error is encountered,
/// e.g when a component in the path does not exist.
///
/// # Example
///
/// ```rust
/// use common::fs::expand_path;
///
/// use std::path::PathBuf;
///
/// let source = "/sys/class/hwmon/hwmon0/subsystem/hwmon0/subsystem/";
/// assert_eq! { PathBuf::from("/sys/class/hwmon"), expand_path(source) };
/// ```
#[inline]
#[must_use]
pub fn expand_path<P: AsRef<Path> + Copy>(path: P) -> PathBuf {
	canonicalize(path).unwrap_or_else(|_| normalize_path(path))
}
