// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::EINVAL;
use crate::constants::SWAP_FLAG_DISCARD;
use crate::constants::SWAP_FLAG_DISCARD_ONCE;
use crate::constants::SWAP_FLAG_DISCARD_PAGES;
use crate::constants::SWAP_FLAG_PREFER;
use crate::constants::SWAP_FLAG_PRIO_MASK;
use crate::constants::SWAP_FLAG_PRIO_SHIFT;
use crate::cstring;
use crate::syscall;

use std::ffi::c_int;
use std::io;
use std::os::unix::ffi::OsStrExt;
use std::path::Path;

/// Swap discard flags used with [`SwapFlags`].
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Discard(c_int);

#[expect(non_upper_case_globals)]
impl Discard {
	/// [`SWAP_FLAG_DISCARD`], discard freed swap pages before reuse.
	pub const Auto: Discard = Discard(SWAP_FLAG_DISCARD);

	/// [`SWAP_FLAG_DISCARD_ONCE`], perform a single discard over the entire swap area at swapon time.
	pub const Once: Discard = Discard(SWAP_FLAG_DISCARD_ONCE);

	/// [`SWAP_FLAG_DISCARD_PAGES`], perform discards as swap pages are freed.
	pub const Pages: Discard = Discard(SWAP_FLAG_DISCARD_PAGES);
}

/// Flags used with [`swapon`].
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct SwapFlags {
	discard:  Option<i32>,
	priority: Option<i32>,
}

impl SwapFlags {
	/// Create a new [`SwapFlags`] instance with device default settings.
	#[inline]
	#[must_use]
	pub fn new() -> SwapFlags {
		SwapFlags::default()
	}

	/// Specify the swap priority flags to be passed to [`swapon`].
	///
	/// # Errors
	///
	/// This method will return an [`io::ErrorKind::InvalidInput`] if the specified
	/// swap **priority > 32767** || **priority < 0**.
	#[inline]
	pub fn priority(&mut self, priority: i32) -> io::Result<SwapFlags> {
		if !(0..=32767).contains(&priority) {
			return Err(io::Error::from_raw_os_error(EINVAL));
		}

		self.priority = Some(priority);
		Ok(self.to_owned())
	}

	/// Specify the swap discard flags to be passed to [`swapon`].
	#[inline]
	#[must_use]
	pub fn discard(&mut self, discard: Discard) -> SwapFlags {
		self.discard = Some(discard.0);
		self.to_owned()
	}

	fn to_swapflags(self) -> c_int {
		self.priority.map_or_else(|| 0, |priority| SWAP_FLAG_PREFER | ((priority << SWAP_FLAG_PRIO_SHIFT) & SWAP_FLAG_PRIO_MASK)) | self.discard.unwrap_or(0)
	}
}

/// Safe wrapper for **swapon()**, to enable swapping to a file or block device.
///
/// # Errors
///
/// This function will return an [`io::Error`] if the specified [`Path`] does not exist. It may
/// also return an error in other os-specific unspecified cases.
///
/// # Example
///
/// ```no_run
/// use common::fs::Discard;
/// use common::fs::SwapFlags;
/// use common::fs::swapon;
///
/// use std::io;
/// use std::path::Path;
///
/// fn main() -> io::Result<()> {
///     let (source_file, mut swapflags) = (Path::new("/path/to/file"), SwapFlags::new());
///
///     // Enable swapping to `source_file` with the priority managed by the kernel.
///     swapon(source_file, swapflags)?;
///
///     // Enable swapping to `source_file` with the priority managed by the kernel
///     // and with `SWAP_FLAG_DISCARD` flag enabled.
///     swapon(source_file, swapflags.discard(Discard::Auto))?;
///
///     // Enable swapping to `source_file` with the priority managed by the kernel
///     // and with `SWAP_FLAG_DISCARD_ONCE` flag enabled.
///     swapon(source_file, swapflags.discard(Discard::Once))?;
///
///     // Enable swapping to `source_file` with the priority managed by the kernel
///     // and with `SWAP_FLAG_DISCARD_PAGES` flag enabled.
///     swapon(source_file, swapflags.discard(Discard::Pages))?;
///
///     // Enable swapping to `source_file` with custom priority.
///     swapon(source_file, swapflags.priority(100)?)?;
///
///     // Enable swapping to `source_file` with custom priority and with
///     // `SWAP_FLAG_DISCARD` flag enabled.
///     swapon(source_file, swapflags.discard(Discard::Auto).priority(100)?)?;
///
///     // Enable swapping to `source_file` with custom priority and with
///     // `SWAP_FLAG_DISCARD_ONCE` flag enabled.
///     swapon(source_file, swapflags.priority(100)?.discard(Discard::Once))?;
///
///     // Enable swapping to `source_file` with custom priority and with
///     // `SWAP_FLAG_DISCARD_PAGES` flag enabled.
///     swapon(source_file, swapflags.priority(100)?.discard(Discard::Pages))
/// }
/// ```
#[inline]
pub fn swapon<P: AsRef<Path>>(path: P, flags: SwapFlags) -> io::Result<()> {
	syscall! { swapon(cstring! { path }.as_ptr(), flags.to_swapflags()) }
}
