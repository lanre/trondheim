// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::AT_FDCWD;
use crate::cstring;
use crate::syscall;

use std::ffi::c_int;
use std::fs;
use std::io;
use std::os::unix::ffi::OsStrExt;
use std::os::unix::fs::MetadataExt;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;

/// Flag used with [`mknodat`] for managing device numbers.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct MakeDev(u64);

impl MakeDev {
	/// Create a new device ID using the specified **`major`** and **`minor`** numbers.
	#[inline]
	#[must_use]
	pub fn new(major: u32, minor: u32) -> MakeDev {
		let major: u64 = u64::from(major);
		let minor: u64 = u64::from(minor);

		let mut dev: u64 = minor & 0x0000_00ff;

		dev |= ( minor & 0xffff_ff00 ) << 12;
		dev |= ( major & 0x0000_0fff ) << 8;
		dev |= ( major & 0xffff_f000 ) << 32;

		MakeDev(dev)
	}

	/// Create device ID from the [`Metadata`] of an existing file.
	///
	/// [`Metadata`]: std::fs::Metadata
	#[inline]
	#[must_use]
	pub fn from_metadata(metadata: &fs::Metadata) -> MakeDev {
		MakeDev(metadata.rdev())
	}
}

/// Safe wrapper for **mknodat()** for creating special or regular files.
///
/// # Errors
///
/// This function will return an [`io::Error`] if the specified [`Path`] does not exist or contains multiple
/// null bytes. It will also return an error if the user lacks permissions to one of more leading components
/// in the path. It may also return an error in other os-specific unspecified cases.
///
/// # Example
///
/// ```no_run
/// use common::constants::S_IFREG;
/// use common::constants::S_IRGRP;
/// use common::constants::S_IROTH;
/// use common::constants::S_IRUSR;
/// use common::constants::S_IWUSR;
/// use common::fs::MakeDev;
/// use common::fs::mknodat;
///
/// use std::fs;
/// use std::io;
/// use std::os::unix::fs::PermissionsExt;
///
/// fn main() -> io::Result<()> {
///     let permission = fs::Permissions::from_mode(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH | S_IFREG);
///
///     // Create a new empty file with permissions -rw-r--r-- (0o644).
///     mknodat("/path/to/file", permission, MakeDev::new(12, 23))
/// }
/// ```
#[inline]
pub fn mknodat<P: AsRef<Path>>(path: P, permission: fs::Permissions, device: MakeDev) -> io::Result<()> {
	syscall! { mknodat(AT_FDCWD, cstring! { path }.as_ptr(), permission.mode(), device.0) }
}
