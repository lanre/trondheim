// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::LOCK_EX;
use crate::constants::LOCK_NB;
use crate::constants::LOCK_SH;
use crate::constants::LOCK_UN;
use crate::syscall;

use std::ffi::c_int;
use std::fs;
use std::io;
use std::os::fd::AsRawFd;

/// Flags used with [`Flock`] for acquiring advisory file locks.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct LockOperation(c_int);

#[expect(non_upper_case_globals)]
impl LockOperation {
	/// Acquire an exclusive file lock.
	pub const Exclusive: LockOperation = LockOperation(LOCK_EX);

	/// Acquire a non-blocking exclusive file lock.
	pub const ExclusiveNonBlocking: LockOperation = LockOperation(LOCK_EX | LOCK_NB);

	/// Acquire a shared file lock.
	pub const Shared: LockOperation = LockOperation(LOCK_SH);

	/// Acquire a non-blocking shared file lock.
	pub const SharedNonBlocking: LockOperation = LockOperation(LOCK_SH | LOCK_NB);

	/// Remove an existing file lock held by the current process.
	pub const Unlock: LockOperation = LockOperation(LOCK_UN);

	/// Remove an existing file lock in non-blocking mode.
	pub const UnlockNonBlocking: LockOperation = LockOperation(LOCK_UN | LOCK_NB);
}

/// **flock()** extension for [`File`].
///
/// [`File`]: std::fs::File
pub trait Flock {
	/// Safe wrapper for **flock()**, for applying or removing an advisory lock on an open
	/// file descriptor.
	///
	/// # Errors
	///
	/// This function will return an [`io::Error`] if interrupted with a signal. It may
	/// also return an error in other os-specific unspecified cases.
	///
	/// # Example
	///
	/// ```rust
	/// use common::fs::CreateFile;
	/// use common::fs::Flock;
	/// use common::fs::LockOperation;
	///
	/// use std::fs;
	/// use std::io;
	/// use std::path::Path;
	/// use std::thread;
	///
	/// fn main() -> io::Result<()> {
	///     let file = Path::new("flock_test_file");
	///
	///     // Create an empty file.
	///     let filedes = CreateFile::new().create(file)?;
	///
	///     // Gain an exclusive non-blocking file lock.
	///     filedes.flock(LockOperation::ExclusiveNonBlocking)?;
	///
	///     // Spawn another thread to gain another exclusive non-blocking lock using a different
	///     // file descriptor.
	///     thread::spawn(move || {
	///             let error = CreateFile::new().flock(LockOperation::ExclusiveNonBlocking).create(file).unwrap_err();
	///             assert_eq! { error.kind(), io::ErrorKind::WouldBlock };
	///     }).join().unwrap();
	///
	///     // Remove the previously held file lock in non-blocking mode.
	///     filedes.flock(LockOperation::UnlockNonBlocking)?;
	///
	///     // A new lock can be gained here, since the former lock was explicitly dropped.
	///     // NOTE: Locks are also dropped when the file descriptor goes out of scope.
	///     thread::spawn(move || {
	///         fs::File::options().write(true).create(true).open(file).unwrap().flock(LockOperation::Exclusive).unwrap();
	///     }).join().unwrap();
	///
	///     // So gaining a new lock here will work, even though another lock was gained
	///     // in a different thread.
	///     filedes.flock(LockOperation::Exclusive)?;
	///
	///     // Change the lock type to shared.
	///     filedes.flock(LockOperation::Shared)?;
	///
	///     // Remove the existing file lock.
	///     filedes.flock(LockOperation::Unlock)?;
	///
	///     // Gain a new lock in another thread and return the file descriptor to preserve the
	///     // lock across thread boundaries.
	///     let filedes = thread::spawn(move || CreateFile::new().flock(LockOperation::Exclusive).create(file).unwrap()).join().unwrap();
	///
	///     // Trying to create a new lock will not work since the lock from the secondary thread
	///     // will still be active.
	///     fs::File::options().write(true).create(true).open(file).unwrap().flock(LockOperation::SharedNonBlocking).map_err(|error| {
	///         assert_eq! { error.kind(), io::ErrorKind::WouldBlock };
	///     });
	///
	///     // Remove file lock.
	///     filedes.flock(LockOperation::Unlock)?;
	///
	///     // "This is fine".
	///     let filedes = fs::File::options().write(true).create(true).open(file)?;
	///     filedes.flock(LockOperation::SharedNonBlocking)?;
	///
	///     // Convert lock from shared non-blocking lock to exclusive non blocking.
	///     // NOTE: lock conversions are not guaranteed to be atomic.
	///     filedes.flock(LockOperation::ExclusiveNonBlocking)?;
	///
	///     // Remove file lock.
	///     filedes.flock(LockOperation::Unlock)?;
	///
	///     fs::remove_file(file)
	/// }
	/// ```
	fn flock(&self, mode: LockOperation) -> io::Result<()>;
}

impl Flock for fs::File {
	#[inline]
	fn flock(&self, mode: LockOperation) -> io::Result<()> {
		syscall! { flock(self.as_raw_fd(), mode.0) }
	}
}
