// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::EFAULT;
use crate::syscall;

use std::ffi::c_char;
use std::ffi::c_int;
use std::ffi::OsString;
use std::io;
use std::mem;
use std::os::unix::ffi::OsStringExt;

/// Structure used to represent information about the current kernel.
#[repr(C)]
#[derive(Clone, Copy, Debug)]
struct utsname {
	/// Operating system name.
	sysname: [c_char; 65],

	/// Name within communications network (hostname).
	nodename: [c_char; 65],

	/// Operating system release.
	release: [c_char; 65],

	/// Operating system version.
	version: [c_char; 65],

	/// Hardware type identifier.
	machine: [c_char; 65],

	/// Domain name.
	domainname: [c_char; 65],
}

/// Convert raw [`utsname`] structure fields to [`OsStrings`].
macro_rules! from_raw {
	($field: expr) => {{
		let mut vec = $field.iter().map(|v| *v as u8).collect::<Vec<u8>>();
		vec.retain(|v| *v != 0);
		OsString::from_vec(vec)
	}};
}

/// Struct returned by [`uname`] for representing information about the system kernel.
#[derive(Clone, Copy, Debug)]
pub struct UtsName(utsname);

impl UtsName {
	/// Returns an [`OsString`] containing the architecture of the CPU that is currently
	/// in use, e.g **x86_64**.
	///
	/// This value returned by this method corresponds to the **machine** field in [`UtsName`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::uname;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", uname()?.cpu_arch().to_string_lossy() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn cpu_arch(&self) -> OsString {
		from_raw! { self.0.machine }
	}

	/// Returns an [`OsString`] containing the device domain name.
	///
	/// This value returned by this method corresponds to the **domainname** field in [`UtsName`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::uname;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", uname()?.domainname().to_string_lossy() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn domainname(&self) -> OsString {
		from_raw! { self.0.domainname }
	}

	/// Returns an [`OsString`] containing the device hostname.
	///
	/// This value returned by this method corresponds to the **nodename** field in [`UtsName`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::uname;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", uname()?.hostname().to_string_lossy() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn hostname(&self) -> OsString {
		from_raw! { self.0.nodename }
	}

	/// Returns an [`OsString`] containing the operating system name, e.g **Linux**.
	///
	/// This value returned by this method corresponds to the **sysname** field in [`UtsName`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::uname;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", uname()?.os_name().to_string_lossy() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn os_name(&self) -> OsString {
		from_raw! { self.0.sysname }
	}

	/// Returns an [`OsString`] containing the operating system version, e.g **6.11.59_1**.
	///
	/// This value returned by this method corresponds to the **release** field in [`UtsName`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::uname;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", uname()?.os_version().to_string_lossy() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn os_version(&self) -> OsString {
		from_raw! { self.0.release }
	}

	/// Returns an [`OsString`] containing the operating system release info, e.g
	/// **#1 SMP PREEMPT_DYNAMIC Sun Jan 15 23:15:56 UTC 2096**
	///
	/// This value returned by this method corresponds to the **version** field in [`UtsName`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::uname;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", uname()?.os_release().to_string_lossy() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn os_release(&self) -> OsString {
		from_raw! { self.0.version }
	}
}

/// Safe wrapper for **uname()** for retrieving information about the current kernel.
///
/// This function will return a [`UtsName`] struct, containing relevant information about
/// the current device.
///
/// # Errors
///
/// This function will return an [`io::Error`], if an error is encountered while fetching
/// system information.
///
/// # Example
///
/// ```rust
/// use common::sys::uname;
///
/// use std::io;
/// use std::ffi::OsString;
///
/// fn main() -> io::Result<()> {
///     let sysinfo = uname()?;
///
///     assert_eq! {
///         sysinfo.os_name(),
///         OsString::from("Linux")
///     };
///     assert_eq! {
///         sysinfo.cpu_arch(),
///         OsString::from("x86_64")
///     };
///
///     Ok(())
/// }
/// ```
#[inline]
pub fn uname() -> io::Result<UtsName> {
	let mut buf = mem::MaybeUninit::<utsname>::uninit();

	syscall! { uname(buf.as_mut_ptr()) }?;

	if buf.as_ptr().is_null() {
		return Err(io::Error::from_raw_os_error(EFAULT));
	}

	// SAFETY: If we get here, then `buf` was initialized and is non-null.
	Ok(UtsName(unsafe { buf.assume_init() }))
}
