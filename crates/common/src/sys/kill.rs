// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::SIGABRT;
use crate::constants::SIGCONT;
use crate::constants::SIGINT;
use crate::constants::SIGKILL;
use crate::constants::SIGPIPE;
use crate::constants::SIGQUIT;
use crate::constants::SIGSEGV;
use crate::constants::SIGSTOP;
use crate::constants::SIGTERM;
use crate::syscall;

use std::ffi::c_int;
use std::io;

/// Flags used with [`kill`].
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Signal(c_int);

#[expect(non_upper_case_globals)]
impl Signal {
	/// [`SIGABRT`], Abort process.
	pub const AbortProcess: Signal = Signal(SIGABRT);

	/// [`SIGPIPE`], Broken pipe signal.
	pub const BrokenPipe: Signal = Signal(SIGPIPE);

	/// [`SIGCONT`], Resume process if stopped.
	pub const ContinueProcess: Signal = Signal(SIGCONT);

	/// Send an empty signal to the process to verify its existence.
	pub const DebugProcess: Signal = Signal(0);

	/// [`SIGINT`], Interrupt process.
	pub const InterruptProcess: Signal = Signal(SIGINT);

	/// [`SIGKILL`], Kill signal.
	pub const KillProcess: Signal = Signal(SIGKILL);

	/// [`SIGQUIT`], Quit process.
	pub const QuitProcess: Signal = Signal(SIGQUIT);

	/// [`SIGSEGV`], Segmentation fault.
	pub const SegmentationFault: Signal = Signal(SIGSEGV);

	/// [`SIGSTOP`], Stop process.
	pub const StopProcess: Signal = Signal(SIGSTOP);

	/// [`SIGTERM`], Termination signal.
	pub const TerminateProcess: Signal = Signal(SIGTERM);

	/// Create a new [`Signal`] instance from a particular system signal.
	///
	/// # Example
	///
	/// ```rust
	/// use common::sys::Signal;
	///
	/// assert_eq! { Signal::DebugProcess, Signal::from_raw_signal(0) };
	/// ```
	#[inline]
	#[must_use]
	pub const fn from_raw_signal(signal: i32) -> Signal {
		Signal(signal)
	}
}

/// Safe wrapper for **kill()**, for sending a signal to a process group or process.
///
/// # Errors
///
/// This function will return an [`io::Error`], if the specified **PID** is not valid. It may
/// also return an error in other os-specific unspecified cases.
///
/// # Example
///
/// ```no_run
/// use common::sys::kill;
/// use common::sys::Signal;
///
/// use std::io;
/// use std::process;
///
/// fn main() -> io::Result<()> {
///     let pid = i32::try_from(process::id()).unwrap_or(23323);
///
///     // Send `SIGKILL` to process.
///     kill(pid, Signal::KillProcess)?;
///
///     // Send `SIGTERM` to process.
///     kill(pid, Signal::TerminateProcess)?;
///
///     // Send `SIGSTOP` to process.
///     kill(pid, Signal::StopProcess)?;
///
///     // Send `SIGCONT` to process.
///     kill(pid, Signal::ContinueProcess)?;
///
///     // Send an empty signal to the process, to verify its exisitence.
///     kill(pid, Signal::DebugProcess)
/// }
/// ```
#[inline]
pub fn kill(pid: i32, signal: Signal) -> io::Result<()> {
	syscall! { kill(pid, signal.0) }
}
