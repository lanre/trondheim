// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::syscall;

use std::ffi::c_int;

/// Safe wrapper for **getpagesize()**, for getting memory page size.
///
/// # Example
///
/// ```rust
/// use common::sys::getpagesize;
///
/// assert_eq! { getpagesize(), 4096 };
/// ```
#[inline]
#[must_use]
pub fn getpagesize() -> i32 {
	syscall! { getpagesize(), c_int }
}
