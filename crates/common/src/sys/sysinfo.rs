// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::constants::EFAULT;
use crate::syscall;

use std::ffi::c_char;
use std::ffi::c_int;
use std::ffi::c_long;
use std::ffi::c_uint;
use std::ffi::c_ulong;
use std::ffi::c_ushort;
use std::io;
use std::mem;

/// Structure used to represent system information.
#[repr(C)]
#[derive(Clone, Copy, Debug)]
struct sysinfo {
	/// Seconds since boot.
	uptime: c_long,

	/// 1, 5, and 15 minute load averages.
	loads: [c_ulong; 3],

	/// Total usable main memory size.
	totalram: c_ulong,

	/// Available memory size.
	freeram: c_ulong,

	/// Amount of shared memory.
	sharedram: c_ulong,

	/// Memory used by buffers.
	bufferram: c_ulong,

	/// Total swap space size.
	totalswap: c_ulong,

	/// swap space still available.
	freeswap: c_ulong,

	/// Number of current processes.
	procs: c_ushort,

	/// Explicit padding for m68k.
	pad: c_ushort,

	/// Total high memory size.
	totalhigh: c_ulong,

	/// Available high memory size.
	freehigh: c_ulong,

	/// Memory unit size in bytes.
	mem_unit: c_uint,

	/// Pads structure to 64 bytes.
	_f: [c_char; 20 - (2 * mem::size_of::<c_ulong>()) - mem::size_of::<c_uint>()],
}

/// Struct returned by [`sysinfo`] for representing system information.
///
/// [`sysinfo`]: crate::sys::sysinfo()
#[derive(Clone, Copy, Debug)]
pub struct SysInfo(sysinfo);

impl SysInfo {
	/// Returns a [`u16`] containing the number of running processes.
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::sysinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", sysinfo()?.active_processes() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub const fn active_processes(&self) -> u16 {
		self.0.procs
	}

	/// Returns a [`u64`] containing the number (in bytes) of memory used by buffers.
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::sysinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let sysinfo = sysinfo()?;
	///     stdoutln! { "{}", sysinfo.buffer_ram() / sysinfo.mem_unit() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub const fn buffer_ram(&self) -> u64 {
		self.0.bufferram
	}

	/// Returns a [`u64`] containing the amount (in bytes) of free high memory.
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::sysinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let sysinfo = sysinfo()?;
	///     stdoutln! { "{}", sysinfo.free_highmem() / sysinfo.mem_unit() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub const fn free_highmem(&self) -> u64 {
		self.0.freehigh
	}

	/// Returns a [`u64`] containing the number (in bytes) of free memory.
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::sysinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let sysinfo = sysinfo()?;
	///     stdoutln! { "{}", sysinfo.free_ram() / sysinfo.mem_unit() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub const fn free_ram(&self) -> u64 {
		self.0.freeram
	}

	/// Returns a [`u64`] containing the number (in bytes) of free swap space.
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::sysinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let sysinfo = sysinfo()?;
	///     stdoutln! { "{}", sysinfo.free_swap() / sysinfo.mem_unit() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub const fn free_swap(&self) -> u64 {
		self.0.freeswap
	}

	/// Returns a slice containing the system load averages over the past 1, 5 and
	/// 15 minutes.
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::sysinfo;
	///
	/// use std::fmt::Write;
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let load_averages = sysinfo()?.load_average().into_iter().fold(String::new(), |mut res, load| {
	///         let _ = write! { res, "{:.02} ", load as f64 / 65536.0 };
	///         res
	///     });
	///
	///     // Will print the load averages in the format: 1 minute, 5 minute, 15 minute.
	///     stdoutln! { "{}", load_averages };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub const fn load_average(&self) -> [u64; 3] {
		self.0.loads
	}

	/// Returns a [`u64`] containing the memory unit size in bytes. Use this value
	/// to divide the results of [`SysInfo`] memory related methods to extract the
	/// actual memory size.
	#[inline]
	#[must_use]
	pub fn mem_unit(&self) -> u64 {
		u64::from(self.0.mem_unit)
	}

	/// Returns a [`u64`] containing the number (in bytes) of shared memory.
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::sysinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let sysinfo = sysinfo()?;
	///     stdoutln! { "{}", sysinfo.shared_ram() / sysinfo.mem_unit() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub const fn shared_ram(&self) -> u64 {
		self.0.sharedram
	}

	/// Returns a [`u64`] containing the amount (in bytes) of total high memory.
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::sysinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let sysinfo = sysinfo()?;
	///     stdoutln! { "{}", sysinfo.total_highmem() / sysinfo.mem_unit() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub const fn total_highmem(&self) -> u64 {
		self.0.totalhigh
	}

	/// Returns a [`u64`] containing the number (in bytes) of total usable memory.
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::sysinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let sysinfo = sysinfo()?;
	///     stdoutln! { "{}", sysinfo.total_ram() / sysinfo.mem_unit() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub const fn total_ram(&self) -> u64 {
		self.0.totalram
	}

	/// Returns a [`u64`] containing the number (in bytes) of total swap space.
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::sysinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let sysinfo = sysinfo()?;
	///     stdoutln! { "{}", sysinfo.total_swap() / sysinfo.mem_unit() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub const fn total_swap(&self) -> u64 {
		self.0.totalswap
	}

	/// Returns an [`i64`] containing the amount of time (in seconds) since boot.
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::sysinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     let uptime = sysinfo()?.uptime();
	///
	///     /// Returns the system uptime in the format: D, H, M, S
	///     stdoutln! { "{}, {}, {}, {}", uptime / 86400, (uptime / 3600) % 24, (uptime / 60) % 60, uptime % 60 };
	///     Ok(())
	/// }
	#[inline]
	#[must_use]
	pub const fn uptime(&self) -> i64 {
		self.0.uptime
	}
}

/// Safe wrapper for **sysinfo()** for retrieving system information.
///
/// This function will return a [`SysInfo`] struct containing relevant information about
/// the current device.
///
/// # Errors
///
/// This function will return an [`io::Error`], if an error is encountered while fetching
/// system information.
///
/// # Example
///
/// ```rust
/// use common::sys::sysinfo;
///
/// use std::io;
/// use std::ffi::OsString;
///
/// fn main() -> io::Result<()> {
///     let sysinfo = sysinfo()?;
///     assert! { sysinfo.total_ram() > sysinfo.free_ram() };
///     Ok(())
/// }
/// ```
#[inline]
pub fn sysinfo() -> io::Result<SysInfo> {
	let mut info = mem::MaybeUninit::<sysinfo>::uninit();

	syscall! { sysinfo(info.as_mut_ptr()) }?;

	if info.as_ptr().is_null() {
		return Err(io::Error::from_raw_os_error(EFAULT));
	}

	// SAFETY: If we get here, then `info` was initialized and is non-null.
	Ok(SysInfo(unsafe { info.assume_init() }))
}
