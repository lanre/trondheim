// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#[cfg(target_os = "android")]
use crate::constants::EOVERFLOW;

use crate::constants::EINVAL;
use crate::fs::readfile;
use crate::sys::getgid;

use std::io;

/// Create a [`String`] from the specified password database entries, returning **(none)** where
/// applicable.
macro_rules! replace {
	($field: expr) => {
		String::from(if $field.is_empty() { "(none)" } else { *$field })
	};
}

/// Struct returned by [`userinfo`] for representing user information extracted from the passwd file.
#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct UserInfo {
	/// Username.
	//
	/// This field corresponds to the `pw_name` field in the `passwd` struct.
	user_name: String,

	/// User password.
	//
	/// This field corresponds to the `pw_passwd` field in the `passwd` struct.
	password: String,

	/// User ID.
	//
	/// This field corresponds to the `pw_uid` field in the `passwd` struct.
	uid: u32,

	/// Group ID.
	//
	/// This field corresponds to the `pw_gid` field in the `passwd` struct.
	gid: u32,

	/// User information or comment.
	//
	/// This field corresponds to the `pw_gecos` field in the `passwd` struct.
	comment: String,

	/// Home directory.
	//
	/// This field corresponds to the `pw_dir` field in the `passwd` struct.
	home_dir: String,

	/// Shell program.
	//
	/// This field corresponds to the `pw_shell` field in the `passwd` struct.
	shell: String,
}

impl UserInfo {
	/// Returns a [`String`] containing extra user information.
	///
	/// This value returned by this method corresponds to the **comment** field in [`UserInfo`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::getuid;
	/// use common::sys::userinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", userinfo(getuid())?.comment() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn comment(&self) -> String {
		self.comment.clone()
	}

	/// Returns the GID of the specified user as a [`u32`].
	///
	/// This value returned by this method corresponds to the **gid** field in [`UserInfo`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::getuid;
	/// use common::sys::userinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", userinfo(getuid())?.gid() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub const fn gid(&self) -> u32 {
		self.gid
	}

	/// Returns a [`String`] containing the path to the home directory of the specified user.
	///
	/// This value returned by this method corresponds to the **home_dir** field in [`UserInfo`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::getuid;
	/// use common::sys::userinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", userinfo(getuid())?.home_directory() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn home_directory(&self) -> String {
		self.home_dir.clone()
	}

	/// Returns a [`String`] representation of the password of the specified user.
	///
	/// This value returned by this method corresponds to the **password** field in [`UserInfo`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::getuid;
	/// use common::sys::userinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", userinfo(getuid())?.password() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn password(&self) -> String {
		self.password.clone()
	}

	/// Returns a [`String`] to the shell program of the specified user.
	///
	/// This value returned by this method corresponds to the **shell** field in [`UserInfo`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::getuid;
	/// use common::sys::userinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", userinfo(getuid())?.shell() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn shell(&self) -> String {
		self.shell.clone()
	}

	/// Returns the UID of the specified user as a [`u32`].
	///
	/// This value returned by this method corresponds to the **uid** field in [`UserInfo`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::getuid;
	/// use common::sys::userinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", userinfo(getuid())?.uid() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub const fn uid(&self) -> u32 {
		self.uid
	}

	/// Returns a [`String`] containing the login name of the specified user.
	///
	/// This value returned by this method corresponds to the **user_name** field in [`UserInfo`].
	///
	/// # Example
	///
	/// ```rust
	/// use common::stdoutln;
	/// use common::sys::getuid;
	/// use common::sys::userinfo;
	///
	/// use std::io;
	///
	/// fn main() -> io::Result<()> {
	///     stdoutln! { "{}", userinfo(getuid())?.username() };
	///     Ok(())
	/// }
	/// ```
	#[inline]
	#[must_use]
	pub fn username(&self) -> String {
		self.user_name.clone()
	}
}

/// Retrieve information about the specified user.
///
/// This function will return a [`UserInfo`] struct containing relevant information about
/// the specified user.
///
/// # Errors
///
/// This function will return an [`io::Error`], if an error is encountered while fetching
/// user information.
///
/// # Example
///
/// ```rust
/// use common::sys::getuid;
/// use common::sys::userinfo;
///
/// use std::io;
///
/// fn main() -> io::Result<()> {
///     // Retreive information about the current user.
///     let userinfo = userinfo(getuid())?;
///
///     assert_eq! { userinfo.gid(), 1000 };
///     assert_eq! { userinfo.uid(), 1000 };
///     Ok(())
/// }
/// ```
#[cfg(target_os = "android")]
#[inline]
pub fn userinfo(uid: u32) -> io::Result<UserInfo> {
	let mut passwd_file: Vec<String> = Vec::with_capacity(10);
	
	// Collect all available password database entries.
	for files in ["/etc/passwd", "/odm/etc/passwd", "/product/etc/passwd", "/system_ext/etc/passwd", "/vendor/etc/passwd"] {
		if let Ok(entries) = readfile(files) {
			let parsed_entries = entries.to_lossy_string()
				.lines()
				.filter(|line| line.contains(&format! { ":x:{}:", uid }) || line.contains(&format! { "::{}:", uid }))
				.map(String::from)
				.collect::<Vec<String>>();

			if !parsed_entries.is_empty() {
				passwd_file.extend(parsed_entries);
			}
		}
	}

	if !passwd_file.is_empty() {
		let parsed_info = passwd_file
			.first()
			.ok_or_else(|| io::Error::from_raw_os_error(EINVAL))?
			.split(':')
			.collect::<Vec<&str>>();

		let Some([username, password, raw_uid, raw_gid, comment, home_dir, shell]) = parsed_info.get(0..=6) else {
			return Err(io::Error::from_raw_os_error(EINVAL));
		};

		return Ok(UserInfo {
			user_name: replace! { username },
			password:  replace! { password },
			uid:       raw_uid.trim().parse::<u32>().unwrap_or(uid),
			gid:       raw_gid.trim().parse::<u32>().unwrap_or(getgid()),
			comment:   replace! { comment },
			home_dir:  replace! { home_dir },
			shell:     replace! { shell },
		});
	}

	// Valid vendor IDs should have already been parsed.
	if (2900..=2999).contains(&uid) || (5000..5999).contains(&uid) {
		return Err(io::Error::from_raw_os_error(EINVAL));
	}

	// Return app ID info.
	if (10000..=19999).contains(&uid) {
		return Ok(UserInfo {
			user_name: format! { "u0_a{}", uid.checked_rem(10000).ok_or_else(|| io::Error::from_raw_os_error(EOVERFLOW))? },
			password:  String::from("(none)"),
			uid,
			gid:       getgid(),
			comment:   String::from("(none)"),
			home_dir:  String::from("/data"),
			shell:     String::from("/system/bin/sh"),
		});
	}

	// Root, system or shell info.
	if uid == 0 || uid == 2000 || uid == 1000 {
		return Ok(UserInfo {
			user_name: String::from(if uid == 0 { "root" } else if uid == 1000 { "system" } else { "shell" }),
			password:  String::from("(none)"),
			uid,
			gid:       getgid(),
			comment:   String::from("(none)"),
			home_dir:  String::from("/"),
			shell:     String::from("/system/bin/sh"),
		});
	}

	// Return an error for all other UIDs.
	Err(io::Error::from_raw_os_error(EINVAL))
}

/// Retrieve information about the specified user.
///
/// This function will return a [`UserInfo`] struct containing relevant information about
/// the specified user.
///
/// # Errors
///
/// This function will return an [`io::Error`] if an error is encountered while fetching
/// user information.
///
/// # Example
///
/// ```rust
/// use common::sys::getuid;
/// use common::sys::userinfo;
///
/// use std::io;
///
/// fn main() -> io::Result<()> {
///     // Retreive information about the current user.
///     let userinfo = userinfo(getuid())?;
///
///     assert_eq! { userinfo.gid(), 1000 };
///     assert_eq! { userinfo.uid(), 1000 };
///     Ok(())
/// }
/// ```
#[cfg(not(target_os = "android"))]
#[inline]
pub fn userinfo(uid: u32) -> io::Result<UserInfo> {
	// Parse user database file.
	let passwd_file = readfile("/etc/passwd")?
		.to_lossy_string()
		.lines()
		.filter(|line| line.contains(&format! { ":x:{}:", uid }) || line.contains(&format! { "::{}:", uid }))
		.map(String::from)
		.collect::<Vec<String>>();

	// Parse extracted entry.
	let parsed_info = passwd_file
		.first()
		.ok_or_else(|| io::Error::from_raw_os_error(EINVAL))?
		.split(':')
		.collect::<Vec<&str>>();

	let Some([username, password, raw_uid, raw_gid, comment, home_dir, shell]) = parsed_info.get(0..=6) else {
		return Err(io::Error::from_raw_os_error(EINVAL));
	};

	Ok(UserInfo {
		user_name: replace! { username },
		password:  replace! { password },
		uid:       raw_uid.trim().parse::<u32>().unwrap_or(uid),
		gid:       raw_gid.trim().parse::<u32>().unwrap_or(getgid()),
		comment:   replace! { comment },
		home_dir:  replace! { home_dir },
		shell:     replace! { shell },
	})
}
