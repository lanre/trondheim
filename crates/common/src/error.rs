// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::stderrln;

use std::error;
use std::fmt;
use std::io;
use std::process;

/// Exit code returned by **`trondheim`** and its built-in programs.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum ExitCode {
	/// Exit code for unsuccessful program termination.
	Failure,

	/// Custom exit code constructed from an arbitrary [`i32`] value.
	Other(i32),

	/// Exit code for successful program termination.
	Success,
}

impl fmt::Display for ExitCode {
	#[inline]
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write! { f, "{}", i32::from(self.to_owned()) }
	}
}

impl From<i32> for ExitCode {
	#[inline]
	fn from(value: i32) -> ExitCode {
		ExitCode::Other(value)
	}
}

impl From<ExitCode> for i32 {
	#[inline]
	fn from(value: ExitCode) -> i32 {
		match value {
			ExitCode::Failure  => 1,
			ExitCode::Other(v) => v,
			ExitCode::Success  => 0,
		}
	}
}

/// Error type used across **`trondheim`** and its built-in programs.
#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Error {
	/// The exit code of the error.
	exitcode: ExitCode,

	/// The error message.
	message: String,

	/// The errno of the error.
	os_error: Option<i32>,
}

impl Error {
	/// Creates a new [`Error`] instance.
	#[inline]
	#[must_use]
	pub fn new<X: Into<ExitCode>, T: Into<String>>(exitcode: X, message: T) -> Error {
		Error {
			exitcode: exitcode.into(),
			message:  message.into(),
			os_error: None,
		}
	}

	/// Returns the [`ExitCode`] related to the [`Error`].
	#[inline]
	#[must_use]
	pub const fn exitcode(&self) -> ExitCode {
		self.exitcode
	}

	/// Returns a [`String`] containing the error message of the [`Error`].
	#[inline]
	#[must_use]
	pub fn message(&self) -> String {
		self.message.clone()
	}

	/// Returns a [`bool`] indicating whether the [`Error`] was constructed from an [`io::Error`].
	#[inline]
	#[must_use]
	pub const fn has_errno(&self) -> bool {
		self.os_error.is_some()
	}

	/// Returns the **`errno`** related to the [`Error`].
	#[inline]
	#[must_use]
	pub const fn errno(&self) -> Option<i32> {
		self.os_error
	}
}

impl error::Error for Error {}

impl fmt::Display for Error {
	#[inline]
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		self.message.fmt(f)
	}
}

impl From<Error> for io::Error {
	#[inline]
	fn from(error: Error) -> io::Error {
		error.errno().map_or_else(|| io::Error::other(error.message()), io::Error::from_raw_os_error)
	}
}

impl From<io::Error> for Error {
	#[inline]
	fn from(error: io::Error) -> Error {
		Error {
			exitcode: ExitCode::Failure,
			message:  error.to_string(),
			os_error: error.raw_os_error(),
		}
	}
}

impl process::Termination for Error {
	#[inline]
	fn report(self) -> process::ExitCode {
		if !self.message.is_empty() {
			stderrln! { "{}", self.message() };
		}

		match self.exitcode {
			ExitCode::Failure  => process::ExitCode::FAILURE,
			ExitCode::Other(v) => process::ExitCode::from(u8::try_from(v).unwrap_or(1)),
			ExitCode::Success  => process::ExitCode::SUCCESS,
		}
	}
}
