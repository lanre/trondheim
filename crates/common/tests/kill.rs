// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::EPERM;
use common::constants::ESRCH;
use common::sys::kill;
use common::sys::Signal;

use std::io;

#[test]
fn test_kill() -> io::Result<()> {
	// Cannot check PID 1, since the user lacks sufficient permissions.
	assert_eq! { kill(1, Signal::DebugProcess).unwrap_err().raw_os_error(), Some(EPERM) }

	// Process should not exist.
	assert_eq! { kill(184734348, Signal::from_raw_signal(0)).unwrap_err().raw_os_error(), Some(ESRCH) }

	// Send `SIGCONT` to the current process.
	kill(std::process::id() as i32, Signal::ContinueProcess)
}
