// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::time::HumanTime;

#[test]
fn humantime_test() {
	assert_eq! {
		"2024-09-24 11:37:39 +0000",
		HumanTime::from_raw_seconds(1727177859).to_utc_string()
	};
	assert_eq! {
		"2024-12-02 08:27:10 +0000",
		HumanTime::from_raw_seconds(1733128030).to_utc_string()
	};
	assert_eq! {
		"2525-06-20 22:35:00 +0000",
		HumanTime::from_raw_seconds(17528913300).to_utc_string()
	};
	assert_eq! {
		"1901-12-13 20:45:52 +0000",
		HumanTime::from_raw_seconds(i32::MIN.into()).to_utc_string()
	};
	assert_eq! {
		"2038-01-19 03:14:07 +0000",
		HumanTime::from_raw_seconds(i32::MAX.into()).to_utc_string()
	};
	assert_eq! {
		"2011-08-21 05:37:06 +0000",
		HumanTime::from_raw_seconds(1313905026).to_utc_string()
	};
	assert_eq! {
		"1960-03-31 23:34:23 +0000",
		HumanTime::from_raw_seconds(-307758337).to_utc_string()
	};
	assert_eq! {
		"1960-04-01 00:34:23 +0000",
		HumanTime::from_raw_seconds(-307754737).to_utc_string()
	};
	assert_eq! {
		"1976-06-20 22:35:00 +0000",
		HumanTime::from_raw_seconds(204158100).to_utc_string()
	};
	assert_eq! {
		"2022-12-31 23:59:59 +0000",
		HumanTime::from_raw_seconds(1672531199).to_utc_string()
	};
	assert_eq! {
		"2023-01-01 00:00:00 +0000",
		HumanTime::from_raw_seconds(1672531200).to_utc_string()
	};
	assert_eq! {
		"2024-02-29 12:34:56 +0000",
		HumanTime::from_raw_seconds(1709210096).to_utc_string()
	};
	assert_eq! {
		"36812-02-20 00:36:15 +0000",
		HumanTime::from_raw_seconds(1099511627775).to_utc_string()
	};
	assert_eq! {
		"-001-12-31 23:59:59 +0000",
		HumanTime::from_raw_seconds(-62167219201).to_utc_string()
	};
	assert_eq! {
		"1141709097-06-13 06:26:07 +0000",
		HumanTime::from_raw_seconds(36028797018963967).to_utc_string()
	};
	assert_eq! {
		"1141709097-06-13 06:26:08 +0000",
		HumanTime::from_raw_seconds(36028797018963968).to_utc_string()
	};
	assert_eq! {
		"-1141705158-07-20 17:33:52 +0000",
		HumanTime::from_raw_seconds(-36028797018963968).to_utc_string()
	};
}
