// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::EINVAL;
use common::constants::ENOENT;
use common::constants::EOPNOTSUPP;
use common::constants::S_IFREG;
use common::constants::S_IRGRP;
use common::constants::S_IROTH;
use common::constants::S_IRUSR;
use common::constants::S_IRWXG;
use common::constants::S_IRWXO;
use common::constants::S_IRWXU;
use common::constants::S_ISGID;
use common::constants::S_ISUID;
use common::constants::S_ISVTX;
use common::constants::S_IWGRP;
use common::constants::S_IWOTH;
use common::constants::S_IWUSR;
use common::constants::S_IXGRP;
use common::constants::S_IXOTH;
use common::constants::S_IXUSR;
use common::fs::CreateFile;
use common::fs::fchmodat;
use common::fs::FchmodFlags;

use std::fs;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;

#[test]
fn test_fchmodat_empty_path() {
	assert_eq! {
		fchmodat("", fs::Permissions::from_mode(0o644), FchmodFlags::Follow).unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
	assert_eq! {
		fchmodat("\0", fs::Permissions::from_mode(0o644), FchmodFlags::Follow).unwrap_err().raw_os_error(),
		Some(EINVAL)
	};
}

#[test]
fn test_fchmodat() {
	// Create an empty file.
	let file = Path::new("fchmodat_test_file");

	let fildes = CreateFile::new().create(file).unwrap();
	assert_eq! { fildes.metadata().unwrap().permissions().mode(), 0o644 | S_IFREG };

	// Change file permission to -rw-rw-rw-. This will follow symbolic links, modifying the target file.
	let permission = fs::Permissions::from_mode(S_IWUSR | S_IRUSR | S_IWGRP | S_IRGRP | S_IWOTH | S_IROTH);

	fchmodat(file, permission, FchmodFlags::Follow).unwrap();
	assert_eq! { fildes.metadata().unwrap().permissions().mode(), 0o666 | S_IFREG };

	//////////////////////////////////////////////////////////////////

	// Change file permission to -r--------.
	let permission = fs::Permissions::from_mode(S_IRUSR);

	fchmodat(file, permission, FchmodFlags::Follow).unwrap();
	assert_eq! { fildes.metadata().unwrap().permissions().mode(), 0o400 | S_IFREG };

	//////////////////////////////////////////////////////////////////

	// Change file permission to ---x--x--x.
	let permission = fs::Permissions::from_mode(S_IXUSR | S_IXGRP | S_IXOTH);

	fchmodat(file, permission, FchmodFlags::Follow).unwrap();
	assert_eq! { fildes.metadata().unwrap().permissions().mode(), 0o111 | S_IFREG };

	//////////////////////////////////////////////////////////////////

	// Change file permission to -rwxrwxrwx.
	let permission = fs::Permissions::from_mode(S_IRWXU | S_IRWXO | S_IRWXG);

	fchmodat(file, permission, FchmodFlags::Follow).unwrap();
	assert_eq! { fildes.metadata().unwrap().permissions().mode(), 0o777 | S_IFREG };

	//////////////////////////////////////////////////////////////////

	// Change file permission to -rwxrwxrwt.
	let permission = fs::Permissions::from_mode(S_IRWXU | S_IRWXO | S_IRWXG | S_ISVTX);

	fchmodat(file, permission, FchmodFlags::Follow).unwrap();
	assert_eq! { fildes.metadata().unwrap().permissions().mode(), 0o777 | S_IFREG | S_ISVTX };

	//////////////////////////////////////////////////////////////////

	// Change file permission to -rwxrwsrwx.
	let permission = fs::Permissions::from_mode(S_IRWXU | S_IRWXO | S_IRWXG | S_ISGID);

	fchmodat(file, permission, FchmodFlags::Follow).unwrap();
	assert_eq! { fildes.metadata().unwrap().permissions().mode(), 0o777 | S_IFREG | S_ISGID };

	//////////////////////////////////////////////////////////////////

	// Change file permission to -rwsrwxrwx.
	let permission = fs::Permissions::from_mode(S_IRWXU | S_IRWXO | S_IRWXG | S_ISUID);

	fchmodat(file, permission, FchmodFlags::Follow).unwrap();
	assert_eq! { fildes.metadata().unwrap().permissions().mode(), 0o777 | S_IFREG | S_ISUID };

	//////////////////////////////////////////////////////////////////

	// To change file permission to -rw-r--r--. This will not follow symbolic
	// links, modifying `file` itself. This option is only available on some
	// operating systems and will return a `ENOTSUP` or `EOPNOTSUPP` on Linux
	// for instance.
	let permission = fs::Permissions::from_mode(S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH);

	match fchmodat(file, permission, FchmodFlags::NoFollow) {
		Err(error) => {
			assert_eq! { error.raw_os_error(), Some(EOPNOTSUPP) };
		},
		Ok(()) => {
			assert_eq! { fildes.metadata().unwrap().permissions().mode(), 0o644 | S_IFREG };
		},
	}

	fs::remove_file(file).unwrap();
}
