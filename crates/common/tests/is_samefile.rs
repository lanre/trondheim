// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::EACCES;
use common::constants::ENOENT;
use common::fs::is_samefile;
use common::fs::IsSameFile;

use std::fs;
use std::os::unix::fs::symlink;
use std::path::Path;
use std::path::PathBuf;

#[test]
fn test_is_samefile_basic() {
	// Immutable file
	let (destination, source) = ("/proc/cpuinfo", "/proc/cpuinfo");
	assert! { is_samefile(source, destination).unwrap() };

	// File
	let (destination, source) = ("/bin/bash", "/bin/bash");
	assert! { is_samefile(source, destination).unwrap() };

	// Immutable directory
	let (destination, source) = ("/proc/sys", "/proc/sys");
	assert! { is_samefile(source, destination).unwrap() };

	// Directory
	let (destination, source) = ("/usr/local", "/usr/local");
	assert! { is_samefile(source, destination).unwrap() };
}

#[test]
fn test_is_samefile_basic_path() {
	// Immutable file
	let destination = "/proc/cpuinfo";
	assert! { Path::new("/proc/cpuinfo").is_samefile(destination).unwrap() };

	// File
	let destination = Path::new("/bin/bash");
	assert! { PathBuf::from("/bin/bash").is_samefile(destination).unwrap() };

	// Immutable directory
	let destination = PathBuf::from("/proc/sys");
	assert! { Path::new("/proc/sys").is_samefile(destination).unwrap() };

	// Directory
	let destination = "/usr/local";
	assert! { PathBuf::from("/usr/local").is_samefile(destination).unwrap() };
}

#[test]
fn test_is_samefile_basic_err() {
	let (destination, source) = ("/proc/cpuinfoooooooooo", "/proc/cpuinfo");
	assert_eq! { is_samefile(source, destination).unwrap_err().raw_os_error(), Some(ENOENT) };

	let (destination, source) = ("/proc/cpuinfo", "/proc/cpuinfoooooooooo");
	assert_eq! { is_samefile(source, destination).unwrap_err().raw_os_error(), Some(ENOENT) };

	// Tries to create destination and fails
	let (destination, source) = ("/root/bar", "/root/foo");
	assert_eq! { is_samefile(source, destination).unwrap_err().raw_os_error(), Some(EACCES) };
}

#[test]
fn test_is_samefile_basic_false() {
	let (destination, source) = ("/proc/meminfo", "/proc/cpuinfo");
	assert! { !is_samefile(source, destination).unwrap() };

	let (destination, source) = ("/sys/bus", "/proc/sys");
	assert! { !is_samefile(source, destination).unwrap() };
}

#[test]
fn test_is_samefile_normalize() {
	let (destination, source) = ("/../../proc/../proc/../proc/cpuinfo", "/proc/cpuinfo");
	assert! { is_samefile(source, destination).unwrap() };

	let (destination, source) = ("/../proc/cpuinfo", "/../proc/../proc/cpuinfo");
	assert! { is_samefile(source, destination).unwrap() };

	let (destination, source) = ("/../sys/../sys/bus/cpu", "/../sys/../sys/bus/../bus/cpu");
	assert! { is_samefile(source, destination).unwrap() };
}

#[test]
fn test_is_samefile_normalize_false() {
	let (destination, source) = ("/../../proc/../proc/../proc/meminfo", "/proc/cpuinfo");
	assert! { !is_samefile(source, destination).unwrap() };

	let (destination, source) = ("/../proc/cpuinfo", "/../proc/../proc/meminfo");
	assert! { !is_samefile(source, destination).unwrap() };
}

#[test]
fn test_is_samefile_normalize_err() {
	// Tries to create destination and fails
	let (destination, source) = ("/../../sys/sys/class/hwmon", "/sys/class/hwmon/../hwmon/../../../sys/class/hwmon");
	assert_eq! { is_samefile(source, destination).unwrap_err().raw_os_error(), Some(EACCES) };

	// Tries to `stat()` and `lstat()` source and fails
	let (destination, source) = ("/sys/class/hwmon/../hwmon/../../../sys/class/hwmon", "/../../sys/sys/class/hwmon");
	assert_eq! { is_samefile(source, destination).unwrap_err().raw_os_error(), Some(ENOENT) };

	// Tries to `stat()` and `lstat()` source and fails
	let destination = "/sys/class/hwmon/../hwmon/../../../sys/class/hwmon";
	assert_eq! { Path::new("/../../sys/sys/class/hwmon").is_samefile(destination).unwrap_err().raw_os_error(), Some(ENOENT) };
}

#[test]
fn test_is_samefile_canonicalize() {
	let (destination, source) = ("/sys/bus/cpu", "/sys/devices/system/cpu/cpu0/subsystem");
	assert! { is_samefile(source, destination).unwrap() };

	let (destination, source) = ("/../../sys/../sys/class/hwmon", "/sys/class/hwmon/../hwmon/../../../sys/class/hwmon");
	assert! { is_samefile(source, destination).unwrap() };

	let (destination, source) = ("/sys/class/hwmon/../hwmon/../../../sys/class/hwmon", "/../../sys/../sys/class/hwmon");
	assert! { is_samefile(source, destination).unwrap() };

	let destination = "/sys/class/hwmon/../hwmon/../../../sys/class/hwmon";
	assert! { Path::new("/../../sys/../sys/class/hwmon").is_samefile(destination).unwrap() };
}

#[test]
fn test_is_samefile_hardlink() {
	let (destination, source) = ("/tmp/common_test_hardlink", "/tmp/bash_hardlink");
	drop((fs::remove_file(destination), fs::remove_file(source)));

	fs::copy("/bin/bash", source).unwrap(); fs::hard_link(source, destination).unwrap();
	assert! { is_samefile(source, destination).unwrap() };

	[destination, source].iter().for_each(|x| fs::remove_file(x).unwrap());
}

#[test]
fn test_is_samefile_symlink() {
	let (destination, source) = ("/tmp/common_test_symlink", "/bin/bash");
	drop(fs::remove_file(destination));

	symlink(source, destination).unwrap();
	assert! { is_samefile(source, destination).unwrap() };

	// Clean up.
	fs::remove_file(destination).unwrap();
}

#[test]
fn test_is_samefile_symlink_ext() {
	let (destination, source) = ("/tmp/common_test_symlink_ext", "/tmp/bash_symlink_ext");
	drop((fs::remove_file(destination), fs::remove_file(source)));

	fs::copy("/bin/bash", source).unwrap(); symlink(source, destination).unwrap();
	assert! { is_samefile(source, destination).unwrap() };

	[destination, source].iter().for_each(|x| fs::remove_file(x).unwrap());
}

#[test]
fn test_is_samefile_broken_symlink() {
	let (destination, source) = ("/tmp/common_test_broken_symlink", "/tmp/bash_broken_symlink");
	drop((fs::remove_file(destination), fs::remove_file(source)));

	fs::copy("/bin/bash", source).unwrap(); symlink(source, destination).unwrap(); fs::remove_file(source).unwrap();

	// NOTE: even though `source` no longer exists, it is temporarily created during the
	// check and `destination` still points to it, they are considered the same file.
	//
	// So to always get accurate results with `is_samefile`, the `source` **must** exist.
	assert! { is_samefile(destination, source).unwrap() };

	// Source no longer exist, so the check for it, fails with an error.
	assert_eq! { is_samefile(source, destination).unwrap_err().raw_os_error(), Some(ENOENT) };

	fs::copy("/bin/bash", source).unwrap();
	assert! { is_samefile(destination, source).unwrap() };
	assert! { is_samefile(source, destination).unwrap() };

	[destination, source].iter().for_each(|x| fs::remove_file(x).unwrap());
}

#[test]
fn test_is_samefile_broken_symlink_ext() {
	let (destination, destination2, source) = ("/tmp/common_test_broken_symlink_ext", "/tmp/common_test_broken_symlink_ext2", "/tmp/bash_broken_symlink_ext");
	drop((fs::remove_file(destination), fs::remove_file(destination2), fs::remove_file(source)));

	symlink(source, destination).unwrap(); symlink(source, destination2).unwrap();

	// NOTE: Both files point to the same location, `source` but source does not
	// exist so they are not considered the same file as there is no relation
	// between the available files. Neither pathnames or inodes/device numbers
	// are similar.
	assert! { !is_samefile(destination, destination2).unwrap() };

	fs::copy("/bin/bash", source).unwrap();
	assert! { is_samefile(destination, destination2).unwrap() };

	[destination, destination2, source].iter().for_each(|x| fs::remove_file(x).unwrap());
}

#[test]
fn test_is_samefilesystem_loop() {
	let (destination, source) = ("/tmp/common_test_filesystem_loop", "/tmp/test_filesystem_loop");
	drop((fs::remove_file(destination), fs::remove_file(source)));

	symlink(source, source).unwrap(); symlink(source, destination).unwrap();

	// NOTE: `destination` points to `source`, but since `source` target
	// file can never "exist", they are not considered the same file. File
	// name comparision will yield nothing, neither will device/inode number
	// comparison.
	assert! { !is_samefile(destination, source).unwrap() };
	assert! { !is_samefile(source, destination).unwrap() };

	[destination, source].iter().for_each(|x| fs::remove_file(x).unwrap());
}

#[test]
fn test_is_samefile_symlink_dir() {
	let (destination, source) = ("/tmp/common_test_dir_symlink", "/tmp/common_test_dir");
	drop((fs::remove_dir_all(source), fs::remove_file(destination)));

	fs::create_dir(source).unwrap(); symlink(source, destination).unwrap();
	assert! { is_samefile(source, destination).unwrap() };

	fs::remove_file(destination).unwrap(); fs::remove_dir_all(source).unwrap();
}
