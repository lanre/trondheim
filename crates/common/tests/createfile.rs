// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::EEXIST;
use common::constants::EWOULDBLOCK;
use common::constants::S_IFREG;
use common::fs::CreateFile;
use common::fs::LockOperation;
use common::fs::readfile;
use common::sys::getgid;
use common::sys::getuid;

use std::fs;
use std::io::Read;
use std::io::Seek;
use std::io::Write;
use std::os::unix::fs::MetadataExt;
use std::os::unix::fs::PermissionsExt;
use std::os::unix::fs::symlink;
use std::thread;

#[test]
fn createfile_basic() {
	let test = "createfile_basic";
	drop(fs::remove_file(test));

	let fildes = CreateFile::new().create(test).unwrap();
	let metadata = fildes.metadata().unwrap();

	assert_eq! { metadata.len(), 0 }
	assert_eq! { metadata.permissions().mode(), 0o644 | S_IFREG };
	assert_eq! { metadata.uid(), getuid() };
	assert_eq! { metadata.gid(), getgid() };

	fs::remove_file(test).unwrap();
}

#[test]
fn createfile_basic_race_protection() {
	let test = "createfile_basic_race_protection";
	drop((fs::remove_file(test), symlink("/bin/bash", test).unwrap()));

	// CreateFile will refuse to create a new `File` if a symbolic link already exists with the file name.
	assert_eq! { CreateFile::new().create(test).unwrap_err().raw_os_error(), Some(EEXIST) };

	assert! { fs::symlink_metadata(test).unwrap().is_symlink() };
	fs::remove_file(test).unwrap();
}

#[test]
fn createfile_fchmod() {
	let test = "createfile_fchmod";
	drop(fs::remove_file(test));

	let fildes = CreateFile::new().fchmod(fs::Permissions::from_mode(0o400)).create(test).unwrap();
	let metadata = fildes.metadata().unwrap();

	assert_eq! { metadata.len(), 0 }
	assert_eq! { metadata.permissions().mode(), 0o400 | S_IFREG };
	assert_eq! { metadata.uid(), getuid() };
	assert_eq! { metadata.gid(), getgid() };

	fs::remove_file(test).unwrap();
}

#[test]
fn createfile_fchown() {
	let test = "createfile_fchown";
	drop(fs::remove_file(test));

	let fildes = CreateFile::new().fchown(Some(1000), Some(1000)).create(test).unwrap();
	let metadata = fildes.metadata().unwrap();

	assert_eq! { metadata.len(), 0 }
	assert_eq! { metadata.permissions().mode(), 0o644 | S_IFREG };
	assert_eq! { metadata.uid(), getuid() };
	assert_eq! { metadata.gid(), getgid() };

	fs::remove_file(test).unwrap();
}

#[test]
fn createfile_truncate() {
	let test = "createfile_truncate";
	drop((fs::remove_file(test), fs::copy("/bin/bash", test).unwrap()));

	let fildes = CreateFile::new().flock(LockOperation::ExclusiveNonBlocking).create(test).unwrap();
	let (source, destination) = (fs::symlink_metadata("/bin/bash").unwrap(), fildes.metadata().unwrap());

	assert_eq! { destination.len(), 0 }
	assert_ne! { source.len(), 0 }
	assert_eq! { destination.permissions().mode(), source.permissions().mode() };

	drop((fs::remove_file(test), fs::copy("/bin/bash", test).unwrap()));

	let fildes = CreateFile::new().create(test).unwrap();
	let (source, destination) = (fs::symlink_metadata("/bin/bash").unwrap(), fildes.metadata().unwrap());

	assert_eq! { destination.len(), 0 }
	assert_ne! { source.len(), 0 }
	assert_eq! { destination.permissions().mode(), source.permissions().mode() };

	fs::remove_file(test).unwrap();
}

#[test]
fn createfile_flock() {
	let test = "createfile_flock";
	drop(fs::remove_file(test));

	// Test if unlock can be called first.
	drop(CreateFile::new().flock(LockOperation::UnlockNonBlocking).create(test).unwrap());

	// Test if unlock can be called first.
	drop(CreateFile::new().flock(LockOperation::Unlock).create(test).unwrap());

	let mut fildes = CreateFile::new().flock(LockOperation::ExclusiveNonBlocking).create(test).unwrap();

	// Write message to file.
	fildes.write_all("file created successfully".as_bytes()).unwrap();

	// Return cursor to the beginning of the stream.
	fildes.rewind().unwrap();

	// Read message from file.
	let mut buffer = Vec::with_capacity(128);
	fildes.read_to_end(&mut buffer).unwrap();

	let metadata = fildes.metadata().unwrap();

	assert_eq! { readfile(test).unwrap().into_inner(), buffer };
	assert_eq! { readfile(test).unwrap().to_lossy_string(), String::from("file created successfully") };
	assert_ne! { metadata.len(), 0 }
	assert_eq! { metadata.permissions().mode(), 0o644 | S_IFREG };
	assert_eq! { metadata.uid(), getuid() };
	assert_eq! { metadata.gid(), getgid() };

	thread::spawn(move || {
		assert_eq! { CreateFile::new().flock(LockOperation::ExclusiveNonBlocking).create(test).unwrap_err().raw_os_error(), Some(EWOULDBLOCK) };
	}).join().unwrap();

	fs::remove_file(test).unwrap();
}
