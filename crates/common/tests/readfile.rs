// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::EISDIR;
use common::constants::ENOENT;
use common::fs::CreateFile;
use common::fs::readfile;

use std::ffi::OsString;
use std::fs;
use std::io::Read;
use std::io::Seek;
use std::io::Write;

#[test]
fn test_readfile_empty_path() {
	assert_eq! {
		readfile("").unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
}

#[test]
fn test_readfile_directory() {
	assert_eq! {
		readfile("/usr/bin").unwrap_err().raw_os_error(),
		Some(EISDIR)
	};
}

#[test]
fn test_readfile_char_device_file() {
	assert_eq! {
		readfile("/dev/null").unwrap().into_inner(),
		Vec::new()
	};
}

#[test]
fn test_readfile_procfs() {
	assert! {
		readfile("/proc/swaps").unwrap().to_lossy_string().starts_with("Filename")
	};
	assert! {
		readfile("/proc/cpuinfo").unwrap().to_lossy_string().starts_with("processor\t: 0")
	};
}

#[expect(clippy::octal_escapes)]
#[test]
fn test_readfile() {
	let test = "test_readfile";
	drop(fs::remove_file(test));

	let mut fildes = CreateFile::new().create(test).unwrap();

	// Write message to file.
	fildes.write_all("file created successfully".as_bytes()).unwrap();

	// Read message from file.
	let file = readfile(test).unwrap();

	assert_eq! {
		file.to_lossy_string(),
		String::from("file created successfully")
	};
	assert_eq! {
		file.to_string().unwrap(),
		String::from("file created successfully")
	};

	// Overwrite file content without moving the cursor to the file start.
	fildes.set_len(0).unwrap();
	fildes.write_all("233".as_bytes()).unwrap();

	// Move cursor back to file start.
	fildes.rewind().unwrap();

	let mut buffer = Vec::with_capacity(128);
	fildes.read_to_end(&mut buffer).unwrap();

	assert_eq! {
		buffer,
		readfile(test).unwrap().into_inner(),
	};
	assert_eq! {
		readfile(test).unwrap().to_lossy_string(),
		String::from("\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0233")
	};
	assert_eq! {
		readfile(test).unwrap().to_string().unwrap(),
		String::from("\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0233")
	};
	assert_eq! {
		readfile(test).unwrap().to_os_string(),
		OsString::from("\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0233")
	};
	assert_eq! {
		readfile(test).unwrap().into_inner(),
		vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 51, 51]
	};
	assert_eq! {
		readfile(test).unwrap().as_raw_bytes(),
		&[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 51, 51]
	};
	// Null bytes are not stripped even though white spaces are.
	assert! {
		readfile(test).unwrap().parse::<i32>().is_err()
	};

	fs::remove_file(test).unwrap();
}
