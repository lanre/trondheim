// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::EINVAL;
use common::constants::ENOENT;
use common::fs::Discard;
use common::fs::SwapFlags;
use common::fs::swapoff;
use common::fs::swapon;
use common::stdoutln;
use common::testbin;

use std::fs;
use std::os::unix::fs::MetadataExt;
use std::path::Path;

#[test]
fn test_swapoff_and_swapon() {
	assert_eq! {
		swapoff(Path::new("")).unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
	assert_eq! {
		swapoff(Path::new("\0")).unwrap_err().raw_os_error(),
		Some(EINVAL)
	};
	assert_eq! {
		swapon(Path::new(""), SwapFlags::new().discard(Discard::Auto)).unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
	assert_eq! {
		swapon(Path::new("\0"), SwapFlags::new().discard(Discard::Auto)).unwrap_err().raw_os_error(),
		Some(EINVAL)
	};

	// This test will only run as root.
	if fs::metadata("/proc/self").unwrap().uid() == 0 {
		let run = testbin! { "athena", "--size 2302MiB --discard --sparse-file -- common_swapoff_test_file" };
		assert! { fs::read_to_string("/proc/swaps").unwrap().contains("common_swapoff_test_file") };

		assert_eq! { String::from_utf8(run.stderr).unwrap(), String::new() };
		assert_eq! { String::from_utf8(run.stdout).unwrap(), String::new() };

		swapoff("common_swapoff_test_file").unwrap(); fs::remove_file("common_swapoff_test_file").unwrap();
		stdoutln! { "ran test_swapoff_and_swapon() as root" };
	}
}
