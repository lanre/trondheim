// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::EEXIST;
use common::constants::EINVAL;
use common::constants::ENOENT;
use common::constants::EPERM;
use common::constants::S_IFBLK;
use common::constants::S_IFCHR;
use common::constants::S_IFDIR;
use common::constants::S_IFIFO;
use common::constants::S_IFREG;
use common::constants::S_IFSOCK;
use common::constants::S_IRGRP;
use common::constants::S_IROTH;
use common::constants::S_IRUSR;
use common::constants::S_IWUSR;
use common::constants::S_IXUSR;
use common::fs::CreateFile;
use common::fs::MakeDev;
use common::fs::mknodat;
use common::stdoutln;

use std::fs;
use std::os::unix::fs::MetadataExt;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;

#[test]
fn test_mknodat_empty_path() {
	assert_eq! {
		mknodat("", fs::Permissions::from_mode(0o600), MakeDev::new(12, 23)).unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
	assert_eq! {
		mknodat("\0", fs::Permissions::from_mode(0o600), MakeDev::new(12, 23)).unwrap_err().raw_os_error(),
		Some(EINVAL)
	};
}

#[test]
fn test_mknodat_fifo() {
	let file = Path::new("mknodat_test_fifo");
	drop(fs::remove_file(file));

	// Create named pipe.
	let permission = fs::Permissions::from_mode(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH | S_IFIFO);
	mknodat(file, permission, MakeDev::new(12, 23)).unwrap();

	// `mknodat` will only set device ID for character device files and block device files.
	assert_eq! { file.symlink_metadata().unwrap().permissions().mode(), 0o644 | S_IFIFO };
	assert_eq! { file.symlink_metadata().unwrap().rdev(), 0 };

	fs::remove_file(file).unwrap();
}

#[test]
fn test_mknodat_regular_file() {
	let file = Path::new("mknodat_test_regular_file");
	drop(fs::remove_file(file));

	// Create regular file.
	let permission = fs::Permissions::from_mode(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH | S_IFREG);
	mknodat(file, permission, MakeDev::new(12, 23)).unwrap();

	// `mknodat` will only set device ID for character device files and block device files.
	assert_eq! { file.symlink_metadata().unwrap().permissions().mode(), 0o644 | S_IFREG };
	assert_eq! { file.symlink_metadata().unwrap().rdev(), 0 };

	fs::remove_file(file).unwrap();
}

#[test]
fn test_mknodat_socket_file() {
	let file = Path::new("mknodat_test_socket_file");
	drop(fs::remove_file(file));

	// Create empty file.
	CreateFile::new().create(file).unwrap();

	// Create unix domain socket.
	let permission = fs::Permissions::from_mode(S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IROTH | S_IFSOCK);

	// `mknodat` cannot create a new node if the file already exists.
	assert_eq! { mknodat(file, permission.clone(), MakeDev::new(12, 23)).unwrap_err().raw_os_error(), Some(EEXIST) };
	fs::remove_file(file).unwrap();

	mknodat(file, permission, MakeDev::new(12, 23)).unwrap();

	// `mknodat` will only set device ID for character device files and block device files.
	assert_eq! { file.symlink_metadata().unwrap().permissions().mode(), 0o744 | S_IFSOCK };
	assert_eq! { file.symlink_metadata().unwrap().rdev(), 0 };

	fs::remove_file(file).unwrap();
}

#[test]
fn test_mknodat_block_device() {
	let file = Path::new("mknodat_test_block_device");
	drop(fs::remove_file(file));

	// This test will only run as root.
	if fs::metadata("/proc/self").unwrap().uid() == 0 {
		// Create character device file.
		let permission = fs::Permissions::from_mode(S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IROTH | S_IFCHR);
		mknodat(file, permission, MakeDev::new(12, 23)).unwrap();

		assert_eq! { file.symlink_metadata().unwrap().permissions().mode(), 0o744 | S_IFCHR };
		assert_eq! { MakeDev::from_metadata(&file.symlink_metadata().unwrap()), MakeDev::new(12, 23) };

		fs::remove_file(file).unwrap();

		// Create block device file.
		let permission = fs::Permissions::from_mode(S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IROTH | S_IFBLK);
		mknodat(file, permission, MakeDev::new(12, 23)).unwrap();

		assert_eq! { file.symlink_metadata().unwrap().permissions().mode(), 0o744 | S_IFBLK };
		assert_eq! { MakeDev::from_metadata(&file.symlink_metadata().unwrap()), MakeDev::new(12, 23) };

		fs::remove_file(file).unwrap();
		stdoutln! { "ran test_mknodat_block_device() as root" };
	}
}

// Test if mknodat can create directories.
#[test]
fn test_mknodat_directory() {
	let file = Path::new("mknodat_test_directory");
	drop(fs::remove_dir(file));

	let permission = fs::Permissions::from_mode(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH | S_IFDIR);
	assert_eq! { mknodat(file, permission, MakeDev::new(12, 23)).unwrap_err().raw_os_error(), Some(EPERM) };
	assert_eq! { file.symlink_metadata().unwrap_err().raw_os_error(), Some(ENOENT) };
}
