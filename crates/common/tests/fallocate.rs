// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::EOPNOTSUPP;
use common::fs::Fallocate;
use common::fs::FallocFlags;

use std::fs;
use std::path::Path;

#[test]
fn test_fallocate() {
	// Make file a 1 GiB sparse file.
	let (size, file) = (1024_u32 << 20_u32, Path::new("fallocate_trait_test_file"));

	// Create an empty file.
	let filedes = fs::File::options().write(true).create(true).truncate(true).open(file).unwrap();
	assert_eq! { filedes.metadata().unwrap().len(), 0 };

	// File size will be increased to 1 GiB.
	filedes.fallocate(FallocFlags::DefaultMode, 0, size.into()).unwrap();
	assert_eq! { filedes.metadata().unwrap().len(), size.into() };

	// Same as above but preserve size at 1 GiB even though the user requested 4 GiB.
	filedes.fallocate(FallocFlags::KeepSize, 0, i64::from(size) * 4).unwrap();
	assert_eq! { filedes.metadata().unwrap().len(), size.into() };

	// Fill empty blocks with zeroes and increase size to 2 GiB.
	filedes.fallocate(FallocFlags::ZeroRange, 0, i64::from(size) * 2).unwrap();
	assert_eq! { filedes.metadata().unwrap().len(), u64::from(size) * 2 };

	// Fill empty blocks with zeroes but preserve size as 2 GiB.
	filedes.fallocate(FallocFlags::ZeroRangeKeepSize, 0, i64::from(size) * 10).unwrap();
	assert_eq! { filedes.metadata().unwrap().len(), u64::from(size) * 2 };

	// Reduce file size back to 1 GiB.
	filedes.fallocate(FallocFlags::CollapseRange, 0, size.into()).unwrap();
	assert_eq! { filedes.metadata().unwrap().len(), size.into() }

	// Increase file to 4 GiB by adding 3 GiB hole.
	filedes.fallocate(FallocFlags::InsertRange, 0, i64::from(size) * 3).unwrap();
	assert_eq! { filedes.metadata().unwrap().len(), u64::from(size) * 4 };

	// Make holes in file, but preserve file size at 4 GiB.
	filedes.fallocate(FallocFlags::PunchHoleKeepSize, 0, i64::from(size) * 6).unwrap();
	assert_eq! { filedes.metadata().unwrap().len(), u64::from(size) * 4 };

	// Make holes in file, but preserve file size at 4 GiB.
	filedes.fallocate(FallocFlags::PunchHoleKeepSize, 0, i64::from(size) * 2).unwrap();
	assert_eq! { filedes.metadata().unwrap().len(), u64::from(size) * 4 };

	// Not supported by every filesystem.
	//
	// Unshare any shared blocks while preserving file size at 4 GiB.
	match filedes.fallocate(FallocFlags::UnshareRangeKeepSize, 0, i64::from(size) * 9) {
		Err(error) => {
			assert_eq! { error.raw_os_error(), Some(EOPNOTSUPP) };
		},
		Ok(()) => {
			assert_eq! { filedes.metadata().unwrap().len(), u64::from(size) * 4 };
		},
	}

	// Not supported by every filesystem.
	//
	// Unshare any shared blocks and increase the file size to 9 GiB.
	match filedes.fallocate(FallocFlags::UnshareRangeKeepSize, 0, i64::from(size) * 9) {
		Err(error) => {
			assert_eq! { error.raw_os_error(), Some(EOPNOTSUPP) };
		},
		Ok(()) => {
			assert_eq! { filedes.metadata().unwrap().len(), u64::from(size) * 9 };
		},
	}

	fs::remove_file(file).unwrap();
}
