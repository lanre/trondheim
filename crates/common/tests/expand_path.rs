// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::S_IFLNK;
use common::constants::S_IFMT;
use common::fs::expand_path;

use std::fs;
use std::os::unix::fs::PermissionsExt;
use std::os::unix::fs::symlink;
use std::path::PathBuf;

#[test]
fn test_expand_path_basic() {
	let source = "/sys/class/hwmon/";
	assert_eq! { expand_path(source), PathBuf::from("/sys/class/hwmon/") };

	let source = "/proc/sys";
	assert_eq! { expand_path(source), PathBuf::from("/proc/sys") };

	let source = "../hwmondsdsd/";
	assert_eq! { expand_path(source), PathBuf::from("../hwmondsdsd") };
}

#[test]
fn test_expand_path_normalize() {
	let source = "/sys/class/hwmon/../hwmon/../hwmon/./../../class/hwmon";
	assert_eq! { expand_path(source), PathBuf::from("/sys/class/hwmon/") };

	let source = "/does/not/exist/sys/class/hwmon/../hwmon/../hwmon/./../../class/hwmon";
	assert_eq! { expand_path(source), PathBuf::from("/does/not/exist/sys/class/hwmon/") };

	let source = "/does/not/exist/sys/class/hwmon/";
	assert_eq! { expand_path(source), PathBuf::from("/does/not/exist/sys/class/hwmon/") };
}

#[test]
fn test_expand_path_canonicalize() {
	let source = "/sys/devices/../../sys/devices/../devices/system/../system/cpu/cpu0/subsystem";
	assert_eq! { expand_path(source), PathBuf::from("/sys/bus/cpu") };
}

#[test]
fn test_expand_path_broken_symlink() {
	let source = "/tmp/trondheim_common_test_broken";
	drop(fs::remove_file(source));

	symlink("/does/not/exist", source).unwrap();
	assert_eq! { expand_path(source), PathBuf::from("/tmp/trondheim_common_test_broken") };
	assert_eq! { fs::symlink_metadata(source).unwrap().permissions().mode() & S_IFMT, S_IFLNK };

	fs::remove_file(source).unwrap();
}

#[test]
fn test_expand_path_broken_symlink_normalize() {
	let source = "/tmp/../../../../tmp/trondheim_common_test_broken1";
	drop(fs::remove_file(source));

	symlink("/does/not/exist", source).unwrap();
	assert_eq! { expand_path(source), PathBuf::from("/tmp/trondheim_common_test_broken1") };
	assert_eq! { fs::symlink_metadata(source).unwrap().permissions().mode() & S_IFMT, S_IFLNK };

	fs::remove_file(source).unwrap();
}
