// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::fs::normalize_path;

use std::path::PathBuf;

#[test]
fn test_normalize_empty_path() {
	assert_eq! { normalize_path(""), PathBuf::new() };
}

#[test]
fn test_normalize_components() {
	let input = &[
		( ".",        "."  ),
		( "..",       ".." ),
		( "..//",     ".." ),
		( "./",       "."  ),
		( "././/./",  "."  ),
		( ".//",      "."  ),
		( ".////./.", "."  ),
		( "/",        "/"  ),
		( "/..",      "/"  ),
		( "/../..",   "/"  ),
		( "/..//",    "/"  ),
		( "/./",      "/"  ),
		( "/.//./",   "/"  ),
		( "//",       "/"  ),
		( "//..",     "/"  ),
		( "///",      "/"  ),
	];

	for (source, fullpath) in input {
		assert_eq! { normalize_path(source), PathBuf::from(fullpath), "input: {}", source };
	}
}

#[test]
fn test_normalize_path_with_root() {
	let input = &[
		( "/../test",               "/test"             ),
		( "/foo/bar",               "/foo/bar"          ),
		( "/foo/bar/",              "/foo/bar"          ),
		( "/foo/bar/..",            "/foo"              ),
		( "/foo/bar/../..",         "/"                 ),
		( "/foo/bar/../../..",      "/"                 ),
		( "/foo/bar/./././///",     "/foo/bar"          ),
		( "/path//to///file",       "/path/to/file"     ),
		( "/proc/sys",              "/proc/sys/"        ),
		( "/sys/class/hwmon/",      "/sys/class/hwmon/" ),
		( "/test/../path",          "/path"             ),
		( "/test/./path/",          "/test/path"        ),
		( "/test/path/../../..",    "/"                 ),
		( "/test/path/../../../..", "/"                 ),
	];

	for (source, fullpath) in input {
		assert_eq! { normalize_path(source), PathBuf::from(fullpath), "input: {}", source };
	}
}

#[test]
fn test_normalize_path_with_root_ext() {
	let input = &[
		( "/sys/class/hwmon/../hwmon/../hwmon/./../../class/hwmon", "/sys/class/hwmon/" ),
		( "/tmp/../../../../tmp/common/.//..////../tmp/common/.",   "/tmp/common"       ),
	];

	for (source, fullpath) in input {
		assert_eq! { normalize_path(source), PathBuf::from(fullpath), "input: {}", source };
	}
}

#[test]
fn test_normalize() {
	let input = &[
		( "../../foo/bar",            "../../foo/bar" ),
		( "../../foo/bar/",           "../../foo/bar" ),
		( "../../foo/bar/..",         "../../foo"     ),
		( "../../foo/bar/../..",      "../.."         ),
		( "../../foo/bar/../../..",   "../../.."      ),
		( "../../foo/bar/./././///",  "../../foo/bar" ),
		( "../foo",                   "../foo"        ),
		( "../foo/",                  "../foo"        ),
		( "../foo/..",                ".."            ),
		( "../foo/bar",               "../foo/bar"    ),
		( "./foo",                    "foo"           ),
		( "./foo/./bar",              "foo/bar"       ),
		( "foo/..",                   "."             ),
		( "foo/../bar",               "bar"           ),
		( "foo/bar",                  "foo/bar"       ),
		( "foo/bar/",                 "foo/bar"       ),
		( "foo/bar/.",                "foo/bar"       ),
		( "foo/bar/..",               "foo"           ),
		( "foo/bar/../..",            "."             ),
		( "foo/bar/../../",           "."             ),
		( "foo/bar/../../..",         ".."            ),
		( "foo/bar/../../../..",      "../.."         ),
		( "foo/bar/../../tee/bar",    "tee/bar"       ),
		( "foo/bar/../../tee/bar/..", "tee"           ),
		( "foo/bar/./././///",        "foo/bar"       ),
		( "path//to///file",          "path/to/file"  ),
	];

	for (source, fullpath) in input {
		assert_eq! { normalize_path(source), PathBuf::from(fullpath), "input: {}", source };
	}
}
