// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::cli::getopt;
use common::cli::HasArg;
use common::constants::*;
use common::error;

use std::ffi::OsString;
use std::os::unix::ffi::OsStringExt;

// Random program options to test `getopt()`
const OPTIONS: &[(&str, &str, HasArg)] = &[
	( "--aruba",         "",   HasArg::True     ),
	( "--power-move",    "-c", HasArg::False    ),
	( "--disable",       "-d", HasArg::False    ),
	( "--help",          "",   HasArg::False    ),
	( "--eyeball",        "",  HasArg::Optional ),
	( "",                "-k", HasArg::False    ),
	( "",                "-e", HasArg::Optional ),
	( "",                "-y", HasArg::True     ),
	( "--program-score", "-o", HasArg::True     ),
	( "--color",         "-C", HasArg::Optional ),
	( "--power-level",   "-p", HasArg::True     ),
	( "--verbose",       "-v", HasArg::False    ),
	( "--version",       "",   HasArg::False    ),
	( "--wait-till",     "-w", HasArg::True     ),
];

#[test]
fn test_getopt_missing() {
	let (option, arg) = getopt(vec!["test_program".into()], OPTIONS, "test_program").unwrap();

	assert_eq! {
		option, Vec::<(String, String)>::new()
	};
	assert_eq! {
		arg, Vec::<String>::new()
	};
}

// `user_input[0]` should always contain the program name
#[test]
fn test_getopt_missing_error() {
	assert_eq! {
		getopt(Vec::<OsString>::new(), OPTIONS, "test_program").unwrap_err(),
		error!{ GETOPT_ERR_CODE, "test_program: fatal error: Failed to detect any input from the command line" },
	};
}

#[test]
fn test_getopt_missing_arg_error() {
	let user_input = "test_program test_string --power-move -cwwop -o --verbose --program-score=1212 -cdp23 -- broken_symlink_copy"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	assert_eq! {
		getopt(user_input, OPTIONS, "test_program").unwrap_err(),
		error! { GETOPT_ERR_CODE, "test_program: an argument is required for `-o` but none was supplied" },
	};
}

#[test]
fn test_getopt_missing_arg_error_positional() {
	let user_input = "test_program test_string --power-move -cdp23 --program-score -- broken_symlink_copy"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	assert_eq! {
		getopt(user_input, OPTIONS, "test_program").unwrap_err(),
		error! { GETOPT_ERR_CODE, "test_program: an argument is required for `--program-score` but none was supplied" },
	};
}

#[test]
fn test_getopt_missing_arg_error_ext() {
	let user_input = "test_program test_string --power-move -o12 -o 1445 -o=56675 -w=-23 --program-score= 1212 -cdp23 -- broken_symlink_copy"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	assert_eq! {
		getopt(user_input, OPTIONS, "test_program").unwrap_err(),
		error! { GETOPT_ERR_CODE, "test_program: an argument is required for `--program-score` but none was supplied" },
	};
}

#[test]
fn test_getopt_negative_val_error() {
	let user_input = "test_program test_string --verbose --program-score -1212 -cdp23 -- broken_symlink_copy"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	assert_eq! {
		getopt(user_input, OPTIONS, "test_program").unwrap_err(),
		error! {GETOPT_ERR_CODE, "test_program: an argument is required for `--program-score` but none was supplied" },
	};
}

#[test]
fn test_getopt_unrecognised_option_error() {
	let user_input = "test_program test_string --verbose --program-score1212 -cdp23 -- broken_symlink_copy"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	assert_eq! {
		getopt(user_input, OPTIONS, "test_program").unwrap_err(),
		error! { GETOPT_ERR_CODE, "test_program: unrecognized option `--program-score1212`" },
	};
}

#[test]
fn test_getopt_unrecognised_option_hypen_error() {
	let user_input = "test_program test_string --power-move -cwwop -o12 -o 1445 -o=56675 - -w=-23 --verbose --program-score=1212 -cdp23 -- broken_symlink_copy"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	assert_eq! {
		getopt(user_input, OPTIONS, "test_program").unwrap_err(),
		error! { GETOPT_ERR_CODE, "test_program: unrecognized option `-`" },
	};
}

#[test]
fn test_getopt_unrecognised_option_whitespace_error() {
	let user_input = [
		"test_program",
		"test_string",
		"--power-move",
		"-cwwop",
		"-o12",
		"-o",
		"1445",
		"-o=56675",
		"- ",
		"-w=-23",
		"--verbose",
		"--program-score=1212",
		"-cdp23",
		"--",
		"broken_symlink_copy",
	].into_iter().map(|x| x.into()).collect::<Vec<OsString>>();

	assert_eq! {
		getopt(user_input, OPTIONS, "test_program").unwrap_err(),
		error! { GETOPT_ERR_CODE, "test_program: invalid option -- ` `"},
	};
}

#[test]
fn test_getopt_invalid_option_err() {
	let user_input = "test_program test_string --power-move -cwwop -o12 -o 1445 -ro=56675 -w=-23 --verbose --program-score=1212 -cdp23 -- broken_symlink_copy"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	assert_eq! {
		getopt(user_input, OPTIONS, "test_program").unwrap_err(),
		error! { GETOPT_ERR_CODE, "test_program: invalid option -- `r`" },
	};
}

#[test]
fn test_getopt_invalid_utf8_option_err() {
	let mut user_input = "test_program test_string --power-move -cwwop -o12"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	user_input.push(OsString::from_vec(vec![45, 255]));

	assert_eq! {
		getopt(user_input, OPTIONS, "test_program").unwrap_err(),
		error! { GETOPT_ERR_CODE, "test_program: invalid option -- `�`" },
	};
}

#[test]
fn test_getopt_extra_arg_short_err() {
	let user_input = "test_program test_string --power-move -cwwop -o12 -o 1445 -k=56675 -w=-23 --verbose --program-score=1212 -cdp23 -- broken_symlink_copy"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	assert_eq! {
		getopt(user_input, OPTIONS, "test_program").unwrap_err(),
		error! { GETOPT_ERR_CODE, "test_program: `-k` does not accept any arguments but '56675' was supplied" },
	};
}

#[test]
fn test_getopt_extra_arg_long_err() {
	let user_input = "test_program test_string --power-move=-cwwop -o12 -o 1445 -k=56675 -w=-23 --verbose --program-score=1212 -cdp23 -- broken_symlink_copy"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	assert_eq! {
		getopt(user_input, OPTIONS, "test_program").unwrap_err(),
		error! { GETOPT_ERR_CODE, "test_program: `--power-move` does not accept any arguments but '-cwwop' was supplied" },
	};
}

#[test]
fn test_getopt_basic() {
	let user_input = "test_program test_string --power-move -cwwop -o12 -o 1445 -o=56675 -w=-23 --verbose --program-score=-1212 -cdp23 -- broken_symlink_copy"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "test_program").unwrap();

	let res: Vec<(String, String)> = [
		( "--power-move",    ""      ),
		( "-c",              ""      ),
		( "-w",              "wop"   ),
		( "-o",              "12"    ),
		( "-o",              "1445"  ),
		( "-o",              "56675" ),
		( "-w",              "-23"   ),
		( "--verbose",       ""      ),
		( "--program-score", "-1212" ),
		( "-c",              ""      ),
		( "-d",              ""      ),
		( "-p",              "23"    ),
	].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect();

	assert_eq! {
		args,
		"test_string broken_symlink_copy".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option, res
	};
}

#[test]
fn test_getopt_basic_ext() {
	let user_input = "test_program test_string hehe --power-move -cwwop i -o12 am -o 1445 alive -o=56675 -- -w=-23 --verbose --program-score=1212 -cdp23 -- broken_symlink_copy"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "test_program").unwrap();

	let res: Vec<(String, String)> = [
		( "--power-move", ""      ),
		( "-c",           ""      ),
		( "-w",           "wop"   ),
		( "-o",           "12"    ),
		( "-o",           "1445"  ),
		( "-o",           "56675" ),
	].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect();

	assert_eq! {
		args,
		"test_string hehe i am alive -w=-23 --verbose --program-score=1212 -cdp23 -- broken_symlink_copy".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option, res
	};
}

#[test]
fn test_getopt() {
	let user_input = "test_program --program-score=-meee --verbose test_string --aruba this --help --help -dckv --disable --power-move --program-score=1212 -vkp23 -o=23 -w=assorted -private -y parsed -- broken_symlink_copy --help --ultimate"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "test_program").unwrap();

	let res: Vec<(String, String)> = [
		( "--program-score", "-meee"    ),
		( "--verbose",       ""         ),
		( "--aruba",         "this"     ),
		( "--help",          ""         ),
		( "--help",          ""         ),
		( "-d",              ""         ),
		( "-c",              ""         ),
		( "-k",              ""         ),
		( "-v",              ""         ),
		( "--disable",       ""         ),
		( "--power-move",    ""         ),
		( "--program-score", "1212"     ),
		( "-v",              ""         ),
		( "-k",              ""         ),
		( "-p",              "23"       ),
		( "-o",              "23"       ),
		( "-w",              "assorted" ),
		( "-p",              "rivate"   ),
		( "-y",              "parsed"   ),
	].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect();

	assert_eq! {
		args,
		"test_string broken_symlink_copy --help --ultimate".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option, res
	};
}

#[test]
fn test_getopt_multiple_long_optarg() {
	let user_input = "test_program --program-score 234 --program-score true --program-score=hehe --program-score=egg --program-score=12 --program-score=-12"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "test_program").unwrap();

	let res: Vec<(String, String)> = [
		( "--program-score", "234"  ),
		( "--program-score", "true" ),
		( "--program-score", "hehe" ),
		( "--program-score", "egg"  ),
		( "--program-score", "12"   ),
		( "--program-score", "-12"  ),
	].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect();

	assert_eq! {
		args, Vec::<String>::new(),
	};
	assert_eq! {
		option, res
	};
}

#[test]
fn test_getopt_multiple_short_optarg() {
	let user_input = "test_program -y 234 -y true -y=23311 -y=pizza -y55654 -yorange -y=34 -y=-34 -y this,is,the"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "test_program").unwrap();

	let res: Vec<(String, String)> = [
		( "-y", "234"         ),
		( "-y", "true"        ),
		( "-y", "23311"       ),
		( "-y", "pizza"       ),
		( "-y", "55654"       ),
		( "-y", "orange"      ),
		( "-y", "34"          ),
		( "-y", "-34"         ),
		( "-y", "this,is,the" ),
	].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect();

	assert_eq! {
		args, Vec::<String>::new(),
	};
	assert_eq! {
		option, res
	};
}

#[test]
fn test_getopt_multiple_optional_optarg() {
	let user_input = "test_program --eyeball=-2 --eyeball=2 --eyeball -d --eyeball mee --eyeball=err --eyeball --aruba=2 --eyeball 234 --power-move --eyeball -C=23311 -C --aruba ss -C --color=pizza --eyeball --eyeball=green --color -e55654 -eorange -e"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "test_program").unwrap();

	let res: Vec<(String, String)> = [
		( "--eyeball",    "-2"     ),
		( "--eyeball",    "2"      ),
		( "--eyeball",    ""       ),
		( "-d",           ""       ),
		( "--eyeball",    "mee"    ),
		( "--eyeball",    "err"    ),
		( "--eyeball",    ""       ),
		( "--aruba",      "2"      ),
		( "--eyeball",    "234"    ),
		( "--power-move", ""       ),
		( "--eyeball",    ""       ),
		( "-C",           "23311"  ),
		( "-C",           ""       ),
		( "--aruba",      "ss"     ),
		( "-C",           ""       ),
		( "--color",      "pizza"  ),
		( "--eyeball",    ""       ),
		( "--eyeball",    "green"  ),
		( "--color",      ""       ),
		( "-e",           "55654"  ),
		( "-e",           "orange" ),
		( "-e",           ""       ),
	].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect();

	assert_eq! {
		args, Vec::<String>::new(),
	};
	assert_eq! {
		option, res
	};
}

#[test]
fn test_getopt_optional_long_optarg() {
	let user_input = "test_program --eyeball mee --eyeball=err sold --eyeball --eyeball=-45 --eyeball=45 -- woo eooo"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "test_program").unwrap();

	let res: Vec<(String, String)> = [
		( "--eyeball", "mee" ),
		( "--eyeball", "err" ),
		( "--eyeball", ""    ),
		( "--eyeball", "-45" ),
		( "--eyeball", "45"  ),
	].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect();

	assert_eq! {
		args,
		"sold woo eooo".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option, res
	};
}

#[test]
fn test_getopt_optional_short_optarg() {
	let user_input = "test_program -e mee -e=err sold -e -e=22 -e=-22 -eyeball -- woo eooo"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "test_program").unwrap();

	let res: Vec<(String, String)> = [
		( "-e", "mee"    ),
		( "-e", "err"    ),
		( "-e", ""       ),
		( "-e", "22"     ),
		( "-e", "-22"    ),
		( "-e", "yeball" ),
	].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect();

	assert_eq! {
		args,
		"sold woo eooo".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option, res
	};
}

#[test]
fn test_getopt_invalid_program_name() {
	let user_input = "-- -e mee -e=err sold -e -e=22 -e=-22 -eyeball -- woo eooo"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "--").unwrap();

	let res: Vec<(String, String)> = [
		( "-e", "mee"    ),
		( "-e", "err"    ),
		( "-e", ""       ),
		( "-e", "22"     ),
		( "-e", "-22"    ),
		( "-e", "yeball" ),
	].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect();

	assert_eq! {
		args,
		"sold woo eooo".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option, res
	};
}

#[test]
fn test_getopt_invalid_program_name_ext() {
	let user_input = "-- -- -e mee -e=err sold -e -e=22 -e=-22 -eyeball -- woo eooo"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "--").unwrap();

	assert_eq! {
		args,
		"-e mee -e=err sold -e -e=22 -e=-22 -eyeball -- woo eooo".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option, Vec::<(String, String)>::new(),
	};
}

#[test]
fn test_getopt_whitespace() {
	let user_input = [
		"test_program",
		"test_string",
		"--power-move",
		"-cwwop",
		"-o12",
		"-o",
		"1445",
		"-o=56675",
		" ",
		"-w=-23",
		"--verbose",
		"--program-score=1212",
		"-cdp23",
		"--",
		"broken_symlink_copy",
		" ",
	].into_iter().map(|x| x.into()).collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "test_program").unwrap();

	let res: Vec<(String, String)> = [
		( "--power-move",    ""      ),
		( "-c",              ""      ),
		( "-w",              "wop"   ),
		( "-o",              "12"    ),
		( "-o",              "1445"  ),
		( "-o",              "56675" ),
		( "-w",              "-23"   ),
		( "--verbose",       ""      ),
		( "--program-score", "1212"  ),
		( "-c",              ""      ),
		( "-d",              ""      ),
		( "-p",              "23"    ),
	].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect();

	assert_eq! {
		args,
		["test_string", " ", "broken_symlink_copy", " "].into_iter().map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option, res
	};
}

#[test]
fn test_getopt_invalid_utf8() {
	let invalid = vec![255];

	let invalid_arg = vec![45, 111, 61, 255];
	let invalid_arg2 = vec![45, 111, 255];

	let user_input = vec![
		"test_program".into(),
		"--power-move".into(),
		OsString::from_vec(invalid_arg2), // -o ...
		"-cwwop".into(),
		"test_string".into(),
		OsString::from_vec(invalid_arg),  // -o= ...
		OsString::from_vec(invalid.clone()),
		"--verbose".into(),
		"--program-score=1212".into(),
		"--".into(),
		OsString::from_vec(invalid),
	];

	let (option, args) = getopt(user_input, OPTIONS, "test_program").unwrap();

	let res: Vec<(String, String)> = [
		( "--power-move",    ""     ),
		( "-o",              "�"    ),
		( "-c",              ""     ),
		( "-w",              "wop"  ),
		( "-o",              "�"    ),
		( "--verbose",       ""     ),
		( "--program-score", "1212" ),
	].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect();

	assert_eq! {
		args,
		"test_string � �".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option, res
	};
}

#[test]
fn test_getopt_posix_examples() {
	/////////////////////////////////////////////
	// Test 1
	/////////////////////////////////////////////
	let user_input = "cmd -ce arg path path"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "cmd").unwrap();

	assert_eq! {
		args,
		"path path".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option,
		[("-c", ""), ("-e", "arg")].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect::<Vec<(String, String)>>(),
	};

	/////////////////////////////////////////////
	// Test 2
	/////////////////////////////////////////////
	let user_input = "cmd -c -e arg path path"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "cmd").unwrap();

	assert_eq! {
		args,
		"path path".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option,
		[("-c", ""), ("-e", "arg")].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect::<Vec<(String, String)>>(),
	};

	/////////////////////////////////////////////
	// Test 3
	/////////////////////////////////////////////
	let user_input = "cmd -e arg -c path path"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "cmd").unwrap();

	assert_eq! {
		args,
		"path path".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option,
		[("-e", "arg"), ("-c", "")].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect::<Vec<(String, String)>>(),
	};

	/////////////////////////////////////////////
	// Test 4
	/////////////////////////////////////////////
	let user_input = "cmd -c -e arg -- path path"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "cmd").unwrap();

	assert_eq! {
		args,
		"path path".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option,
		[("-c", ""), ("-e", "arg")].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect::<Vec<(String, String)>>(),
	};

	/////////////////////////////////////////////
	// Test 5
	/////////////////////////////////////////////
	let user_input = "cmd -c -earg path path"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "cmd").unwrap();

	assert_eq! {
		args,
		"path path".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option,
		[("-c", ""), ("-e", "arg")].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect::<Vec<(String, String)>>(),
	};

	/////////////////////////////////////////////
	// Test 6
	/////////////////////////////////////////////
	let user_input = "cmd -cearg path path"
		.split_whitespace()
		.map(|s| s.into())
		.collect::<Vec<OsString>>();

	let (option, args) = getopt(user_input, OPTIONS, "cmd").unwrap();

	assert_eq! {
		args,
		"path path".split(' ').map(String::from).collect::<Vec<String>>(),
	};
	assert_eq! {
		option,
		[("-c", ""), ("-e", "arg")].into_iter().map(|(x, y)| (String::from(x), String::from(y))).collect::<Vec<(String, String)>>(),
	};
}
