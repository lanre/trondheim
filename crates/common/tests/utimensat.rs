// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::O_NOATIME;
use common::constants::O_NOFOLLOW;
use common::constants::O_PATH;
use common::fs::UtimeFlags;
use common::fs::utimensat;
use common::time::HumanTime;

use std::fs;
use std::os::unix::fs::MetadataExt;
use std::os::unix::fs::OpenOptionsExt;
use std::os::unix::fs::symlink;
use std::path::Path;
use std::time::Duration;
use std::time::SystemTime;

// A beautiful bird.
fn canary<P: AsRef<Path>>(file: P) {
	let file = file.as_ref();
	drop(fs::remove_file(file));

	let parentdir = file.parent().unwrap_or_else(|| Path::new(""));

	if !parentdir.as_os_str().is_empty() {
		fs::create_dir_all(parentdir).unwrap();
	}

	fs::write(file, "Serinus canaria forma domestica").unwrap();
}

#[test]
fn test_mtime() {
	let dest = Path::new("test_mtime");
	canary(dest);

	let fildes = fs::File::options().read(true).open(dest).unwrap();
	assert! { fildes.metadata().unwrap().file_type().is_file() };

	utimensat(dest, None, fs::symlink_metadata("/bin/bash").unwrap().modified().ok(), UtimeFlags::Follow).unwrap();
	let (source, destination) = (fs::symlink_metadata("/bin/bash").unwrap(), fildes.metadata().unwrap());

	assert_eq! { source.modified().unwrap(), destination.modified().unwrap() };
	assert_ne! { source.accessed().unwrap(), destination.accessed().unwrap() };
	assert_eq! { fs::read_to_string(dest).unwrap(), String::from("Serinus canaria forma domestica") };

	let system_time = SystemTime::now();
	utimensat(dest, None, Some(system_time), UtimeFlags::NoFollow).unwrap();

	assert_eq! { fildes.metadata().unwrap().modified().unwrap(), system_time };
	fs::remove_file(dest).unwrap();
}

#[test]
fn test_atime() {
	let dest = Path::new("test_atime");
	canary(dest);

	let fildes = fs::File::options().read(true).custom_flags(O_NOATIME).open(dest).unwrap();
	assert! { fildes.metadata().unwrap().file_type().is_file() };

	utimensat(dest, fs::symlink_metadata("/bin/bash").unwrap().accessed().ok(), None, UtimeFlags::Follow).unwrap();
	let (source, destination) = (fs::symlink_metadata("/bin/bash").unwrap(), fildes.metadata().unwrap());

	assert_ne! { source.modified().unwrap(), destination.modified().unwrap() };
	assert_eq! { source.accessed().unwrap(), destination.accessed().unwrap() };
	assert_eq! { fs::read_to_string(dest).unwrap(), String::from("Serinus canaria forma domestica") };

	let system_time = SystemTime::now();
	utimensat(dest, Some(system_time), None, UtimeFlags::NoFollow).unwrap();

	assert_eq! { fildes.metadata().unwrap().accessed().unwrap(), system_time };
	fs::remove_file(dest).unwrap();
}

#[test]
fn test_symlink_times() {
	let (dir, dest) = (Path::new("test_symlink_time_symlink"), Path::new("new_symlink_test_common"));
	drop(fs::remove_file(dest)); canary(dir); symlink(dir, dest).unwrap();

	let fildes = fs::File::options().read(true).custom_flags(O_NOATIME | O_NOFOLLOW | O_PATH).open(dest).unwrap();
	assert! { fildes.metadata().unwrap().file_type().is_symlink() };

	let bashdes = fs::File::options().read(true).custom_flags(O_NOFOLLOW | O_PATH).open("/bin/bash").unwrap();
	assert! { bashdes.metadata().unwrap().file_type().is_file() };

	let (source, destination) = (bashdes.metadata().unwrap(), fildes.metadata().unwrap());

	assert_ne! { source.modified().unwrap(), destination.modified().unwrap() };
	assert_ne! { source.accessed().unwrap(), destination.accessed().unwrap() };

	utimensat(dest, source.accessed().ok(), source.modified().ok(), UtimeFlags::NoFollow).unwrap();
	let (source, destination) = (bashdes.metadata().unwrap(), fildes.metadata().unwrap());

	assert_eq! { source.modified().unwrap(), destination.modified().unwrap() };
	assert_eq! { source.accessed().unwrap(), destination.accessed().unwrap() };
	assert_eq! { fs::read_to_string(dest).unwrap(), String::from("Serinus canaria forma domestica") };

	fs::remove_file(dest).unwrap(); fs::remove_file(dir).unwrap();
}

#[test]
fn test_proleptic_calender() {
	let dest = Path::new("test_atime_proleptic");
	canary(dest);

	let fildes = fs::File::options().read(true).custom_flags(O_NOATIME).open(dest).unwrap();
	assert! { fildes.metadata().unwrap().file_type().is_file() };

	//////////////////////////////////////////////////////////////////

	let system_time = SystemTime::UNIX_EPOCH - Duration::from_secs(fs::symlink_metadata("/bin/bash").unwrap().atime() as u64);
	assert! { HumanTime::from_systemtime(system_time).year() < 1970 };

	utimensat(dest, Some(system_time), Some(system_time), UtimeFlags::NoFollow).unwrap();

	assert_eq! { system_time, fildes.metadata().unwrap().modified().unwrap() };
	assert_eq! { system_time, fildes.metadata().unwrap().accessed().unwrap() };
	assert_eq! { fs::read_to_string(dest).unwrap(), String::from("Serinus canaria forma domestica") };

	//////////////////////////////////////////////////////////////////

	let system_time = SystemTime::UNIX_EPOCH - Duration::from_secs(1738802633);
	assert_eq! { HumanTime::from_systemtime(system_time).to_utc_string(), String::from("1914-11-25 23:16:07 +0000") };

	utimensat(dest, Some(system_time), Some(system_time), UtimeFlags::NoFollow).unwrap();

	assert_eq! { system_time, fildes.metadata().unwrap().modified().unwrap() };
	assert_eq! { system_time, fildes.metadata().unwrap().accessed().unwrap() };
	assert_eq! { fs::read_to_string(dest).unwrap(), String::from("Serinus canaria forma domestica") };

	//////////////////////////////////////////////////////////////////

	let system_time = SystemTime::UNIX_EPOCH - Duration::from_secs(307758337);
	assert_eq! { HumanTime::from_systemtime(system_time).to_utc_string(), String::from("1960-03-31 23:34:23 +0000") };

	utimensat(dest, Some(system_time), Some(system_time), UtimeFlags::NoFollow).unwrap();

	assert_eq! { system_time, fildes.metadata().unwrap().modified().unwrap() };
	assert_eq! { system_time, fildes.metadata().unwrap().accessed().unwrap() };
	assert_eq! { fs::read_to_string(dest).unwrap(), String::from("Serinus canaria forma domestica") };

	//////////////////////////////////////////////////////////////////

	let system_time = SystemTime::UNIX_EPOCH - Duration::from_secs(507758323);
	assert_eq! { HumanTime::from_systemtime(system_time).to_utc_string(), String::from("1953-11-29 04:01:17 +0000") };

	utimensat(dest, Some(system_time), Some(system_time), UtimeFlags::NoFollow).unwrap();

	assert_eq! { system_time, fildes.metadata().unwrap().modified().unwrap() };
	assert_eq! { system_time, fildes.metadata().unwrap().accessed().unwrap() };
	assert_eq! { fs::read_to_string(dest).unwrap(), String::from("Serinus canaria forma domestica") };

	//////////////////////////////////////////////////////////////////

	let system_time = SystemTime::UNIX_EPOCH - Duration::from_secs(1073741824);
	assert_eq! { HumanTime::from_systemtime(system_time).to_utc_string(), String::from("1935-12-23 10:22:56 +0000") };

	utimensat(dest, Some(system_time), Some(system_time), UtimeFlags::NoFollow).unwrap();

	assert_eq! { system_time, fildes.metadata().unwrap().modified().unwrap() };
	assert_eq! { system_time, fildes.metadata().unwrap().accessed().unwrap() };
	assert_eq! { fs::read_to_string(dest).unwrap(), String::from("Serinus canaria forma domestica") };

	fs::remove_file(dest).unwrap();
}
