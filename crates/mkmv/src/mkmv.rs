// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::EEXIST;
use common::constants::ELOOP;
use common::constants::ENOENT;
use common::constants::ENOTDIR;
use common::constants::EOPNOTSUPP;
use common::constants::EXDEV;
use common::constants::O_NOFOLLOW;
use common::constants::REC_ERR_CODE;
use common::constants::S_IFBLK;
use common::constants::S_IFCHR;
use common::constants::S_IFDIR;
use common::constants::S_IFIFO;
use common::constants::S_IFLNK;
use common::constants::S_IFMT;
use common::constants::S_IFSOCK;
use common::constants::SF_ERR_CODE;
use common::constants::SRC_ERR_CODE;
use common::constants::USER_ERR_CODE;
use common::error::ExitCode;
use common::error;
use common::fs::fchmodat;
use common::fs::FchmodFlags;
use common::fs::is_samefile;
use common::fs::MakeDev;
use common::fs::mknodat;
use common::fs::UtimeFlags;
use common::fs::utimensat;
use common::ret;
use common::stderr;
use common::stderrln;
use common::stdoutln;

use crate::Options;

use std::collections::HashSet;
use std::fs;
use std::io;
use std::os::unix::fs::lchown;
use std::os::unix::fs::MetadataExt;
use std::os::unix::fs::OpenOptionsExt;
use std::os::unix::fs::PermissionsExt;
use std::os::unix::fs::symlink;
use std::path::Path;
use std::path::PathBuf;

// Validate the input arguments of `move_files()`.
fn input_validation(mut files: Vec<PathBuf>, mkmv: Options) -> common::Result<(PathBuf, Vec<PathBuf>)> {
	if mkmv.no_clobber && (mkmv.backup || mkmv.force || mkmv.interactive || mkmv.suffix.is_some()) {
		ret! { ExitCode::Failure, "mkmv: `--no-clobber` cannot be specified with `--backup`, `--force`, `--interactive` or `--suffix`" };
	}

	if files.is_empty() {
		ret! { ExitCode::Failure, "mkmv: missing file operand" };
	}

	if files.get(1).is_none() {
		if let Some(file) = files.first() {
			ret! { ExitCode::Failure, "mkmv: missing destination file operand after '{}'", file.display() };
		}
	}

	if mkmv.no_target {
		if let Some([dest, file]) = files.get(1..=2) {
			ret! { ExitCode::Failure, "mkmv: extra operand '{}' after destination file '{}'", file.display(), dest.display() };
		}
	}

	Ok((files.pop().ok_or_else(|| error! { ExitCode::Failure, "mkmv: fatal error: Failed to get destination file from command line" })?, files))
}

// Prevent same file / directory copy.
fn check_paths(source: &Path, destination: &Path) -> common::Result<()> {
	match is_samefile(source, destination) {
		Err(error) => ret! {
			ExitCode::Failure, "mkmv: not renaming '{}' to '{}': {}", source.display(), destination.display(), error
		},
		Ok(result) => if result {
			ret! { SF_ERR_CODE, "mkmv: not renaming '{}' to '{}': Same file or file path", source.display(), destination.display() };
		},
	}

	Ok(())
}

// Check if user specified options prevent `source` from being moved.
fn prepare_destination(source_metadata: fs::Metadata, source: &Path, destination: &Path, mkmv: Options) -> common::Result<()> {
	if destination.to_string_lossy().ends_with('/') && !source_metadata.file_type().is_dir() {
		ret! { ExitCode::Failure, "mkmv: cannot overwrite directory '{}' with non-directory '{}'", destination.display(), source.display() };
	}

	if let Ok(destination_metadata) = fs::symlink_metadata(destination) {
		if mkmv.no_clobber {
			ret! { USER_ERR_CODE, "mkmv: not replacing '{}'", destination.display() };
		}

		if mkmv.update && source_metadata.mtime() <= destination_metadata.mtime() {
			ret! { USER_ERR_CODE, "mkmv: not replacing '{}', it is newer than '{}'", destination.display(), source.display() };
		}

		if destination_metadata.file_type().is_dir() && !source_metadata.file_type().is_dir() {
			ret! { ExitCode::Failure, "mkmv: cannot overwrite directory '{}' with non-directory '{}'", destination.display(), source.display() };
		}

		if !destination_metadata.file_type().is_dir() && source_metadata.file_type().is_dir() {
			ret! { ExitCode::Failure, "mkmv: cannot overwrite non-directory '{}' with directory '{}'", destination.display(), source.display() };
		}

		if mkmv.interactive {
			let mut choice = String::with_capacity(64);
			stderr! { "mkmv: do you want to overwrite '{}'? ", destination.display() };

			if let Err(error) = io::stdin().read_line(&mut choice) {
				ret! { ExitCode::Failure, "mkmv: not replacing '{}', failed to read the standard input: {}", destination.display(), error };
			}

			let choice = choice.strip_suffix('\n').unwrap_or(&choice);

			if choice != "Y" && choice != "y" {
				ret! { USER_ERR_CODE, "mkmv: not replacing '{}', received '{}' from the standard input", destination.display(), choice };
			}
		}
	}

	Ok(())
}

// Remove existing `destination` file / directory if necessary.
fn clobber_destination(destination: &Path, mkmv: &mut Options) -> common::Result<()> {
	mkmv.renamed = false;

	if mkmv.backup || mkmv.suffix.is_some() {
		let destination_bak = format! { "{}{}", destination.display(), mkmv.suffix.unwrap_or("~") };

		match fs::rename(destination, &destination_bak) {
			Err(error) => if error.raw_os_error() != Some(ENOENT) {
				ret! { ExitCode::Failure, "mkmv: failed to rename '{}' to '{}': {}", destination.display(), destination_bak, error };
			},
			Ok(()) => {
				mkmv.renamed = true;
			},
		}
	}

	if mkmv.force || mkmv.interactive {
		if let Err(error) = fs::remove_dir(destination) {
			if error.raw_os_error() == Some(ENOTDIR) {
				if let Err(error) = fs::remove_file(destination) {
					ret! { ExitCode::Failure, "mkmv: failed to overwrite target file '{}': {}", destination.display(), error };
				}
			}
			else if error.raw_os_error() != Some(ENOENT) {
				ret! { ExitCode::Failure, "mkmv: failed to overwrite target directory '{}': {}", destination.display(), error };
			}
		}
	}

	Ok(())
}

// Preserve file attributes during fallback copy.
fn preserve_attributes(source_metadata: fs::Metadata, source: &Path, destination: &Path, mkmv: Options) -> common::Result<()> {
	if let Err(error) = utimensat(destination, source_metadata.accessed().ok(), source_metadata.modified().ok(), UtimeFlags::NoFollow) {
		ret! { ExitCode::Failure, "mkmv: failed to preserve timestamps for '{}': {}", destination.display(), error };
	}

	if let Err(error) = lchown(destination, Some(source_metadata.uid()), Some(source_metadata.gid())) {
		ret! { ExitCode::Failure, "mkmv: failed to preserve ownership for '{}': {}", destination.display(), error };
	}

	if let Err(error) = fchmodat(destination, source_metadata.permissions(), FchmodFlags::NoFollow) {
		if error.raw_os_error() != Some(EOPNOTSUPP) {
			ret! { ExitCode::Failure, "mkmv: failed to preserve permissions for '{}': {}", destination.display(), error };
		}
	}

	// Try to remove `source` after a successful file copy.
	if source_metadata.file_type().is_dir() {
		if let Err(error) = fs::remove_dir(source) {
			ret! { SRC_ERR_CODE, "mkmv: failed to remove '{}' after copying it to '{}': {}", source.display(), destination.display(), error };
		}
	}
	else if let Err(error) = fs::remove_file(source) {
		ret! { SRC_ERR_CODE, "mkmv: failed to remove '{}' after copying it to '{}': {}", source.display(), destination.display(), error };
	}

	// Only show if `source` was removed without issue.
	if mkmv.verbose && mkmv.renamed {
		stdoutln! { "renamed '{}' -> '{}' (backup: '{1}{}')", source.display(), destination.display(), mkmv.suffix.unwrap_or("~") };
	}
	else if mkmv.verbose {
		stdoutln! { "renamed '{}' -> '{}'", source.display(), destination.display() };
	}

	Ok(())
}

// Return a formatted `common::error::Error` containing filetype.
macro_rules! styled_error {
	($source_metadata: ident, $source: expr, $destination: expr, $error: expr) => {{
		let filetype = match $source_metadata.permissions().mode() & S_IFMT {
			S_IFBLK  => "block device file ",
			S_IFCHR  => "character device file ",
			S_IFDIR  => "directory ",
			S_IFIFO  => "named pipe ",
			S_IFLNK  => "symbolic link ",
			S_IFSOCK => "unix domain socket ",
			_        => "",
		};

		error! { ExitCode::Failure, "mkmv: failed to rename {}'{}' to '{}': {}", filetype, $source.display(), $destination.display(), $error }
	}};
}

// "Move" files from `source` to `destination` during fallback 'copy and delete'.
fn file_move(source_metadata: fs::Metadata, source: &Path, destination: &Path) -> common::Result<()> {
	if source_metadata.file_type().is_symlink() {
		if let Err(error) = symlink(fs::read_link(source).unwrap_or_else(|_| PathBuf::from(source)), destination) {
			return Err(styled_error! { source_metadata, source, destination, error });
		}
	}
	else if source_metadata.file_type().is_file() {
		let mut source_fildes = fs::File::options().read(true).open(source).map_err(|error| error! {
			ExitCode::Failure, "mkmv: failed to open '{}' for reading: {}", source.display(), error
		})?;

		match fs::File::options().write(true).create(true).truncate(true).custom_flags(O_NOFOLLOW).open(destination) {
			Err(error) => return Err(styled_error! {
				source_metadata, source, destination, if error.raw_os_error() == Some(ELOOP) { io::Error::from_raw_os_error(EEXIST) } else { error }
			}),
			Ok(mut destination_fildes) => if let Err(error) = io::copy(&mut source_fildes, &mut destination_fildes) {
				return Err(styled_error! { source_metadata, source, destination, error });
			},
		}
	}
	else if let Err(error) = mknodat(destination, source_metadata.permissions(), MakeDev::from_metadata(&source_metadata)) {
		return Err(styled_error! { source_metadata, source, destination, error });
	}

	Ok(())
}

// "Move" directory content from `source` to `destination` during fallback 'copy and delete'.
fn directory_move(source: &Path, dest: &Path, mkmv: &mut Options) -> common::Result<()> {
	for dirent in fs::read_dir(source)? {
		let dirent = dirent?;

		let source_metadata = match dirent.metadata() {
			Err(error) => {
				stderrln! { "mkmv: cannot stat '{}': {}", dirent.path().display(), error };
				mkmv.exitcode = Some(ExitCode::Failure);
				continue;
			},
			Ok(metadata) => {
				metadata
			},
		};

		let (source, destination) = (&dirent.path(), &dest.join(dirent.file_name()));

		if let Err(error) = prepare_destination(source_metadata.clone(), source, destination, *mkmv) {
			stderrln! { "{}", error.message() };
			mkmv.exitcode = Some(error.exitcode());
			continue;
		}

		if let Err(error) = clobber_destination(destination, mkmv) {
			stderrln! { "{}", error.message() };
			mkmv.exitcode = Some(error.exitcode());
			continue;
		}

		// Create the leading component directories of `destination` if needed.
		let destination_dir = if source_metadata.file_type().is_dir() { destination } else { destination.parent().unwrap_or_else(|| Path::new("")) };

		if !destination_dir.as_os_str().is_empty() {
			if let Err(error) = fs::DirBuilder::new().recursive(true).create(destination_dir) {
				stderrln! { "mkmv: failed to create target directory '{}': {}", destination_dir.display(), error };
				mkmv.exitcode = Some(REC_ERR_CODE);
				continue;
			}
		}

		if source_metadata.file_type().is_dir() {
			if let Err(error) = directory_move(source, destination, mkmv) {
				stderrln! { "mkmv: failed to read directory '{}': {}", source.display(), error };
				mkmv.exitcode = Some(error.exitcode());
				continue;
			}
		}
		else {
			if let Err(error) = check_paths(source, destination) {
				stderrln! { "{}", error.message() };
				mkmv.exitcode = Some(error.exitcode());
				continue;
			}

			if let Err(error) = file_move(source_metadata.clone(), source, destination) {
				stderrln! { "{}", error.message() };
				mkmv.exitcode = Some(REC_ERR_CODE);
				continue;
			}
		}

		if let Err(error) = preserve_attributes(source_metadata, source, destination, *mkmv) {
			stderrln! { "{}", error.message() };
			mkmv.exitcode = Some(error.exitcode());
		}
	}

	Ok(())
}

#[doc(hidden)]
#[inline]
pub fn move_files(files: Vec<PathBuf>, mut mkmv: Options) -> common::Result<()> {
	let (dest, files) = input_validation(files, mkmv)?;

	// Keep track of moved files.
	let mut moved_files: HashSet<PathBuf> = HashSet::with_capacity(files.len());

	for source in files {
		let source_metadata = match fs::symlink_metadata(&source) {
			Err(error) => {
				stderrln! { "mkmv: cannot stat '{}': {}", source.display(), error };
				mkmv.exitcode = Some(ExitCode::Failure);
				continue;
			},
			Ok(metadata) => {
				metadata
			},
		};

		let (source, destination) = (&source, if mkmv.no_target { &dest } else { &dest.join(source.file_name().unwrap_or_default()) });

		// Prevent writing to `destination` multiple times.
		if moved_files.contains(destination) {
			stderrln! { "mkmv: not overwriting newly created '{}' with '{}'", destination.display(), source.display() };
			mkmv.exitcode = Some(ExitCode::Failure);
			continue;
		}

		// Check if `source` and `destination` refer to the same file.
		if let Err(error) = check_paths(source, destination) {
			stderrln! { "{}", error.message() };
			mkmv.exitcode = Some(error.exitcode());
			continue;
		}

		// Check if user options prevent destination from being overwritten.
		if let Err(error) = prepare_destination(source_metadata.clone(), source, destination, mkmv) {
			stderrln! { "{}", error.message() };
			mkmv.exitcode = Some(error.exitcode());
			continue;
		}

		// Remove existing destination files if needed.
		if let Err(error) = clobber_destination(destination, &mut mkmv) {
			stderrln! { "{}", error.message() };
			mkmv.exitcode = Some(error.exitcode());
			continue;
		}

		// Create the leading component directories of `destination` if needed.
		let destination_dir = if source_metadata.file_type().is_dir() { destination } else { destination.parent().unwrap_or_else(|| Path::new("")) };

		if !destination_dir.as_os_str().is_empty() {
			if let Err(error) = fs::DirBuilder::new().recursive(true).create(destination_dir) {
				ret! { ExitCode::Failure, "mkmv: failed to create target directory '{}': {}", destination_dir.display(), error };
			}
		}

		// Move `source` to `destination`.
		if let Err(error) = fs::rename(source, destination) {
			if error.raw_os_error() != Some(EXDEV) || mkmv.no_copy {
				stderrln! { "{}", styled_error! { source_metadata, source, destination, error } };
				mkmv.exitcode = Some(ExitCode::Failure);
				continue;
			}

			// Fallback to 'copy and delete' cycle if `rename()` fails.
			if source_metadata.file_type().is_dir() {
				if let Err(error) = directory_move(source, destination, &mut mkmv) {
					stderrln! { "mkmv: failed to recurse into directory '{}': {}", source.display(), error };
					mkmv.exitcode = Some(REC_ERR_CODE);
					continue;
				}
			}
			else {
				// Try to overwrite existing destination files to replicate `rename()` behavior.
				if let Err(error) = fs::remove_file(destination) {
					if error.raw_os_error() != Some(ENOENT) {
						stderrln! { "{}", styled_error! { source_metadata, source, destination, error } };
						mkmv.exitcode = Some(ExitCode::Failure);
						continue;
					}
				}
				
				if let Err(error) = file_move(source_metadata.clone(), source, destination) {
					stderrln! { "{}", error.message() };
					mkmv.exitcode = Some(error.exitcode());
					continue;
				}
			}

			if let Err(error) = preserve_attributes(source_metadata, source, destination, mkmv) {
				stderrln! { "{}", error.message() };
				mkmv.exitcode = Some(error.exitcode());
			}

			// Output messages have already been shown.
			moved_files.insert(PathBuf::from(destination));
			continue;
		}

		if mkmv.verbose && mkmv.renamed {
			stdoutln! { "renamed '{}' -> '{}' (backup: '{1}{}')", source.display(), destination.display(), mkmv.suffix.unwrap_or("~") };
		}
		else if mkmv.verbose {
			stdoutln! { "renamed '{}' -> '{}'", source.display(), destination.display() };
		}

		// Track moved files / directories.
		moved_files.insert(PathBuf::from(destination));
	}

	ret! { mkmv.exitcode.unwrap_or(ExitCode::Success), "" };
}
