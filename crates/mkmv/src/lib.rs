// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod mkmv;

use common::cli::getopt;
use common::cli::HasArg;
use common::cli::version;
use common::error::ExitCode;
use common::ret;
use common::stdoutln;

use mkmv::move_files;

use std::ffi::OsString;
use std::mem;
use std::path::PathBuf;

#[doc(hidden)]
pub const HELP: &str =
"mkmv - Move SOURCE or multiple SOURCEs to DESTINATION.

Usage:
    mkmv [OPTION...] SOURCE... DESTINATION

Options:
    -b, --backup
          Make backups of each existing DESTINATION file, with '~' as the backup
          suffix.

    -f, --force
          Overwrite existing DESTINATION files.

    -h, --help
          Display this help message and exit.

    -i, --interactive
          Show a prompt before overwriting an existing DESTINATION file.

    -N, --no-copy
          Do not copy SOURCE if `rename()` fails.

    -n, --no-clobber
          Do not overwrite existing DESTINATION files.

    -S, --suffix <SUFFIX>
          Make backups of existing DESTINATION files, using SUFFIX as the backup
          suffix.

    -T, --no-target-directory
          Treat DESTINATION as a normal file.

    -u, --update
          Do not overwrite an existing DESTINATION file, unless it is older than
          the corresponding SOURCE file.

    -V, --version
          Display version information and exit.

    -v, --verbose
          Show what is being done. Errors will always be printed to the standard
          error stream, even when this option is not specified.

You can report bugs by opening a new issue at <https://codeberg.org/lanre/trondheim/>
or by sending an email to <koladeolanrewaju@tutanota.com>.";

/// Struct used to process supported options in `mkmv`.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[doc(hidden)]
pub struct Options<'mkmv> {
	/// `-b` | `--backup`.
	pub backup: bool,

	/// Track exit codes during file copy.
	pub exitcode: Option<ExitCode>,

	/// `-f` | `--force`.
	pub force: bool,

	/// `-i` | `--interactive`.
	pub interactive: bool,

	/// `-n` | `--no-clobber`.
	pub no_clobber: bool,

	/// `-N` | `--no-copy`.
	pub no_copy: bool,

	/// `-T` | `--no-target-directory`.
	pub no_target: bool,

	/// Track backup file creation during copy.
	pub renamed: bool,

	/// `-S` | `--suffix`.
	pub suffix: Option<&'mkmv str>,

	/// `-u` | `--update`.
	pub update: bool,

	/// `-v | --verbose`.
	pub verbose: bool,
}

#[doc(hidden)]
#[inline]
pub fn try_main(user_input: Vec<OsString>) -> common::Result<()> {
	const OPTIONS: &[(&str, &str, HasArg)] = &[
		( "--backup",              "-b", HasArg::False ),
		( "--force",               "-f", HasArg::False ),
		( "--help",                "-h", HasArg::False ),
		( "--interactive",         "-i", HasArg::False ),
		( "--no-clobber",          "-n", HasArg::False ),
		( "--no-copy",             "-N", HasArg::False ),
		( "--no-target-directory", "-T", HasArg::False ),
		( "--suffix",              "-S", HasArg::True  ),
		( "--update",              "-u", HasArg::False ),
		( "--verbose",             "-v", HasArg::False ),
		( "--version",             "-V", HasArg::False ),
	];

	let ((options, files), mut mkmv, mut suffix) = (getopt(user_input, OPTIONS, "mkmv")?, Options::default(), String::new());

	for (option, mut argument) in options {
		match option.as_str() {
			"--backup" | "-b" => {
				mkmv.backup = true;
			},
			"--force" | "-f" => {
				mkmv.force = true;
			},
			"--help" | "-h" => {
				stdoutln! { "{}", HELP };
				return Ok(());
			},
			"--interactive" | "-i" => {
				mkmv.interactive = true;
			},
			"--no-clobber" | "-n" => {
				mkmv.no_clobber = true;
			},
			"--no-copy" | "-N" => {
				mkmv.no_copy = true;
			},
			"--no-target-directory" | "-T" => {
				mkmv.no_target = true;
			},
			"--suffix" | "-S" if !suffix.is_empty() => ret! {
				ExitCode::Failure, "mkmv: `{}` cannot be specified multiple times", option
			},
			"--suffix" | "-S" => {
				suffix = mem::take(&mut argument);
			},
			"--update" | "-u" => {
				mkmv.update = true;
			},
			"--verbose" | "-v" => {
				mkmv.verbose = true;
			},
			"--version" | "-V" => {
				stdoutln! { "{}", version("mkmv", env! { "CARGO_PKG_VERSION" }) };
				return Ok(());
			},
			_ => ret! {
				ExitCode::Failure, "mkmv: fatal error: Unhandled option `{}`", option
			},
		}
	}

	if !suffix.is_empty() {
		mkmv.suffix = Some(&suffix);
	}

	move_files(files.into_iter().map(PathBuf::from).collect(), mkmv)
}
