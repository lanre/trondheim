// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod mktouch;

use common::cli::getopt;
use common::cli::HasArg;
use common::cli::version;
use common::error::ExitCode;
use common::ret;
use common::stdoutln;

use mktouch::create_files;

use std::ffi::OsString;
use std::mem;
use std::path::Path;
use std::path::PathBuf;

#[doc(hidden)]
pub const HELP: &str =
"mktouch - Change FILE access and modification time.

Usage:
    mktouch [OPTION...] FILE...

Options:
    -a, --access-time
          Update FILE access time. FILE modification time is not updated if this
          option is specified, unless `--mod-time` is also specified.

    -c, --no-create
          Do not create any FILE that does not exist.

    -h, --help
          Display this help message and exit.

    -m, --mod-time
          Update FILE modification time. FILE access time is not updated if this
          option is specified, unless `--access-time` is also specified.

    -r, --reference <REF_FILE>
          Update the corresponding time of FILE using the timestamps of REF_FILE
          instead of the current time.

    -V, --version
          Display version information and exit.

You can report bugs by opening a new issue at <https://codeberg.org/lanre/trondheim/>
or by sending an email to <koladeolanrewaju@tutanota.com>.";

/// Struct used to process supported options in `mktouch`.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[doc(hidden)]
pub struct Options<'mktouch> {
	/// `-a` | `--access-time`.
	pub access_time: bool,

	/// `-c` | `--no-create`.
	pub no_create: bool,

	/// `-m` | `--mod-time`.
	pub mod_time: bool,

	/// `-r` | `--reference`.
	pub reference: Option<&'mktouch Path>,
}

#[doc(hidden)]
#[inline]
pub fn try_main(user_input: Vec<OsString>) -> common::Result<()> {
	const OPTIONS: &[(&str, &str, HasArg)] = &[
		( "--access-time", "-a", HasArg::False ),
		( "--no-create",   "-c", HasArg::False ),
		( "--help",        "-h", HasArg::False ),
		( "--mod-time",    "-m", HasArg::False ),
		( "--reference",   "-r", HasArg::True  ),
		( "--version",     "-V", HasArg::False ),
	];

	let ((options, files), mut mktouch, mut reference) = (getopt(user_input, OPTIONS, "mktouch")?, Options::default(), String::new());

	for (option, mut argument) in options {
		match option.as_str() {
			"--access-time" | "-a" => {
				mktouch.access_time = true;
			},
			"--no-create" | "-c" => {
				mktouch.no_create = true;
			},
			"--help" | "-h" => {
				stdoutln! { "{}", HELP };
				return Ok(());
			},
			"--mod-time" | "-m" => {
				mktouch.mod_time = true;
			},
			"--reference" | "-r" if !reference.is_empty() => ret! {
				 ExitCode::Failure, "mktouch: `{}` cannot be specified multiple times", option
			},
			"--reference" | "-r" => {
				reference = mem::take(&mut argument);
			},
			"--version" | "-V" => {
				stdoutln! { "{}", version("mktouch", env! { "CARGO_PKG_VERSION" }) };
				return Ok(());
			},
			_ => ret! {
				ExitCode::Failure, "mktouch: fatal error: Unhandled option `{}`", option
			},
		}
	}

	if !reference.is_empty() {
		mktouch.reference = Some(Path::new(&reference));
	}

	create_files(files.into_iter().map(PathBuf::from).collect(), mktouch)
}
