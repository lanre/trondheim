// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::ELOOP;
use common::constants::ENOENT;
use common::constants::O_NOFOLLOW;
use common::constants::RACE_ERR_CODE;
use common::constants::USER_ERR_CODE;
use common::error::ExitCode;
use common::error;
use common::fs::UtimeFlags;
use common::fs::utimensat;
use common::ret;
use common::stderrln;

use crate::Options;

use std::fs;
use std::os::unix::fs::OpenOptionsExt;
use std::path::Path;
use std::path::PathBuf;
use std::time::SystemTime;

// Extract timestamps from a reference file or use the current system time.
fn get_timestamps(mktouch: Options) -> common::Result<(Option<SystemTime>, Option<SystemTime>)> {
	let (mut atime, mut mtime) = (None, None);

	if let Some(path) = mktouch.reference {
		let metadata = path.symlink_metadata().map_err(|error| error! {
			ExitCode::Failure, "mktouch: cannot stat reference file '{}': {}", path.display(), error
		})?;

		if mktouch.access_time || !mktouch.mod_time {
			atime = metadata.accessed().ok();
		}

		if !mktouch.access_time || mktouch.mod_time {
			mtime = metadata.modified().ok();
		}
	}
	else {
		if mktouch.access_time || !mktouch.mod_time {
			atime = Some(SystemTime::now());
		}

		if !mktouch.access_time || mktouch.mod_time {
			mtime = Some(SystemTime::now());
		}
	}

	Ok((atime, mtime))
}

// Create the target file if needed.
fn create_target(source: &Path, mktouch: Options) -> common::Result<()> {
	if mktouch.no_create {
		if let Err(error) = fs::symlink_metadata(source) {
			if error.raw_os_error() == Some(ENOENT) {
				ret! { USER_ERR_CODE, "mktouch: not creating '{}'", source.display() };
			}
			else {
				ret! { ExitCode::Failure, "mktouch: failed to update timestamp of '{}': {}", source.display(), error };
			}
		}
	}
	else {
		let destination = source.parent().unwrap_or_else(|| Path::new(""));

		if !destination.as_os_str().is_empty() {
			if let Err(error) = fs::DirBuilder::new().recursive(true).create(destination) {
				ret! { ExitCode::Failure, "mktouch: failed to create target directory '{}': {}", destination.display(), error };
			}
		}

		if let Err(error) = fs::File::options().write(true).create(true).truncate(false).custom_flags(O_NOFOLLOW).open(source) {
			if error.raw_os_error() != Some(ELOOP) {
				ret! { ExitCode::Failure, "mktouch: failed to update timestamp of '{}': {}", source.display(), error };
			}
		}
	}

	Ok(())
}

#[doc(hidden)]
#[inline]
pub fn create_files(files: Vec<PathBuf>, mktouch: Options) -> common::Result<()> {
	if files.is_empty() {
		ret! { ExitCode::Failure, "mktouch: missing file operand" };
	}

	// Track exit codes.
	let mut exitcode: ExitCode = ExitCode::Success;

	// Set access and modification time.
	let (atime, mtime) = get_timestamps(mktouch)?;

	for source in files {
		if let Err(error) = create_target(&source, mktouch) {
			stderrln! { "{}", error.message() };
			exitcode = error.exitcode();
			continue;
		}

		if let Err(error) = utimensat(&source, atime, mtime, UtimeFlags::NoFollow) {
			if error.raw_os_error() == Some(ENOENT) {
				stderrln! { "mktouch: failed to update timestamp of '{}': Race condition detected", source.display() };
				exitcode = RACE_ERR_CODE;
			}
			else {
				stderrln! { "mktouch: failed to update timestamp of '{}': {}", source.display(), error };
				exitcode = ExitCode::Failure;
			}
		}
	}

	ret! { exitcode, "" };
}
