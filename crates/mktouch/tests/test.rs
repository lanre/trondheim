// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::cli::version;
use common::constants::EACCES;
use common::constants::ENOENT;
use common::constants::S_IFLNK;
use common::constants::S_IFMT;
use common::constants::S_IFREG;
use common::testbin;

use mktouch::HELP;

use std::fs;
use std::os::unix::fs::PermissionsExt;
use std::os::unix::fs::symlink;
use std::path::Path;

// Mktouch test binary.
const BIN: &str = env! { "CARGO_BIN_EXE_mktouch" };

// Mktouch version.
const VERSION: &str = env! { "CARGO_PKG_VERSION" };

// A beautiful bird.
fn canary<P: AsRef<Path>>(file: P) {
	let file = file.as_ref();
	drop(fs::remove_file(file));

	let parent = file.parent().unwrap_or_else(|| Path::new(""));

	if !parent.as_os_str().is_empty() {
		fs::create_dir_all(parent).unwrap();
	}

	fs::write(file, "Serinus canaria forma domestica").unwrap();
}

#[test]
fn test_help() {
	// mktouch --help
	let run = testbin! { BIN, "--help" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", HELP }
	};
}

#[test]
fn test_help_short_opt() {
	// mktouch --help
	let run = testbin! { BIN, "-h" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", HELP }
	};
}

#[test]
fn test_version() {
	// mktouch --version
	let run = testbin! { BIN, "--version" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", version("mktouch", VERSION) }
	};
}

#[test]
fn test_version_short_opt() {
	// mktouch --version
	let run = testbin! { BIN, "-V" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", version("mktouch", VERSION) }
	};
}

#[test]
fn test_help_args() {
	// mktouch --help=euorpa
	let run = testbin! { BIN, "--help=europa" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mktouch: `--help` does not accept any arguments but 'europa' was supplied\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_unrecognized_option() {
	// mktouch --esd
	let run = testbin! { BIN, "--esd" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mktouch: unrecognized option `--esd`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_unrecognized_option_hypen() {
	// mktouch -
	let run = testbin! { BIN, "-" };
 
	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mktouch: unrecognized option `-`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_invalid_short_opts() {
	// mktouch -esd
	let run = testbin! { BIN, "-esd" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mktouch: invalid option -- `e`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_no_command() {
	// mktouch
	let run = testbin! { BIN };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mktouch: missing file operand\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_no_command_positional() {
	// mktouch --
	let run = testbin! { BIN, "--" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mktouch: missing file operand\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_help_positional() {
	let dest = Path::new("--help");
	drop(fs::remove_file(dest));

	// mktouch -- --help
	let run = testbin! { BIN, "-- --help" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().len(),
		0
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_file() {
	let dest = Path::new("esd");
	drop(fs::remove_file(dest));

	// mktouch esd
	let run = testbin! { BIN, "esd" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().len(),
		0
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_file_positional() {
	let dest = Path::new("test_file_positional");
	drop(fs::remove_file(dest));

	// mktouch -- test_file_positional
	let run = testbin! { BIN, "-- test_file_positional" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().len(),
		0
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_pos_positional() {
	let dest = Path::new("--");
	drop(fs::remove_file(dest));

	// mktouch -- --
	let run = testbin! { BIN, "-- --" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().len(),
		0
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_no_create() {
	let dest = Path::new("test_no_create");
	drop(fs::remove_file(dest));

	// mktouch -- {dest}
	let run = testbin! { BIN, "--no-create {}", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(5)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "mktouch: not creating '{}'\n", dest.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
}

#[test]
fn test_permission() {
	// mktouch /root/test_permission
	let run = testbin! { BIN, "/root/test_permission" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mktouch: failed to update timestamp of '/root/test_permission': Permission denied (os error 13)\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

// This test will cause a stack overflow if it fails.
#[test]
fn test_deep_recursive_create() {
	let dest = Path::new("new_stack_test/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new1");

	let _ = fs::remove_dir_all("new_stack_test");

	// mktouch {dest}
	let run = testbin! { BIN, "{}", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().len(),
		0
	};

	fs::remove_dir_all("new_stack_test").unwrap();
}

#[test]
fn test_atime_mtime() {
	let dest = Path::new("test_atime_mtime");
	drop(fs::remove_file(dest));

	// mktouch {dest} --mod-time --access-time
	let run = testbin! { BIN, "{} --mod-time --access-time", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().len(),
		0
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_atime_mtime_shortopt() {
	let dest = Path::new("test_atime_mtime_shortopt");
	drop(fs::remove_file(dest));

	// mktouch {dest} --mod-time --access-time
	let run = testbin! { BIN, "{} -ma", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().len(),
		0
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_mtime_only() {
	let dest = Path::new("test_mtime_only");
	drop(fs::remove_file(dest));

	// mktouch test_mtime_only --mod-time
	let run = testbin! { BIN, "test_mtime_only --mod-time" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().len(),
		0
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_atime_only() {
	let dest = Path::new("test_atime_only");
	drop(fs::remove_file(dest));

	// mktouch test_atime_only --access-time
	let run = testbin! { BIN, "test_atime_only --mod-time" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().len(),
		0
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_same_file_reference() {
	let dest = Path::new("test_same_file_reference");
	canary(dest);

	// mktouch {dest} {dest}
	let run = testbin! { BIN, "{} {}", dest.display(), dest.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		fs::read_to_string(dest).unwrap(),
		String::from("Serinus canaria forma domestica")
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_reference() {
	let dest = Path::new("test_reference");
	canary(dest);

	// mktouch --reference /bin/bash {dest}
	let run = testbin! { BIN, "--reference /bin/bash {}", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().modified().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().modified().unwrap()
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().accessed().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().accessed().unwrap()
	};
	assert_eq! {
		fs::read_to_string(dest).unwrap(),
		String::from("Serinus canaria forma domestica")
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_reference_mtime_only() {
	let dest = Path::new("test_reference_mtime_only");
	canary(dest);

	// mktouch --reference /bin/bash {dest} --mod-time
	let run = testbin! { BIN, "--reference /bin/bash {} --mod-time", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().modified().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().modified().unwrap()
	};
	assert_ne! {
		dest.symlink_metadata().unwrap().accessed().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().accessed().unwrap()
	};
	assert_eq! {
		fs::read_to_string(dest).unwrap(),
		String::from("Serinus canaria forma domestica")
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_reference_atime_only() {
	let dest = Path::new("test_reference_atime_only");
	canary(dest);

	// mktouch --reference /bin/bash {dest} --access-time
	let run = testbin! { BIN, "--reference /bin/bash {} --access-time", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_ne! {
		dest.symlink_metadata().unwrap().modified().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().modified().unwrap()
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().accessed().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().accessed().unwrap()
	};
	assert_eq! {
		fs::read_to_string(dest).unwrap(),
		String::from("Serinus canaria forma domestica")
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_reference_both() {
	let dest = Path::new("test_reference_both");
	canary(dest);

	// mktouch --reference /bin/bash {dest} --mod-time --access-time
	let run = testbin! { BIN, "--reference /bin/bash {} --mod-time --access-time", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode(),
		S_IFREG | 0o644
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().modified().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().modified().unwrap()
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().accessed().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().accessed().unwrap()
	};
	assert_eq! {
		fs::read_to_string(dest).unwrap(),
		String::from("Serinus canaria forma domestica")
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_permissions() {
	/////////////////////////////////////////////////////////////////////////////////
	// mktouch /root/test_permission --no-create
	/////////////////////////////////////////////////////////////////////////////////
	let run = testbin! { BIN, "/root/test_permission --no-create" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mktouch: failed to update timestamp of '/root/test_permission': Permission denied (os error 13)\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	/////////////////////////////////////////////////////////////////////////////////
	// mktouch /root/test_permission
	/////////////////////////////////////////////////////////////////////////////////
	let run = testbin! { BIN, "/root/test_permission" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mktouch: failed to update timestamp of '/root/test_permission': Permission denied (os error 13)\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_reference_error() {
	let dest = Path::new("test_reference_error");
	drop(fs::remove_file(dest));

	/////////////////////////////////////////////////////////////////////////////////
	// mktouch --reference /bin/does_not_exist {dest}
	/////////////////////////////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--reference /bin/does_not_exist {}", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mktouch: cannot stat reference file '/bin/does_not_exist': No such file or directory (os error 2)\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	// NOTE: mktouch will not create the target file if the specified reference file does not exist
	// or is un-stat-able.
	assert_eq! {
		fs::symlink_metadata("/bin/does_not_exist").unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
	assert_eq! {
		dest.symlink_metadata().unwrap_err().raw_os_error(),
		Some(ENOENT)
	};

	/////////////////////////////////////////////////////////////////////////////////
	// mktouch --reference /root/does_not_exist {dest}
	/////////////////////////////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--reference /root/does_not_exist {}", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mktouch: cannot stat reference file '/root/does_not_exist': Permission denied (os error 13)\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	// NOTE: mktouch will not create the target file if the specified reference file does not exist
	// or is un-stat-able.
	assert_eq! {
		fs::symlink_metadata("/root/does_not_exist").unwrap_err().raw_os_error(),
		Some(EACCES)
	};
	assert_eq! {
		dest.symlink_metadata().unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
}

#[test]
fn test_reference_symlink() {
	let dest = Path::new("test_reference_symlink");
	drop((fs::remove_file(dest), symlink("does_not_exist", dest).unwrap()));

	// mktouch --reference /bin/bash {dest}
	let run = testbin! { BIN, "--reference /bin/bash {}", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	// NOTE: `mktouch` will NOT write through dangling symbolic links, as that will create
	// the missing target file.
	assert_eq! {
		fs::symlink_metadata("does_not_exist").unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().modified().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().modified().unwrap()
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().accessed().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().accessed().unwrap()
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_reference_symlink_atime_only() {
	let dest = Path::new("test_reference_symlink_atime_only");
	drop((fs::remove_file(dest), symlink(dest, dest).unwrap()));

	// mktouch --reference /bin/bash {dest} --access-time
	let run = testbin! { BIN, "--reference /bin/bash {} --access-time", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_ne! {
		dest.symlink_metadata().unwrap().modified().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().modified().unwrap()
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().accessed().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().accessed().unwrap()
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_reference_symlink_mtime_only() {
	let dest = Path::new("test_reference_symlink_mtime_only");
	drop((fs::remove_file(dest), symlink(dest, dest).unwrap()));

	// mktouch --reference /bin/bash {dest} --mod-time
	let run = testbin! { BIN, "--reference /bin/bash {} --mod-time", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().modified().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().modified().unwrap()
	};
	assert_ne! {
		dest.symlink_metadata().unwrap().accessed().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().accessed().unwrap()
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_reference_symlink_both() {
	let dest = Path::new("test_reference_symlink_both");
	drop((fs::remove_file(dest), symlink("/does/not/exist", dest).unwrap()));

	// mktouch --reference /bin/bash {dest} --mod-time --access-time
	let run = testbin! { BIN, "--reference /bin/bash {} --mod-time --access-time", dest.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dest.symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().modified().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().modified().unwrap()
	};
	assert_eq! {
		dest.symlink_metadata().unwrap().accessed().unwrap(),
		fs::symlink_metadata("/bin/bash").unwrap().accessed().unwrap()
	};

	fs::remove_file(dest).unwrap();
}
