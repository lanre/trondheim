# mkcp - Advanced File Copy Utility

`mkcp` is a command-line utility for copying files and directories, with control over preserved file attributes, backups and overwrites. It supports copying files as symbolic/hard links, recursive file copying...

## Table of content
- [Installation](#installation)
- [Usage](#usage)
- [Options](#options)
- [Exit Codes](#exitCodes)
- [License](#license)

## Installation
```shell
# Build mkcp as a standalone static binary.
$ RUSTFLAGS="-C target-feature=+crt-static" cargo build --release --bin mkcp

# Copy the binary to `$PATH`.
$ cp --preserve=mode target/release/mkcp DESTINATION

# Or build trondheim with all its built-in programs as a static binary.
$ RUSTFLAGS="-C target-feature=+crt-static" cargo build --release --bin trondheim

# Install `trondheim` and all its built-in programs in `$PATH`.
$ ./target/release/trondheim --install <DESTINATION>
```

## Usage
```shell
mkcp [OPTION...] SOURCE... DESTINATION
```
`SOURCE` is the file / directory to copy, while `DESTINATION` is the location to copy `SOURCE` to. If `DESTINATION` does not exist, it will be created as an empty directory before the copy occurs.

> [!WARNING]
> `DESTINATION` is **always** assumed to be a directory, unless `-T` / `--no-target-directory` is specified. If `--no-target-directory` is specified, only a single `SOURCE` file / directory can be specified.

> [!WARNING]
> `mkcp` and `GNU cp` **are not** the same program, and have completely different behaviors, **even** with similarly named options. **DO NOT** open an issue complaining about disparities between both programs, without first reading this guide.

## Options
```shell
mkcp - Copy SOURCE or multiple SOURCEs to DESTINATION.

Usage:
    mkcp [OPTION...] SOURCE... DESTINATION

Options:
    -A, --attributes-only
          Copy only file permission bits from SOURCE.

    -b, --backup
          Make backups of each existing DESTINATION file, with '~' as the backup
          suffix.

    -F, --follow
          Follow any symbolic link encountered in SOURCE. This option is ignored
          when `--preserve filetype` is specified.

    -f, --force
          Overwrite existing DESTINATION files.

    -h, --help
          Display this help message and exit.

    -i, --interactive
          Show a prompt before overwriting an existing DESTINATION file.

    -l, --link
          Create hard links to SOURCE instead of copying.

    -n, --no-clobber
          Do not overwrite existing DESTINATION files.

    -p, --preserve <ATTR...>
          Preserve the specified ATTRibutes when copying SOURCE. Where ATTR is a
          comma seperated list of file attributes.
          all         - Preserve all supported ATTRibutes.
          filetype    - Preserve filesystem node type.
          ownership   - Preserve owner ID and group ID.
          permissions - Preserve file permission bits.
          timestamps  - Preserve last access time and last modification time.

    -R, -r, --recursive
          Copy directories recursively.

    -S, --suffix <SUFFIX>
          Make backups of existing DESTINATION files, using SUFFIX as the backup
          suffix.

    -s, --symbolic-link
          Create symbolic links to SOURCE instead of copying.

    -T, --no-target-directory
          Treat DESTINATION as a normal file.

    -u, --update
          Do not overwrite an existing DESTINATION file, unless it is older than
          the corresponding SOURCE file.

    -V, --version
          Display version information and exit.

    -v, --verbose
          Show what is being done. Errors will always be printed to the standard
          error stream, even when this option is not specified.

You can report bugs by opening a new issue at <https://codeberg.org/lanre/trondheim/>
or by sending an email to <koladeolanrewaju@tutanota.com>.
```
---

#### `-A` / `--attributes-only`
Copy only file permissions bits (excluding `S_ISGID`, `S_ISUID` and `S_ISVTX`) from `SOURCE`. No other file content is copied. This option is useful for replicating file permissions or replicating directory structures without file data transfer. 

> [!WARNING]
> `mkcp` **does not** preserve any file permission bits during copy by default. To preserve file permissions, `--preserve permissions` **must** be specified as an option.

```shell
# Copying /bin/cat and forgetting to specify `--preserve permissions`.
$ mkcp /bin/cat .

# The file is copied using the programs umask and no execute bits are preserved.
$ ls -l cat
total 44
-rw-r--r-- 1 user user 43392 Feb  12 12:12 cat

# To avoid copying the file again, especially if it's a large file.
$ mkcp --attributes-only /bin/cat .

# The permissions have been copied from /bin/cat without copying the file again
# or needing to use chmod.
$ ls -l cat
total 44
-rwxr-xr-x 1 user user 43392 Feb  12 12:12 cat
```

---

#### `-b` / `--backup`
Create backups of existing files in `DESTINATION` with the same name as `SOURCE`. This is the same option as `-S` / `--suffix`, except it uses `~` as the suffix for the backup file.

> [!NOTE]
> If `--suffix` and `--backup` are specified together, `--suffix` will have higher precedence and `SUFFIX` will be used instead of `~`.

```shell
$ ls
cat

# Trying to overwrite cat with /bin/bash.
$ mkcp /bin/bash --no-target-directory cat --backup

$ ls
cat cat~
```

---

#### `-F` / `--follow`
Follow any symbolic links in `SOURCE` and copy the target file instead of the symbolic link. 

> [!WARNING]
> Symbolic links in `SOURCE` are **never** followed by default, and the symbolic link itself is copied.

> [!NOTE]
> `SOURCE` will not be followed and will still be copied as a symbolic link if `--preserve filetype` is also specified.

```shell
$ ls -l
total 0
lrwxrwxrwx 1 user user 9 Feb  13 13:13 test -> /bin/bash

$ mkcp test --no-target-directory example

$ ls -l
total 0
lrwxrwxrwx 1 user user 9 Feb  13 13:13 example -> /bin/bash
lrwxrwxrwx 1 user user 9 Feb  13 13:13 test -> /bin/bash

# An error will occur here, since `SOURCE` and `DESTINATION` "are" the same file.
$ mkcp test --no-target-directory example --follow
mkcp: not copying 'test' to 'example': Same file or file path

# Using `--force` here will not work, since the check for `SOURCE` and `DESTINATION` origins
# preceeds any file processing.
$ mkcp test --no-target-directory example --follow --force
mkcp: not copying 'test' to 'example': Same file or file path

# Follow the symbolic link `test` and preserve the permissions of the target file.
$ rm example; mkcp test --no-target-directory example --follow --verbose --preserve permissions
'test' -> 'example'

$ ls -l
total 1396
-rwxr-xr-x 1 user user 1426632 Feb  13 13:14 example
lrwxrwxrwx 1 user user       9 Feb  13 13:13 test -> /bin/bash
```

---

#### `-f` / `--force`
Delete existing `DESTINATION` files with the same name as `SOURCE` before attempting to copy.

```shell
$ ls -l
total 1396
-rwxr-xr-x 1 user user 1426632 Feb  13 13:14 example
lrwxrwxrwx 1 user user       9 Feb  13 13:13 test -> /bin/bash

# An error will occur here, because `mkcp` does not allow writing regular files through symbolic links.
# Dangling or otherwise.
$ mkcp /bin/ls --no-target-directory test
mkcp: failed to copy '/bin/ls' to 'test': File exists (os error 17)

# Delete test before attempting to copy, then copy the file without preserving permissions.
$ mkcp --force /bin/cat --no-target-directory test --verbose
'/bin/ls' -> 'test'

$ ls -l 
-rwxr-xr-x 1 user user 1426632 Feb  2 11:16 example
-rw-r--r-- 1 user user  154432 Feb  2 11:23 test
```

---

#### `-i` / `--interactive`
Show a prompt asking for user consent, before attempting to delete any existing `DESTINATION` file with the same name as `SOURCE`.

> [!NOTE]
> If `-interactive` and `--force` are specified together, `--interactive` will have higher precedence and a prompt will be shown.

```shell
# Positive confirmation.
$ mkcp /bin/ls --interactive --no-target-directory example --verbose
mkcp: do you want to overwrite 'example'? y
'/bin/ls' -> 'example'

# Negative confirmation.
mkcp /bin/ls --interactive --no-target-directory example --verbose
mkcp: do you want to overwrite 'example'? n
mkcp: not replacing 'example', received 'n' from the standard input
```

---

#### `-l` / `--link`
Create hard links of `SOURCE` files to `DESTINATION` instead of copying.

> [!NOTE]
> Hardlinks can only be created if `SOURCE` and `DESTINATION` exist on the same filesystem, an error will result otherwise.

> [!NOTE]
> `--link` and `--symbolic-link` are mutually exclusive. You can either create a symbolic link or a hardlink, but not both at the same time.

```shell
$ ls -il
7475759 -rw-r--r-- 1 user user 154432 Feb  2 12:22 example

$ mkcp --link example --no-target-directory ls --verbose
'example' -> 'ls'

$ ls -il
7475759 -rw-r--r-- 2 user user 154432 Feb  2 12:22 example
7475759 -rw-r--r-- 2 user user 154432 Feb  2 12:22 ls
```

---

#### `-n` / `--no-clobber`
Do not overwrite / delete any `DESTINATION` file that has the same name as `SOURCE`.

> [!NOTE]
> `--no-clobber` has higher precedence over `--update`, if both options are specified together.

> [!NOTE]
> `--no-clobber` is mutually exclusive with, and cannot be specified with `--backup`, `--force`, `--interactive` or `--suffix`.

```shell
$ ls
example test

# Note that an error is still printed, even though this behavior was user requested.
$ mkcp example --no-target-directory test --no-clobber
mkcp: not replacing 'test'

# Also note that the exit code is non-zero.
# `mkcp` behaves this way, so users are notified about skipped files, and which files were skipped.
# This is particularly useful when `mkcp` is run in shell script.
$ echo $?
5
```

---

#### `-p` / `--preserve <ATTRIBUTE...>`
Preserve `ATTRIBUTE` from copied `SOURCE` files in `DESTINATION`. Where `ATTRIBUTE` is a comma separated list of supported attributes.

|**Attribute**  | **Description**                                                                                                             |
|---------------|-----------------------------------------------------------------------------------------------------------------------------|
| `all`         | All supported `ATTRIBUTE`s. `--preserve all` is the same as `--preserve filetype,ownership,permissions,timestamps`          |
| `filetype`    | Preserve the filesystem node type. For instance copy a symbolic link as a symbolic link, a UNIX domain socket file as-is... |
| `ownership`   | Preserve the file ownership info (UID and GID).                                                                             |
| `permissions` | Preserve all the file permissions bits (S_IRUSR, S_IWUSR...).                                                               |
| `timestamps`  | Preserve the last access and last modification time.                                                                        |


> [!WARNING]
> `mkcp` **does not** preserve any file attributes by default. Any attributes the user wishes to preserve from `SOURCE` **must** be explicitly specified.

> [!NOTE]
> The order of precedence for preservation is `filetype` -> `timestamps` -> `ownership` -> `permissions`. If any chain in the link fails, the remaining options are not preserved. For instance if preserving the timestamps fails, the ownership and permissions are **not** preserved, and so on.

> [!NOTE]
> If preserving the `filetype` fails for any reason, the file is **not** copied. However, if preserving either of `timestamps`, `ownership` or `permissions` fails, the file **is still** copied but an error message is shown and the exit code is updated accordingly.

> [!NOTE]
> `--preserve filetype` is mutually exclusive with `--link` or `--symbolic-link`.

```shell
# Preserve file permission bits.
# `S_ISVTX`, `S_ISGID` and `S_ISUID` bit are not preserved unless file `ownership` is also preserved.
$ mkcp --preserve permissions /bin/sudo . --verbose
'/bin/sudo' -> './sudo'

$ ls -l
-rwxr-xr-x 1 user user 289976 Feb  2 22:46 sudo

# Preserve file permission bit and file `UID` and `GID`.
# The file is owned by the root user, so root permissions are needed to preserve its `UID` and `GID`.
$ sudo mkcp --preserve permissions,ownership /bin/sudo . --verbose
'/bin/sudo' -> './sudo'

$ ls -l
-rwsr-xr-x 1 root root 289976 Feb  2 22:46 sudo

# Preserve timestamps and file permissions.
$ mkcp --preserve timestamps,permissions /bin/bash --verbose .
'/bin/bash' -> './bash'

$ ls -l /bin/bash; ls -l bash
-rwxr-xr-x 1 root root 1426632 Sep 24 12:37 /bin/bash
-rwxr-xr-x 1 user user 1426632 Sep 24 12:37 bash

# Create a new named pipe (FIFO) file.
$ mkfifo test

# This operation will block indefinitely as it tries to read from an unconnected pipe.
$ mkcp test --no-target-directory example --verbose

# Preserve the `filetype`.
# This operation will succeed and copy the FIFO file as-is.
$ mkcp test --no-target-directory example --verbose --preserve filetype
'test' -> 'example'

$ ls -l
prw-r--r-- 1 user user 0 Feb  2 23:04 example
prw-r--r-- 1 user user 0 Feb  2 23:04 test
```

---

#### `-R` / `-r` / `--recursive`
Copy directories and subdirectories recursively from `SOURCE` to `DESTINATION`.

```shell
# Trying to copy a directory without specifying `--recursive` will result in an error.
$ mkcp ~/.local/share newfolder
mkcp: `--recursive` not specified: Omitting directory '/home/user/.local/share'

$ mkcp ~/.local/share newfolder --recursive

# `SOURCE` is copied to the specified directory `newfolder`.
$ ls -l newfolder/
total 4
drwxr-xr-x 35 user user 4096 Feb  3 14:06 share
```

---

#### `-S` / `--suffix <SUFFIX>`
Create backups of existing files in `DESTINATION` with the same name as `SOURCE`. This is the same option as `-b` / `--backup`, except it uses the specified `SUFFIX` as the suffix for the backup file.

```shell
$ ls
cat

# Trying to overwrite cat with /bin/bash.
$ mkcp /bin/bash --no-target-directory cat --suffix .bak

$ ls
cat cat.bak
```

---

#### `-s` / `--symbolic-link`
Create symbolic links of `SOURCE` files to `DESTINATION` instead of copying.

```shell
$ ls -l
-rw-r--r-- 1 user user 0 Feb  3 14:14 new

$ mkcp --symbolic-link new test --no-target-directory --verbose
'example' -> 'ls'

$ ls -l
total 0
-rw-r--r-- 1 user user 0 Feb  3 14:14 new
lrwxrwxrwx 1 user user 3 Feb  3 14:14 test -> new
```

---

#### `-T` / `--no-target-directory`
Treat `DESTINATION` as a file not a directory. By default, `mkcp` **always** assumes the specified `DESTINATION` is a directory and will attempt to copy `SOURCE` **into** `DESTINATION`. This option explicitly requests that `DESTINATION` not be treated like a directory.

> [!NOTE]
> A side effect of specifying this option is that only a single `SOURCE` file / directory can be specified as input.

```shell
# Not specifying `--no-target-directory` implies that `test` is a folder and should be treated as such.
$ mkcp /bin/bash --verbose test
'/bin/bash' -> 'test/bash'

$ ls -l test/bash
-rw-r--r-- 1 user user 1426632 Feb  3 14:24 test/bash

# Multiple `SOURCE`s can be specified.
$ mkcp /bin/bash /bin/cat /bin/ls --verbose test
'/bin/bash' -> 'test/bash'
'/bin/cat' -> 'test/cat'
'/bin/ls' -> 'test/ls'

$ ls -l test
-rw-r--r-- 1 user user 1426632 Feb  3 14:26 bash
-rw-r--r-- 1 user user   43392 Feb  3 14:26 cat
-rw-r--r-- 1 user user  154432 Feb  3 14:26 ls

# This will fail with an error, since `test` currently exists as a directory and `SOURCE` is not.
$ mkcp /bin/bash --verbose test --no-target-directory
mkcp: cannot overwrite directory 'test' with non-directory '/bin/bash'

# `test` itself is replaced with `SOURCE` since it is no longer treated like a directory.
$ rm -r test; mkcp /bin/bash --verbose test --no-target-directory
'/bin/bash' -> 'test'

# Multiple `SOURCE`s cannot be specified.
# Here, `/bin/bash` is assumed as the `SOURCE` and `/bin/cat` is assumed as the `DESTINATION`.
$ mkcp /bin/bash /bin/cat /bin/ls --verbose test --no-target-directory
mkcp: extra operand '/bin/ls' after destination file '/bin/cat'
```

---

#### `-u` / `--update`
Do not overwrite / delete any existing `DESTINATION` file that has the same name as `SOURCE`, unless `SOURCE` is a more recent version of the file.

```shell
$ ls
example test

# Note that an error is still printed, even though this behavior was user requested.
$ mkcp example --no-target-directory test --update
mkcp: not replacing 'test', it is newer than 'example'

# Also note that, same as `--no-clobber`, the exit code is non-zero.
$ echo $?
5
```

---

## ExitCodes
| Exit code | Description                                                               |
|-----------|---------------------------------------------------------------------------|
| `0`       | All files were copied from `SOURCE` to `DESTINATION` without issues.      |
| `1`       | Generic errors (SOURCE doesn't exist, permission issues...).              |
| `2`       | Command line errors (Invalid Input, missing arguments, syntax errors...). |
| `4`       | Errors were encountered during the recursive walk of a directory.         |
| `5`       | User specified options like `--update` caused skipped operations          |
| `7`       | `SOURCE` and `DESTINATION` are the same file or file path.                |

## License

trondheim and all its components / built-in programs are licensed under [GPL-3.0-or-later](LICENSE).

Copyright (c) 2024-2025, Kolade Ayomide Olanrewaju <<koladeolanrewaju@tutanota.com>>.
