// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod mkcp;

use common::cli::getopt;
use common::cli::HasArg;
use common::cli::version;
use common::error::ExitCode;
use common::ret;
use common::stdoutln;

use mkcp::copy_files;

use std::ffi::OsString;
use std::mem;
use std::path::PathBuf;

#[doc(hidden)]
pub const HELP: &str =
"mkcp - Copy SOURCE or multiple SOURCEs to DESTINATION.

Usage:
    mkcp [OPTION...] SOURCE... DESTINATION

Options:
    -A, --attributes-only
          Copy only file permission bits from SOURCE.

    -b, --backup
          Make backups of each existing DESTINATION file, with '~' as the backup
          suffix.

    -F, --follow
          Follow any symbolic link encountered in SOURCE. This option is ignored
          when `--preserve filetype` is specified.

    -f, --force
          Overwrite existing DESTINATION files.

    -h, --help
          Display this help message and exit.

    -i, --interactive
          Show a prompt before overwriting an existing DESTINATION file.

    -l, --link
          Create hard links to SOURCE instead of copying.

    -n, --no-clobber
          Do not overwrite existing DESTINATION files.

    -p, --preserve <ATTR...>
          Preserve the specified ATTRibutes when copying SOURCE. Where ATTR is a
          comma seperated list of file attributes.
          all         - Preserve all supported ATTRibutes.
          filetype    - Preserve filesystem node type.
          ownership   - Preserve owner ID and group ID.
          permissions - Preserve file permission bits.
          timestamps  - Preserve last access time and last modification time.

    -R, -r, --recursive
          Copy directories recursively.

    -S, --suffix <SUFFIX>
          Make backups of existing DESTINATION files, using SUFFIX as the backup
          suffix.

    -s, --symbolic-link
          Create symbolic links to SOURCE instead of copying.

    -T, --no-target-directory
          Treat DESTINATION as a normal file.

    -u, --update
          Do not overwrite an existing DESTINATION file, unless it is older than
          the corresponding SOURCE file.

    -V, --version
          Display version information and exit.

    -v, --verbose
          Show what is being done. Errors will always be printed to the standard
          error stream, even when this option is not specified.

You can report bugs by opening a new issue at <https://codeberg.org/lanre/trondheim/>
or by sending an email to <koladeolanrewaju@tutanota.com>.";

/// Struct used to process supported file ATTRibutes.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[doc(hidden)]
pub struct Attribute {
	/// `-p | --preserve filetype`.
	pub filetype: bool,

	/// `-p | --preserve ownership`.
	pub ownership: bool,

	/// `-p | --preserve permissions`.
	pub permissions: bool,

	/// `-p | --preserve timestamps`.
	pub timestamps: bool,
}

impl Attribute {
	#[doc(hidden)]
	#[inline]
	pub fn parse_attributes<A: AsRef<str>, B: AsRef<str>>(&mut self, argument: A, option: B) -> common::Result<Attribute> {
		for attribute in argument.as_ref().split(',') {
			match attribute {
				"all" => {
					self.filetype    = true;
					self.ownership   = true;
					self.permissions = true;
					self.timestamps  = true;
				},
				"filetype" => {
					self.filetype = true;
				},
				"ownership" => {
					self.ownership = true;
				},
				"permissions" => {
					self.permissions = true;
				},
				"timestamps" => {
					self.timestamps = true;
				},
				_ => ret! {
					ExitCode::Failure, "mkcp: invalid argument '{}' found for `{}`", attribute, option.as_ref()
				},
			}
		}

		Ok(self.to_owned())
	}
}

/// Custom type for specifying file copy operation.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[doc(hidden)]
pub enum CopyMode {
	/// `-A` | `--attributes-only`.
	AttributesOnly,

	/// Copy files.
	#[default]
	FileCopy,

	/// `-l` | `--link`.
	HardLink,

	/// `-s` | `--symbolic-link`.
	SymbolicLink,
}

/// Struct used to process supported options in `mkcp`.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[doc(hidden)]
pub struct Options<'mkcp> {
	/// `-b` | `--backup`.
	pub backup: bool,

	/// File operation mode.
	pub copy_mode: CopyMode,

	/// Track exit codes during file copy.
	pub exitcode: Option<ExitCode>,

	/// `-F` | `--follow`.
	pub follow: bool,

	/// `-f` | `--force`.
	pub force: bool,

	/// `-i` | `--interactive`.
	pub interactive: bool,

	/// `-n` | `--no-clobber`.
	pub no_clobber: bool,

	/// `-T` | `--no-target-directory`.
	pub no_target: bool,

	/// `-p` | `--preserve`.
	pub preserve: Attribute,

	/// `-R` | `-r` | `--recursive`.
	pub recursive: bool,

	/// Track backup file creation during copy.
	pub renamed: bool,

	/// `-S` | `--suffix`.
	pub suffix: Option<&'mkcp str>,

	/// `-u` | `--update`.
	pub update: bool,

	/// `-v` | `--verbose`.
	pub verbose: bool,
}

#[doc(hidden)]
#[inline]
pub fn try_main(user_input: Vec<OsString>) -> common::Result<()> {
	const OPTIONS: &[(&str, &str, HasArg)] = &[
		( "--attributes-only",     "-A", HasArg::False ),
		( "--backup",              "-b", HasArg::False ),
		( "--follow",              "-F", HasArg::False ),
		( "--force",               "-f", HasArg::False ),
		( "--help",                "-h", HasArg::False ),
		( "--interactive",         "-i", HasArg::False ),
		( "--link",                "-l", HasArg::False ),
		( "--no-clobber",          "-n", HasArg::False ),
		( "--no-target-directory", "-T", HasArg::False ),
		( "--preserve",            "-p", HasArg::True  ),
		( "--recursive",           "-R", HasArg::False ),
		( "--recursive",           "-r", HasArg::False ),
		( "--suffix",              "-S", HasArg::True  ),
		( "--symbolic-link",       "-s", HasArg::False ),
		( "--update",              "-u", HasArg::False ),
		( "--verbose",             "-v", HasArg::False ),
		( "--version",             "-V", HasArg::False ),
	];

	let ((options, files), mut mkcp, mut suffix) = (getopt(user_input, OPTIONS, "mkcp")?, Options::default(), String::new());

	for (option, mut argument) in options {
		match option.as_str() {
			"--attributes-only" | "-A" if mkcp.copy_mode == CopyMode::HardLink || mkcp.copy_mode == CopyMode::SymbolicLink => ret! {
				ExitCode::Failure, "mkcp: `--attributes-only` cannot be specified with `--link` or `--symbolic-link`"
			},
			"--attributes-only" | "-A" => {
				mkcp.copy_mode = CopyMode::AttributesOnly;
				mkcp.preserve.parse_attributes("permissions", option)?;
			},
			"--backup" | "-b" => {
				mkcp.backup = true;
			},
			"--follow" | "-F" => {
				mkcp.follow = true;
			},
			"--force" | "-f" => {
				mkcp.force = true;
			},
			"--help" | "-h" => {
				stdoutln! { "{}", HELP };
				return Ok(());
			},
			"--interactive" | "-i" => {
				mkcp.interactive = true;
			},
			"--link" | "-l" if mkcp.copy_mode == CopyMode::AttributesOnly || mkcp.copy_mode == CopyMode::SymbolicLink => ret! {
				ExitCode::Failure, "mkcp: `--link` cannot be specified with `--attributes-only` or `--symbolic-link`"
			},
			"--link" | "-l" => {
				mkcp.copy_mode = CopyMode::HardLink;
			},
			"--no-clobber" | "-n" => {
				mkcp.no_clobber = true;
			},
			"--no-target-directory" | "-T" => {
				mkcp.no_target = true;
			},
			"--preserve" | "-p" => {
				mkcp.preserve.parse_attributes(argument, option)?;
			},
			"--recursive" | "-r" | "-R" => {
				mkcp.recursive = true;
			},
			"--suffix" | "-S" if !suffix.is_empty() => ret! {
				ExitCode::Failure, "mkcp: `{}` cannot be specified multiple times", option
			},
			"--suffix" | "-S" => {
				suffix = mem::take(&mut argument);
			},
			"--symbolic-link" | "-s" if mkcp.copy_mode == CopyMode::AttributesOnly || mkcp.copy_mode == CopyMode::HardLink => ret! {
				ExitCode::Failure, "mkcp: `--symbolic-link` cannot be specified with `--attributes-only` or `--link`"
			},
			"--symbolic-link" | "-s" => {
				mkcp.copy_mode = CopyMode::SymbolicLink;
			},
			"--update" | "-u" => {
				mkcp.update = true;
			},
			"--verbose" | "-v" => {
				mkcp.verbose = true;
			},
			"--version" | "-V" => {
				stdoutln! { "{}", version("mkcp", env! { "CARGO_PKG_VERSION" }) };
				return Ok(());
			},
			_ => ret! {
				ExitCode::Failure, "mkcp: fatal error: Unhandled option `{}`", option
			},
		}
	}

	if !suffix.is_empty() {
		mkcp.suffix = Some(&suffix);
	}

	copy_files(files.into_iter().map(PathBuf::from).collect(), mkcp)
}
