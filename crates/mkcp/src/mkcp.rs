// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::EEXIST;
use common::constants::ELOOP;
use common::constants::ENOENT;
use common::constants::EOPNOTSUPP;
use common::constants::O_NOFOLLOW;
use common::constants::REC_ERR_CODE;
use common::constants::S_IFBLK;
use common::constants::S_IFCHR;
use common::constants::S_IFDIR;
use common::constants::S_IFIFO;
use common::constants::S_IFLNK;
use common::constants::S_IFMT;
use common::constants::S_IFSOCK;
use common::constants::S_ISGID;
use common::constants::S_ISUID;
use common::constants::S_ISVTX;
use common::constants::SF_ERR_CODE;
use common::constants::USER_ERR_CODE;
use common::error::ExitCode;
use common::error;
use common::fs::fchmodat;
use common::fs::FchmodFlags;
use common::fs::is_samefile;
use common::fs::MakeDev;
use common::fs::mknodat;
use common::fs::UtimeFlags;
use common::fs::utimensat;
use common::ret;
use common::stderr;
use common::stderrln;
use common::stdoutln;

use crate::CopyMode;
use crate::Options;

use std::collections::HashSet;
use std::fs;
use std::io;
use std::os::unix::fs::lchown;
use std::os::unix::fs::MetadataExt;
use std::os::unix::fs::OpenOptionsExt;
use std::os::unix::fs::PermissionsExt;
use std::os::unix::fs::symlink;
use std::path::Path;
use std::path::PathBuf;

// Validate the input arguments of `copy_files()`.
fn input_validation(mut files: Vec<PathBuf>, mkcp: Options) -> common::Result<(PathBuf, Vec<PathBuf>)> {
	if mkcp.no_clobber && (mkcp.backup || mkcp.force || mkcp.interactive || mkcp.suffix.is_some()) {
		ret! { ExitCode::Failure, "mkcp: `--no-clobber` cannot be specified with `--backup`, `--force`, `--interactive` or `--suffix`" };
	}

	if mkcp.preserve.filetype && (mkcp.copy_mode == CopyMode::HardLink || mkcp.copy_mode == CopyMode::SymbolicLink) {
		ret! { ExitCode::Failure, "mkcp: `--preserve filetype` cannot be specified with `--link` or `--symbolic-link`" };
	}

	if files.is_empty() {
		ret! { ExitCode::Failure, "mkcp: missing file operand" };
	}

	if files.get(1).is_none() {
		if let Some(file) = files.first() {
			ret! { ExitCode::Failure, "mkcp: missing destination file operand after '{}'", file.display() };
		}
	}

	if mkcp.no_target {
		if let Some([dest, file]) = files.get(1..=2) {
			ret! { ExitCode::Failure, "mkcp: extra operand '{}' after destination file '{}'", file.display(), dest.display() };
		}
	}

	Ok((files.pop().ok_or_else(|| error! { ExitCode::Failure, "mkcp: fatal error: Failed to get destination file from command line" })?, files))
}

// Prevent same file / directory copy.
fn check_paths(source: &Path, destination: &Path) -> common::Result<()> {
	match is_samefile(source, destination) {
		Err(error) => ret! {
			ExitCode::Failure, "mkcp: not copying '{}' to '{}': {}", source.display(), destination.display(), error
		},
		Ok(result) => if result {
			ret! { SF_ERR_CODE, "mkcp: not copying '{}' to '{}': Same file or file path", source.display(), destination.display() };
		},
	}

	Ok(())
}

// Check if user specified options prevent `source` from being copied.
fn prepare_destination(source_metadata: fs::Metadata, source: &Path, destination: &Path, mkcp: Options) -> common::Result<()> {
	if destination.to_string_lossy().ends_with('/') && !source_metadata.file_type().is_dir() {
		ret! { ExitCode::Failure, "mkcp: cannot overwrite directory '{}' with non-directory '{}'", destination.display(), source.display() };
	}

	if let Ok(destination_metadata) = fs::symlink_metadata(destination) {
		if mkcp.no_clobber {
			ret! { USER_ERR_CODE, "mkcp: not replacing '{}'", destination.display() };
		}

		if mkcp.update && source_metadata.mtime() <= destination_metadata.mtime() {
			ret! { USER_ERR_CODE, "mkcp: not replacing '{}', it is newer than '{}'", destination.display(), source.display() };
		}

		if destination_metadata.file_type().is_dir() && !source_metadata.file_type().is_dir() {
			ret! { ExitCode::Failure, "mkcp: cannot overwrite directory '{}' with non-directory '{}'", destination.display(), source.display() };
		}

		if !destination_metadata.file_type().is_dir() && source_metadata.file_type().is_dir() {
			ret! { ExitCode::Failure, "mkcp: cannot overwrite non-directory '{}' with directory '{}'", destination.display(), source.display() };
		}

		if mkcp.interactive {
			let mut choice = String::with_capacity(64);
			stderr! { "mkcp: do you want to overwrite '{}'? ", destination.display() };

			if let Err(error) = io::stdin().read_line(&mut choice) {
				ret! { ExitCode::Failure, "mkcp: not replacing '{}', failed to read the standard input: {}", destination.display(), error };
			}

			let choice = choice.strip_suffix('\n').unwrap_or(&choice);

			if choice != "Y" && choice != "y" {
				ret! { USER_ERR_CODE, "mkcp: not replacing '{}', received '{}' from the standard input", destination.display(), choice };
			}
		}
	}

	Ok(())
}

// Remove existing `destination` file / directory if necessary.
fn clobber_destination(destination: &Path, mkcp: &mut Options) -> common::Result<()> {
	mkcp.renamed = false;

	if mkcp.backup || mkcp.suffix.is_some() {
		let destination_bak = format! { "{}{}", destination.display(), mkcp.suffix.unwrap_or("~") };

		match fs::rename(destination, &destination_bak) {
			Err(error) => if error.raw_os_error() != Some(ENOENT) {
				ret! { ExitCode::Failure, "mkcp: failed to rename '{}' to '{}': {}", destination.display(), destination_bak, error };
			},
			Ok(()) => {
				mkcp.renamed = true;
			},
		}
	}

	if mkcp.force || mkcp.interactive {
		if let Err(error) = fs::remove_file(destination) {
			if error.raw_os_error() != Some(ENOENT) {
				ret! { ExitCode::Failure, "mkcp: failed to overwrite target file '{}': {}", destination.display(), error };
			}
		}
	}

	Ok(())
}

// Preserve the user specified attributes in copied files / directories.
fn preserve_attributes(source_metadata: fs::Metadata, source: &Path, destination: &Path, mkcp: Options) -> common::Result<()> {
	// Avoid setting S_ISVTX, S_ISUID and S_ISGID bits if file owner is not preserved.
	let mut preserved_owner = false;

	if mkcp.preserve.timestamps {
		if let Err(error) = utimensat(destination, source_metadata.accessed().ok(), source_metadata.modified().ok(), UtimeFlags::NoFollow) {
			ret! { ExitCode::Failure, "mkcp: failed to preserve timestamps for '{}': {}", destination.display(), error };
		}
	}

	if mkcp.preserve.ownership {
		match lchown(destination, Some(source_metadata.uid()), Some(source_metadata.gid())) {
			Err(error) => ret! {
				ExitCode::Failure, "mkcp: failed to preserve ownership for '{}': {}", destination.display(), error
			},
			Ok(()) => {
				preserved_owner = true;
			},
		}
	}

	if mkcp.preserve.permissions && !source_metadata.file_type().is_symlink() {
		let permissions = if preserved_owner { source_metadata.permissions().mode() } else { source_metadata.permissions().mode() & !(S_ISGID | S_ISUID | S_ISVTX) };

		if let Err(error) = fchmodat(destination, fs::Permissions::from_mode(permissions), FchmodFlags::NoFollow) {
			if error.raw_os_error() != Some(EOPNOTSUPP) {
				ret! { ExitCode::Failure, "mkcp: failed to preserve permissions for '{}': {}", destination.display(), error };
			}
		}
	}

	if mkcp.verbose && mkcp.renamed {
		stdoutln! { "'{}' -> '{}' (backup: '{1}{}')", source.display(), destination.display(), mkcp.suffix.unwrap_or("~") };
	}
	else if mkcp.verbose {
		stdoutln! { "'{}' -> '{}'", source.display(), destination.display() };
	}

	Ok(())
}

// Return a formatted `common::Result` containing the filesystem node type.
macro_rules! styled_error {
	($source_metadata: ident, $source: expr, $destination: expr, $error: expr) => {{
		let filetype = match $source_metadata.permissions().mode() & S_IFMT {
			S_IFBLK  => "block device file ",
			S_IFCHR  => "character device file ",
			S_IFDIR  => "directory ",
			S_IFIFO  => "named pipe ",
			S_IFLNK  => "symbolic link ",
			S_IFSOCK => "unix domain socket ",
			_        => "",
		};

		ret! { ExitCode::Failure, "mkcp: failed to copy {}'{}' to '{}': {}", filetype, $source.display(), $destination.display(), $error };
	}};
}

// Try to "copy" `source` to `destination`.
fn try_file_copy(source_metadata: fs::Metadata, source: &Path, destination: &Path, mkcp: Options) -> common::Result<()> {
	if mkcp.copy_mode == CopyMode::HardLink || mkcp.copy_mode == CopyMode::SymbolicLink {
		let target = if mkcp.follow { fs::read_link(source).unwrap_or_else(|_| PathBuf::from(source)) } else { PathBuf::from(source) };

		if mkcp.copy_mode == CopyMode::HardLink {
			if let Err(error) = fs::hard_link(target, destination) {
				ret! { ExitCode::Failure, "mkcp: failed to create hard link '{}' to '{}': {}", destination.display(), source.display(), error };
			}
		}
		else if mkcp.copy_mode == CopyMode::SymbolicLink {
			if let Err(error) = symlink(target, destination) {
				ret! { ExitCode::Failure, "mkcp: failed to create symbolic link '{}' to '{}': {}", destination.display(), source.display(), error };
			}
		}
	}
	else if (mkcp.preserve.filetype || !mkcp.follow) && source_metadata.file_type().is_symlink() {
		if let Err(error) = symlink(fs::read_link(source).unwrap_or_else(|_| source.to_owned()), destination) {
			styled_error! { source_metadata, source, destination, error };
		}
	}
	else if mkcp.preserve.filetype && !source_metadata.file_type().is_file() {
		if let Err(error) = mknodat(destination, source_metadata.permissions(), MakeDev::from_metadata(&source_metadata)) {
			styled_error! { source_metadata, source, destination, error };
		}
	}
	else if mkcp.copy_mode == CopyMode::AttributesOnly {
		if let Err(error) = fs::File::options().write(true).create(true).truncate(false).open(destination) {
			ret! { ExitCode::Failure, "mkcp: failed to copy attributes from '{}' to '{}': {}", source.display(), destination.display(), error };
		}
	}
	else if mkcp.copy_mode == CopyMode::FileCopy {
		let mut source_fildes = fs::File::options().read(true).open(source).map_err(|error| error! {
			ExitCode::Failure, "mkcp: failed to open '{}' for reading: {}", source.display(), error
		})?;

		match fs::File::options().write(true).create(true).truncate(true).custom_flags(O_NOFOLLOW).open(destination) {
			Err(error) => styled_error! {
				source_metadata, source, destination, if error.raw_os_error() == Some(ELOOP) { io::Error::from_raw_os_error(EEXIST) } else { error }
			},
			Ok(mut destination_fildes) => if let Err(error) = io::copy(&mut source_fildes, &mut destination_fildes) {
				styled_error! { source_metadata, source, destination, error };
			},
		}
	}

	Ok(())
}

// Copy files from `source` to `destination`.
fn file_copy(source_metadata: fs::Metadata, source: &Path, destination: &Path, mkcp: &mut Options) -> common::Result<()> {
	// Check if destination can be worked on.
	prepare_destination(source_metadata.clone(), source, destination, *mkcp)?;

	// Create the leading component directories of `destination` if needed.
	let destination_dir = destination.parent().unwrap_or_else(|| Path::new(""));

	if !destination_dir.as_os_str().is_empty() {
		if let Err(error) = fs::DirBuilder::new().recursive(true).create(destination_dir) {
			ret! { ExitCode::Failure, "mkcp: failed to create target directory '{}': {}", destination_dir.display(), error };
		}
	}

	// Remove destination file if needed.
	clobber_destination(destination, mkcp)?;

	// Copy source file to destination.
	try_file_copy(source_metadata, source, destination, *mkcp)
}

// Copy directories recursively.
fn directory_copy(source: &Path, dest: &Path, mkcp: &mut Options) -> common::Result<()> {
	for dirent in fs::read_dir(source)? {
		let dirent = dirent?;

		let source_metadata = match if mkcp.follow && !mkcp.preserve.filetype { fs::metadata(dirent.path()) } else { dirent.metadata() } {
			Err(error) => {
				stderrln! { "mkcp: cannot stat '{}': {}", dirent.path().display(), error };
				mkcp.exitcode = Some(ExitCode::Failure);
				continue;
			},
			Ok(metadata) => {
				metadata
			},
		};

		let (source, destination) = (&dirent.path(), &dest.join(dirent.file_name()));

		if source_metadata.file_type().is_dir() {
			if let Err(error) = fs::DirBuilder::new().recursive(true).create(destination) {
				stderrln! { "mkcp: failed to create target directory '{}': {}", destination.display(), error };
				mkcp.exitcode = Some(REC_ERR_CODE);
				continue;
			}

			// Recurse till all files are copied.
			if let Err(error) = directory_copy(source, destination, mkcp) {
				stderrln! { "mkcp: failed to read directory '{}': {}", source.display(), error };
				mkcp.exitcode = Some(error.exitcode());
				continue;
			}
		}
		else {
			if let Err(error) = check_paths(source, destination) {
				stderrln! { "{}", error.message() };
				mkcp.exitcode = Some(error.exitcode());
				continue;
			}

			if let Err(error) = file_copy(source_metadata.clone(), source, destination, mkcp) {
				stderrln! { "{}", error.message() };
				mkcp.exitcode = Some(REC_ERR_CODE);
				continue;
			}
		}

		if let Err(error) = preserve_attributes(source_metadata, source, destination, *mkcp) {
			stderrln! { "{}", error.message() };
			mkcp.exitcode = Some(REC_ERR_CODE);
		}
	}

	Ok(())
}

#[doc(hidden)]
#[inline]
pub fn copy_files(files: Vec<PathBuf>, mut mkcp: Options) -> common::Result<()> {
	let (dest, files) = input_validation(files, mkcp)?;

	// Keep track of all copied files / directories.
	let mut copied_files: HashSet<PathBuf> = HashSet::with_capacity(files.len());

	for source in files {
		let source_metadata = match if mkcp.follow && !mkcp.preserve.filetype { fs::metadata(&source) } else { fs::symlink_metadata(&source) } {
			Err(error) => {
				stderrln! { "mkcp: cannot stat '{}': {}", source.display(), error };
				mkcp.exitcode = Some(ExitCode::Failure);
				continue;
			},
			Ok(metadata) => {
				metadata
			},
		};

		if !mkcp.recursive && source_metadata.file_type().is_dir() {
			stderrln! { "mkcp: `--recursive` not specified: Omitting directory '{}'", source.display() };
			mkcp.exitcode = Some(ExitCode::Failure);
			continue;
		}

		let (source, destination) = (&source, if mkcp.no_target { &dest } else { &dest.join(source.file_name().unwrap_or_default()) });

		// Prevent copying to `destination` multiple times.
		if copied_files.contains(destination) {
			stderrln! { "mkcp: not overwriting newly created '{}' with '{}'", destination.display(), source.display() };
			mkcp.exitcode = Some(ExitCode::Failure);
			continue;
		}

		// Check if `source` and `destination` refer to the same file.
		if let Err(error) = check_paths(source, destination) {
			stderrln! { "{}", error.message() };
			mkcp.exitcode = Some(error.exitcode());
			continue;
		}

		if source_metadata.file_type().is_dir() {
			if let Err(error) = fs::DirBuilder::new().recursive(true).create(destination) {
				ret! { ExitCode::Failure, "mkcp: failed to create target directory '{}': {}", destination.display(), error };
			}

			if let Err(error) = directory_copy(source, destination, &mut mkcp) {
				stderrln! { "mkcp: failed to recurse into directory '{}': {}", source.display(), error };
				mkcp.exitcode = Some(REC_ERR_CODE);
				continue;
			}
		}
		else if let Err(error) = file_copy(source_metadata.clone(), source, destination, &mut mkcp) {
			stderrln! { "{}", error.message() };
			mkcp.exitcode = Some(error.exitcode());
			continue;
		}

		// Track copied files / directories.
		copied_files.insert(PathBuf::from(destination));

		if let Err(error) = preserve_attributes(source_metadata, source, destination, mkcp) {
			stderrln! { "{}", error.message() };
			mkcp.exitcode = Some(error.exitcode());
		}
	}

	ret! { mkcp.exitcode.unwrap_or(ExitCode::Success), "" };
}
