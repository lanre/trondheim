// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::cli::version;
use common::constants::ENOENT;
use common::constants::OVERFLOW_ERR_CODE;
use common::constants::S_IFCHR;
use common::constants::S_IFIFO;
use common::constants::S_IFMT;
use common::constants::S_IFSOCK;
use common::constants::S_IRGRP;
use common::constants::S_IROTH;
use common::constants::S_IRUSR;
use common::constants::S_IWUSR;
use common::error::ExitCode;
use common::error;
use common::fs::CreateFile;
use common::fs::MakeDev;
use common::fs::mknodat;
use common::fs::readfile;
use common::fs::swapoff;
use common::sys::getuid;
use common::stdoutln;
use common::testbin;

use athena::HELP;
use athena::parse_size;

use std::fs;
use std::os::unix::fs::PermissionsExt;
use std::os::unix::fs::symlink;
use std::path::Path;
use std::thread;
use std::time::Duration;

// Athena test binary.
const BIN: &str = env! { "CARGO_BIN_EXE_athena" };

// Athena build dir.
const TESTDIR: &str = env! { "CARGO_MANIFEST_DIR" };

// Athena version.
const VERSION: &str = env! { "CARGO_PKG_VERSION" };

#[test]
fn test_help() {
	// athena --help
	let run = testbin! { BIN, "--help" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", HELP }
	};
}

#[test]
fn test_help_short_opt() {
	// athena --help
	let run = testbin! { BIN, "-h" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", HELP }
	};
}

#[test]
fn test_version() {
	// athena --version
	let run = testbin! { BIN, "--version" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", version("athena", VERSION) }
	};
}

#[test]
fn test_version_short_opt() {
	// athena --version
	let run = testbin! { BIN, "-V" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", version("athena", VERSION) }
	};
}

#[test]
fn test_help_args() {
	// athena --help=sdsd
	let run = testbin! { BIN, "--help=sdsd" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("athena: `--help` does not accept any arguments but 'sdsd' was supplied\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_help_positional() {
	// athena -- --help
	let run = testbin! { BIN, "-- --help" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("athena: missing file size operand\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_unrecognized_option() {
	// athena --a4erw
	let run = testbin! { BIN, "--a4erw" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("athena: unrecognized option `--a4erw`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_unrecognized_option_hypen() {
	// athena -
	let run = testbin! { BIN, "-" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("athena: unrecognized option `-`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_invalid_short_opts() {
	// athena -rkl
	let run = testbin! { BIN, "-rkl" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("athena: invalid option -- `r`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_no_command() {
	// athena
	let run = testbin! { BIN };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("athena: missing file size operand\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_no_command_positional() {
	// athena --
	let run = testbin! { BIN, "--" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("athena: missing file size operand\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_parse_size() {
	assert_eq! {
		parse_size("2040m", "--size").unwrap(),
		(2_139_095_040, String::from("2040 MiB"))
	};
	assert_eq! {
		parse_size("2040M", "--size").unwrap(),
		(2_139_095_040, String::from("2040 MiB"))
	};
	assert_eq! {
		parse_size("2040Mb", "--size").unwrap(),
		(2_139_095_040, String::from("2040 MiB"))
	};
	assert_eq! {
		parse_size("2040MB", "--size").unwrap(),
		(2_139_095_040, String::from("2040 MiB"))
	};
	assert_eq! {
		parse_size("2040mib", "--size").unwrap(),
		(2_139_095_040, String::from("2040 MiB"))
	};
	assert_eq! {
		parse_size("2040MIB", "--size").unwrap(),
		(2_139_095_040, String::from("2040 MiB"))
	};
	assert_eq! {
		parse_size("2040mIB", "--size").unwrap(),
		(2_139_095_040, String::from("2040 MiB"))
	};
	assert_eq! {
		parse_size("2040MiB", "--size").unwrap(),
		(2_139_095_040, String::from("2040 MiB"))
	};
	assert_eq! {
		parse_size("2040g", "--size").unwrap(),
		(2_190_433_320_960, String::from("2040 GiB"))
	};
	assert_eq! {
		parse_size("2g", "--size").unwrap(),
		(2_147_483_648, String::from("2 GiB"))
	};
}

#[test]
fn test_parse_size_error() {
	assert_eq! {
		parse_size("20000000000000000000g", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: failed to parse '20000000000000000000g' for `--size`: number too large to fit in target type" }
	};
	assert_eq! {
		parse_size("200000000000g", "--size").unwrap_err(),
		error! { OVERFLOW_ERR_CODE, "athena: fatal error: Integer overflow occured while parsing '200000000000g'" }
	};
	assert_eq! {
		parse_size("2342 m", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: invalid size argument '2342 m' specified for `--size`" }
	};
	assert_eq! {
		parse_size("2342k", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: invalid size argument '2342k' specified for `--size`" }
	};
	assert_eq! {
		parse_size("2342t", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: invalid size argument '2342t' specified for `--size`" }
	};
	assert_eq! {
		parse_size("2342mi", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: invalid size argument '2342mi' specified for `--size`" }
	};
	assert_eq! {
		parse_size("23i42m", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: invalid size argument '23i42m' specified for `--size`" }
	};
	assert_eq! {
		parse_size("2342m23", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: invalid size argument '2342m23' specified for `--size`" }
	};
	assert_eq! {
		parse_size("23kib42mib", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: invalid size argument '23kib42mib' specified for `--size`" }
	};
	assert_eq! {
		parse_size("2342_m", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: invalid size argument '2342_m' specified for `--size`" }
	};
	assert_eq! {
		parse_size("", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: invalid size argument '' specified for `--size`" }
	};
	assert_eq! {
		parse_size("km", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: invalid size argument 'km' specified for `--size`" }
	};
	assert_eq! {
		parse_size("m", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: failed to parse 'm' for `--size`: cannot parse integer from empty string" }
	};
	assert_eq! {
		parse_size("MiB", "--size").unwrap_err(),
		error! { ExitCode::Failure, "athena: failed to parse 'MiB' for `--size`: cannot parse integer from empty string" }
	};
}

#[test]
fn test_missing_permissions() {
	let file = Path::new(TESTDIR).join("test_missing_permissions");
	drop(fs::remove_file(&file));

	// athena --size 2302m {file}
	let run = testbin! { BIN, "--size 2302m {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: failed to create swapfile '{}': Operation not permitted (os error 1)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap();
}

#[test]
fn test_existing_error() {
	let file = Path::new(TESTDIR).join("test_existing_error");
	drop(CreateFile::new().create(&file).unwrap());

	// athena --size 2302m {file}
	let run = testbin! { BIN, "--size 2302m {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: cannot create swapfile '{}': File exists (os error 17)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap();
}

#[test]
fn test_small_size_error() {
	let file = Path::new(TESTDIR).join("test_small_size_error");
	drop(fs::remove_file(&file));

	// athena --size 10m {file}
	let run = testbin! { BIN, "--size 10m {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: the specified file size '10 MiB' is too small\n" }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
	assert_eq! {
		fs::symlink_metadata(file).unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
}

#[test]
fn test_overwrite() {
	let file  = Path::new(TESTDIR).join("test_overwrite");
	drop(CreateFile::new().create(&file).unwrap());

	// athena --size 2302m --force {file}
	let run = testbin! { BIN, "--size 2302m --force {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: failed to create swapfile '{}': Operation not permitted (os error 1)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap();
}

#[test]
fn test_discard_all() {
	let file = Path::new(TESTDIR).join("test_discard_all");
	drop(fs::remove_file(&file));

	// athena --size 2302m --discard -- {file}
	let run = testbin! { BIN, "--size 2302m --discard -- {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: failed to create swapfile '{}': Operation not permitted (os error 1)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap();
}

#[test]
fn test_discard_once() {
	let file = Path::new(TESTDIR).join("test_discard_once");
	drop(fs::remove_file(&file));

	// athena --size 2302m --discard once -- {file}
	let run = testbin! { BIN, "--size 2302m --discard once -- {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: failed to create swapfile '{}': Operation not permitted (os error 1)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap();
}

#[test]
fn test_discard_pages() {
	let file = Path::new(TESTDIR).join("test_discard_pages");
	drop(fs::remove_file(&file));

	// athena --size 2302m --discard pages -- {file}
	let run = testbin! { BIN, "--size 2302m --discard pages -- {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: failed to create swapfile '{}': Operation not permitted (os error 1)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap();
}

#[test]
fn test_priority_discard() {
	let file = Path::new(TESTDIR).join("test_priority_discard");
	drop(fs::remove_file(&file));

	// athena --size 2302m --discard --priority 2332 -- {file}
	let run = testbin! { BIN, "--size 2302m --discard --priority 2332 -- {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: failed to create swapfile '{}': Operation not permitted (os error 1)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap();
}

#[test]
fn test_priority_discard_short_opt() {
	let file = Path::new(TESTDIR).join("test_priority_discard_short_opt");
	drop(fs::remove_file(&file));

	// athena --size 2302m --discard --priority 2332 -- {file}
	let run = testbin! { BIN, "-s 2302m -d -p 2332 -- {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: failed to create swapfile '{}': Operation not permitted (os error 1)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap();
}

#[test]
fn test_extra_file_error() {
	let file = Path::new(TESTDIR).join("test_extra_file_error");
	drop(fs::remove_file(&file));

	// athena --size 1000m {file} original
	let run = testbin! { BIN, "--size 1000m {} original", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: extra operand 'original' after target file '{}'\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
	assert_eq! {
		fs::symlink_metadata(file).unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
	assert_eq! {
		fs::symlink_metadata("original").unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
}

#[test]
fn test_missing_file() {
	// athena --size 2g
	let run = testbin! { BIN, "--size 2g" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("athena: missing file operand\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_deep_recursive_create() {
	let stack_destroyer = Path::new("new_stack_test/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
	/new/new/new/new/new/new/new/new/new/new1");

	drop(fs::remove_file(stack_destroyer));

	// athena --size 2302m --discard --priority 2332 -- {stack_destroyer}
	let run = testbin! { BIN, "-s 2302m -d -p 2332 -- {}", stack_destroyer.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: failed to create swapfile '{}': Operation not permitted (os error 1)\n", stack_destroyer.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_dir_all("new_stack_test").unwrap();
}

#[test]
fn test_symlink_error() {
	let file = &Path::new(TESTDIR).join("test_symlink_error");
	drop(fs::remove_file(file)); symlink(file, file).unwrap();

	//////////////////////////////////////////////////////////
	// athena --size 2302m {file}
	//////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--size 2302m {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: cannot create swapfile '{}': File exists (os error 17)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap(); symlink("/bin/bash", file).unwrap();

	//////////////////////////////////////////////////////////
	// athena --size 2302m {file}
	//////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--size 2302m {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: cannot create swapfile '{}': File exists (os error 17)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap(); symlink("/does/not/exist", file).unwrap();

	//////////////////////////////////////////////////////////
	// athena --size 2302m {file}
	//////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--size 2302m {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: cannot create swapfile '{}': File exists (os error 17)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap();
}

#[test]
fn test_socket_file_error() {
	let file = &Path::new(TESTDIR).join("test_socket_file_error");
	drop(fs::remove_file(file));

	// Create a unix domain socket.
	mknodat(file, fs::Permissions::from_mode(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH | S_IFSOCK), MakeDev::new(12, 55)).unwrap();

	assert_eq! {
		file.symlink_metadata().unwrap().permissions().mode(),
		0o644 | S_IFSOCK
	};

	//////////////////////////////////////////////////////////
	// athena --size 2302m {file}
	//////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--size 2302m {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: cannot create swapfile '{}': File exists (os error 17)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	//////////////////////////////////////////////////////////
	// athena --size 2302m {file} --force
	//////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--size 2302m {} --force", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: cannot overwrite special file '{}': Operation not supported (os error 95)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap();
}

#[test]
fn test_fifo_file_error() {
	let file = &Path::new(TESTDIR).join("test_fifo_file_error");
	drop(fs::remove_file(file));

	// Create a named pipe
	mknodat(file, fs::Permissions::from_mode(S_IRUSR | S_IWUSR | S_IRGRP | S_IFIFO), MakeDev::new(12, 55)).unwrap();

	assert_eq! {
		file.symlink_metadata().unwrap().permissions().mode(),
		0o640 | S_IFIFO
	};

	//////////////////////////////////////////////////////////
	// athena --size 2302m {file}
	//////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--size 2302m {}", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: cannot create swapfile '{}': File exists (os error 17)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	//////////////////////////////////////////////////////////
	// athena --size 2302m {file} --force
	//////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--size 2302m {} --force", file.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: cannot overwrite special file '{}': Operation not supported (os error 95)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(file).unwrap();
}

#[test]
fn test_char_file_error() {
	assert_eq! {
		fs::symlink_metadata("/dev/null").unwrap().permissions().mode() & S_IFMT,
		S_IFCHR
	};

	//////////////////////////////////////////////////////////
	// athena --size 2302m /dev/null
	//////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--size 2302m /dev/null" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("athena: cannot create swapfile '/dev/null': File exists (os error 17)\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	//////////////////////////////////////////////////////////
	// athena --size 2302m --force /dev/null
	//////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--size 2302m --force /dev/null" };

	assert_eq! {
		fs::symlink_metadata("/dev/null").unwrap().permissions().mode() & S_IFMT,
		S_IFCHR
	};

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("athena: cannot overwrite special file '/dev/null': Operation not supported (os error 95)\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_swapfile_create() {
	// Only run as root user.
	if getuid() != 0 {
		return;
	}

	let file = &Path::new(TESTDIR).join("test_swapfile_create");
	drop((swapoff(file), fs::remove_file(file)));

	//////////////////////////////////////////////////////////
	// athena --size 2302mib --sparse-file --discard --priority 20000 {file} --verbose
	//////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--size 2302mib --sparse-file --discard --priority 20000 {} --verbose", file.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::from("athena: setting up swapspace version 1, no label, no uuid, size = 2302 MiB (2413821952 bytes)\n")
	};

	assert! { readfile("/proc/swaps").unwrap().to_lossy_string().contains(file.to_str().unwrap()) };
	swapoff(file).unwrap();

	//////////////////////////////////////////////////////////
	// athena --size 1024mib --discard --priority 20000 {file} --verbose
	//////////////////////////////////////////////////////////
	let run = testbin! {BIN, "--size 1024mib --discard --priority 200 {} --verbose", file.display()};

	// NOTE: Even when running as root user, existing files will not be overwritten unless `--force` is specified.
	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		format! { "athena: cannot create swapfile '{}': File exists (os error 17)\n", file.display() }
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	stdoutln! { "ran test_swapfile_create() as root" };
	fs::remove_file(file).unwrap();
}

#[test]
fn test_swapfile_thread_safety() {
	// Only run as root user.
	if getuid() != 0 {
		return;
	}

	let file = &Path::new(TESTDIR).join("test_swapfile_thread_safety");
	drop((swapoff(file), fs::remove_file(file)));

	thread::scope(|test| {
		test.spawn(|| {
			thread::sleep(Duration::from_secs(2));

			//////////////////////////////////////////////////////////
			// athena --size 1024mib --discard --priority 20000 {file} --verbose --force
			//////////////////////////////////////////////////////////
			let run = testbin! { BIN, "--size 1024mib --discard --priority 200 {} --verbose --force", file.display() };

			// NOTE: The file already has a lock on it, trying to gain another one a new thread
			// will fail as intended.
			assert_eq! {
				run.status.code(),
				Some(1)
			};
			assert_eq! {
				String::from_utf8(run.stderr).unwrap(),
				format! { "athena: failed to create swapfile '{}': Resource temporarily unavailable (os error 11)\n", file.display() }
			};
			assert_eq! {
				String::from_utf8(run.stdout).unwrap(),
				String::new()
			};
		});
		test.spawn(|| {
			//////////////////////////////////////////////////////////
			// athena --size 1024mib --discard --priority 20000 {file} --verbose --force
			//////////////////////////////////////////////////////////
			let run = testbin! { BIN, "--size 1024mib --discard --priority 200 {} --verbose --force", file.display() };

			assert_eq! {
				run.status.code(),
				Some(0)
			};
			assert_eq! {
				String::from_utf8(run.stderr).unwrap(),
				String::new()
			};
			assert_eq! {
				String::from_utf8(run.stdout).unwrap(),
				String::from("athena: setting up swapspace version 1, no label, no uuid, size = 1024 MiB (1073741824 bytes)\n")
			};
			assert_eq! {
				fs::symlink_metadata(file).unwrap().len(),
				1_073_741_824
			};

			assert! { readfile("/proc/swaps").unwrap().to_lossy_string().contains(file.to_str().unwrap()) };
			swapoff(file).unwrap();
		});
	});

	stdoutln! { "ran test_swapfile_thread_safety() as root" };
	fs::remove_file(file).unwrap();
}
