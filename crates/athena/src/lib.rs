// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod athena;

use common::cli::getopt;
use common::cli::HasArg;
use common::cli::version;
use common::constants::OVERFLOW_ERR_CODE;
use common::error::ExitCode;
use common::error;
use common::fs::Discard;
use common::fs::SwapFlags;
use common::ret;
use common::stdoutln;

use athena::swapfile;

use std::ffi::OsString;
use std::path::PathBuf;

#[doc(hidden)]
pub const HELP: &str =
"athena - Create and setup a FILE for paging and swapping.

Usage:
    athena OPTION... FILE

Options:
    -d, --discard [<POLICY>]
          Discard swap pages using the specified POLICY.
          'once'  - Discard once, across the entire swap region upon activation.
          'pages' - Discard any freed swap pages before reuse.

    -f, --force
          Overwrite FILE if it already exists.

    -h, --help
          Display this help message and exit.

    -p, --priority <PRIORITY>
          Specify the swap FILE PRIORITY. PRIORITY must be a value between 0 and
          32767.

    -S, --sparse-file
          Create a new FILE by preallocating blocks.

    -s, --size <SIZE>
          Specify the SIZE of the swap FILE. A size suffix, either of MiB or GiB
          must be included with the specified SIZE. The 'iB' is optional.

    -V, --version
          Display version information and exit.

    -v, --verbose
          Show what is being done. Errors will always be printed to the standard
          error stream, even when this option is not specified.

You can report bugs by opening a new issue at <https://codeberg.org/lanre/trondheim/>
or by sending an email to <koladeolanrewaju@tutanota.com>.";

/// Struct used to process supported options in `athena`.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[doc(hidden)]
pub struct Options<'athena> {
	/// `-d` | `--discard`.
	discard: bool,

	/// `-p` | `--priority`.
	priority: bool,

	/// `-f` | `--force`.
	pub force: bool,

	/// `-s` | `--size`.
	pub size: (i64, &'athena str),

	/// `-S` | `--sparse-file`.
	pub sparsefile: bool,

	/// flags for `swapon()`.
	pub swapflags: SwapFlags,

	/// `-v` | `--verbose`.
	pub verbose: bool,
}

#[doc(hidden)]
#[inline]
pub fn parse_size<A: AsRef<str>, B: AsRef<str>>(argument: A, option: B) -> common::Result<(i64, String)> {
	let (argument, lowercase, option) = (argument.as_ref(), argument.as_ref().to_lowercase(), option.as_ref());

	macro_rules! parse {
		($letter: expr, $exp: expr, $suffix: expr) => {{
			let (size, _) = match lowercase.split_once($letter) {
				Some((size, suffix)) if (suffix.is_empty() || suffix == "ib" || suffix == "b") => {
					(size, suffix)
				},
				_ => ret! {
					ExitCode::Failure, "athena: invalid size argument '{}' specified for `{}`", argument, option
				},
			};

			match size.parse::<i64>() {
				Err(error) => ret! {
					ExitCode::Failure, "athena: failed to parse '{}' for `{}`: {}", argument, option, error
				},
				Ok(size) => {
					let bytes = size.checked_mul($exp).ok_or_else(|| error! {
						OVERFLOW_ERR_CODE, "athena: fatal error: Integer overflow occured while parsing '{}'", argument
					})?;

					return Ok((bytes, format! { "{} {}", size, $suffix }));
				},
			}
		}};
	}

	for letter in lowercase.chars() {
		match letter {
			'g' => parse! {
				letter, 1_073_741_824, "GiB"
			},
			'm' => parse! {
				letter, 1_048_576, "MiB"
			},
			_ if letter.is_ascii_digit() => {
			},
			_ => ret! {
				ExitCode::Failure, "athena: invalid size argument '{}' specified for `{}`", argument, option
			},
		}
	}

	ret! { ExitCode::Failure, "athena: invalid size argument '{}' specified for `{}`", argument, option }
}

#[doc(hidden)]
#[inline]
pub fn try_main(user_input: Vec<OsString>) -> common::Result<()> {
	const OPTIONS: &[(&str, &str, HasArg)] = &[
		( "--discard",     "-d", HasArg::Optional ),
		( "--force",       "-f", HasArg::False    ),
		( "--help",        "-h", HasArg::False    ),
		( "--priority",    "-p", HasArg::True     ),
		( "--size",        "-s", HasArg::True     ),
		( "--sparse-file", "-S", HasArg::False    ),
		( "--verbose",     "-v", HasArg::False    ),
		( "--version",     "-V", HasArg::False    ),
	];

	let ((options, files), mut athena, mut size) = (getopt(user_input, OPTIONS, "athena")?, Options::default(), None);

	for (option, argument) in options {
		match option.as_str() {
			"--discard" | "-d" if athena.discard => ret! {
				ExitCode::Failure, "athena: `{}` cannot be specified multiple times", option
			},
			"--discard" | "-d" => {
				let policy = match argument.as_str() {
					"once" => {
						Discard::Once
					},
					"pages" => {
						Discard::Pages
					},
					_ if argument.is_empty() => {
						Discard::Auto
					},
					_ => ret! {
						ExitCode::Failure, "athena: invalid argument '{}' found for `{}`", argument, option
					},
				};

				(athena.discard, athena.swapflags) = (true, athena.swapflags.discard(policy));
			},
			"--force" | "-f" => {
				athena.force = true;
			},
			"--help" | "-h" => {
				stdoutln! { "{}", HELP };
				return Ok(());
			},
			"--priority" | "-p" if athena.priority => ret! {
				ExitCode::Failure, "athena: `{}` cannot be specified multiple times", option
			},
			"--priority" | "-p" => {
				let priority = argument.parse::<i32>().map_err(|error| error! {
					ExitCode::Failure, "athena: failed to parse '{}' for `{}`: {}", argument, option, error
				})?;

				(athena.priority, athena.swapflags) = (true, athena.swapflags.priority(priority).map_err(|error| error! {
					ExitCode::Failure, "athena: failed to parse '{}' for `{}`: {}", argument, option, error
				})?);
			},
			"--size" | "-s" if size.is_some() => ret! {
				ExitCode::Failure, "athena: `{}` cannot be specified multiple times", option
			},
			"--size" | "-s" => {
				size = Some(parse_size(argument, option)?);
			},
			"--sparse-file" | "-S" => {
				athena.sparsefile = true;
			},
			"--verbose" | "-v" => {
				athena.verbose = true;
			},
			"--version" | "-V" => {
				stdoutln! { "{}", version("athena", env! { "CARGO_PKG_VERSION" }) };
				return Ok(());
			},
			_ => ret! {
				ExitCode::Failure, "athena: fatal error: Unhandled option `{}`", option
			},
		}
	}

	let (size, raw) = size.ok_or_else(|| error! {
		ExitCode::Failure, "athena: missing file size operand"
	})?;

	athena.size = (size, &raw);

	swapfile(files.into_iter().map(PathBuf::from).collect(), athena)
}
