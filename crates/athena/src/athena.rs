// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::EEXIST;
use common::constants::ENOENT;
use common::constants::EOPNOTSUPP;
use common::constants::EOVERFLOW;
use common::constants::OVERFLOW_ERR_CODE;
use common::constants::S_IRUSR;
use common::constants::S_IWUSR;
use common::error::ExitCode;
use common::error;
use common::fs::CreateFile;
use common::fs::Fallocate;
use common::fs::FallocFlags;
use common::fs::Flock;
use common::fs::LockOperation;
use common::fs::swapon;
use common::ret;
use common::stdoutln;
use common::sys::getpagesize;

use crate::Options;

use std::fs;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;
use std::io;
use std::os::unix::fs::PermissionsExt;
use std::path::Path;
use std::path::PathBuf;

// Validate input arguments of `swap_file()`.
fn input_validation(files: Vec<PathBuf>, athena: Options) -> common::Result<(i64, i64, PathBuf)> {
	let file = match files.first_chunk::<2>() {
		None => files.first().ok_or_else(|| error! {
			ExitCode::Failure, "athena: missing file operand"
		})?,
		Some([dest, file]) => ret! {
			ExitCode::Failure, "athena: extra operand '{}' after target file '{}'", file.display(), dest.display()
		},
	};

	let ((size, sizestring), pagesize) = (athena.size, i64::from(getpagesize()));

	match size.checked_div(pagesize) {
		None => ret! {
			OVERFLOW_ERR_CODE, "athena: fatal error: Integer overflow occured while parsing '{}'", sizestring
		},
		Some(pages) if size < 104_857_600 || pages < 10 => ret! {
			ExitCode::Failure, "athena: the specified file size '{}' is too small", sizestring
		},
		Some(pages) => {
			Ok((pages, pagesize, PathBuf::from(file)))
		},
	}
}

// Prevent overwrites even when running as root, unless explicitly requested with `--force`.
fn clobber_existing<P: AsRef<Path>>(file: P, athena: Options) -> common::Result<()> {
	let file = file.as_ref();

	match fs::symlink_metadata(file) {
		Err(error) => if error.raw_os_error() != Some(ENOENT) {
			ret! { ExitCode::Failure, "athena: cannot create swapfile '{}': {}", file.display(), error };
		},
		Ok(_) if !athena.force => ret! {
			ExitCode::Failure, "athena: cannot create swapfile '{}': {}", file.display(), io::Error::from_raw_os_error(EEXIST)
		},
		Ok(metadata) if metadata.file_type().is_dir() => if let Err(error) = fs::remove_dir(file) {
			ret! { ExitCode::Failure, "athena: failed to overwrite '{}': {}", file.display(), error };
		},
		Ok(metadata) if metadata.file_type().is_symlink() => if let Err(error) = fs::remove_file(file) {
			ret! { ExitCode::Failure, "athena: failed to overwrite '{}': {}", file.display(), error };
		},
		Ok(metadata) => if !metadata.file_type().is_file() {
			ret! { ExitCode::Failure, "athena: cannot overwrite special file '{}': {}", file.display(), io::Error::from_raw_os_error(EOPNOTSUPP) };
		},
	}

	Ok(())
}

// Create a file with the user specified size.
fn set_filesize(fildes: &mut fs::File, athena: Options) -> io::Result<()> {
	let (size, sizestring) = athena.size;

	if athena.verbose {
		stdoutln! { "athena: setting up swapspace version 1, no label, no uuid, size = {} ({} bytes)", sizestring, size };
	}

	if athena.sparsefile {
		fildes.fallocate(FallocFlags::DefaultMode, 0, size)?;
	}
	else {
		let mut block = 0;

		while block < size {
			fildes.write_all(&[0; 1_048_576])?;
			block = block.checked_add(1_048_576).ok_or_else(|| io::Error::from_raw_os_error(EOVERFLOW))?;
		}
	}

	// Truncate file to the user specified file size.
	fildes.set_len(u64::try_from(size).map_err(|_| io::Error::from_raw_os_error(EOVERFLOW))?)?;

	Ok(())
}

// Write swap header to the created swapfile.
fn write_header(fildes: &mut fs::File, pages: i64, pagesize: i64) -> io::Result<()> {
	// Move cursor past disk label region.
	fildes.seek(SeekFrom::Start(1024))?;

	// Write swap version.
	fildes.write_all(&1_i32.to_ne_bytes())?;

	// Write last page.
	fildes.write_all(&pages.checked_sub(1).ok_or_else(|| io::Error::from_raw_os_error(EOVERFLOW))?.to_ne_bytes())?;

	// Write number of bad pages.
	fildes.write_all(&0_i32.to_ne_bytes())?;

	// Write swap UUID.
	fildes.write_all(&[0; 16])?;

	// Write swap label.
	fildes.write_all(b"")?;

	let pagesize = match u64::try_from(pagesize) {
		Err(_) => {
			return Err(io::Error::from_raw_os_error(EOVERFLOW));
		},
		Ok(page) => page.checked_sub(10).ok_or_else(|| {
			io::Error::from_raw_os_error(EOVERFLOW)
		})?,
	};

	// Move cursor past reserved region.
	fildes.seek(SeekFrom::Start(pagesize))?;

	// Write swap signature.
	fildes.write_all(b"SWAPSPACE2")?;

	// Return cursor to file start.
	fildes.seek(SeekFrom::Start(0))?;

	Ok(())
}

#[doc(hidden)]
#[inline]
pub fn swapfile(files: Vec<PathBuf>, athena: Options) -> common::Result<()> {
	let (pages, pagesize, swapfile) = input_validation(files, athena)?;

	// Remove existing files if user options allow it.
	clobber_existing(&swapfile, athena)?;

	let ((_, sizestring), destination) = (athena.size, swapfile.parent().unwrap_or_else(|| Path::new("")));

	// Create destination directory if needed.
	if !destination.as_os_str().is_empty() {
		if let Err(error) = fs::DirBuilder::new().recursive(true).create(destination) {
			ret! { ExitCode::Failure, "athena: failed to create target directory '{}': {}", destination.display(), error };
		}
	}

	let mut fildes = CreateFile::new().flock(LockOperation::ExclusiveNonBlocking).fchown(Some(0), Some(0)).fchmod(fs::Permissions::from_mode(S_IRUSR | S_IWUSR)).create(&swapfile).map_err(|error| error! {
		ExitCode::Failure, "athena: failed to create swapfile '{}': {}", swapfile.display(), error
	})?;

	// Set the file size to user specified value.
	if let Err(error) = set_filesize(&mut fildes, athena) {
		ret! { ExitCode::Failure, "athena: failed to set '{}' file size to '{}': {}", swapfile.display(), sizestring, error };
	}

	if let Err(error) = write_header(&mut fildes, pages, pagesize) {
		ret! { ExitCode::Failure, "athena: failed to write swap header to file '{}': {}", swapfile.display(), error };
	}

	if let Err(error) = swapon(&swapfile, athena.swapflags) {
		ret! { ExitCode::Failure, "athena: failed to enable swapfile '{}': {}", swapfile.display(), error };
	}

	// Try to remove file lock. The lock is dropped automatically when `fildes` goes
	// out of scope, so this is just here to confirm that the swapfile was created successfully.
	if let Err(error) = fildes.flock(LockOperation::UnlockNonBlocking) {
		ret! { ExitCode::Failure, "athena: failed to remove lock on '{}' after setting up swapspace: {}", swapfile.display(), error };
	}

	Ok(())
}
