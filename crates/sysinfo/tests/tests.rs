// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::cli::version;
use common::testbin;

use sysinfo::HELP;

// Sysinfo test binary.
const BIN: &str = env! { "CARGO_BIN_EXE_sysinfo" };

// Sysinfo version.
const VERSION: &str = env! { "CARGO_PKG_VERSION" };

#[test]
fn test_help() {
	// sysinfo --help
	let run = testbin! { BIN, "--help" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", HELP }
	};
}

#[test]
fn test_help_short_opt() {
	// sysinfo --help
	let run = testbin! { BIN, "-h" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", HELP }
	};
}

#[test]
fn test_version() {
	// sysinfo --version
	let run = testbin! { BIN, "--version" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", version("sysinfo", VERSION) }
	};
}

#[test]
fn test_version_short_opt() {
	// sysinfo --version
	let run = testbin! { BIN, "-V" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", version("sysinfo", VERSION) }
	};
}

#[test]
fn test_help_args() {
	// sysinfo --help=sdsd
	let run = testbin! { BIN, "--help=sdsd" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("sysinfo: `--help` does not accept any arguments but 'sdsd' was supplied\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_help_positional() {
	// sysinfo -- --help
	let run = testbin! { BIN, "-- --help" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("sysinfo: invalid argument `--help`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_unrecognized_option() {
	// sysinfo --a4erw
	let run = testbin! { BIN, "--a4erw" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("sysinfo: unrecognized option `--a4erw`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_unrecognized_option_hypen() {
	// sysinfo -
	let run = testbin! { BIN, "-" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("sysinfo: unrecognized option `-`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_invalid_short_opts() {
	// sysinfo -rkl
	let run = testbin! { BIN, "-rkl" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("sysinfo: invalid option -- `r`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_invalid_args_positional() {
	// sysinfo -- --esd
	let run = testbin! { BIN, "-- --esd" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("sysinfo: invalid argument `--esd`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_invalid_short_opts_positional() {
	// sysinfo -- -esd
	let run = testbin! { BIN, "-- -esd" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("sysinfo: invalid argument `-esd`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_no_command() {
	// sysinfo
	let run = testbin! { BIN };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_ne! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_no_command_positional() {
	// sysinfo --
	let run = testbin! { BIN, "--" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_ne! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_no_command_positional_ext() {
	/////////////////////////////////////////////////////////
	// sysinfo -- --
	/////////////////////////////////////////////////////////
	let run = testbin! { BIN, "-- --" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("sysinfo: invalid argument `--`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	/////////////////////////////////////////////////////////
	// sysinfo -- -
	/////////////////////////////////////////////////////////
	let run = testbin! { BIN, "-- -" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("sysinfo: invalid argument `-`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_suffix_error() {
	// sysinfo --suffix Megi
	let run = testbin! { BIN, "--suffix Megi" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("sysinfo: invalid size suffix 'Megi' found for `--suffix`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_suffix_error_ext() {
	// sysinfo --suffix Mega --suffix Tera
	let run = testbin! { BIN, "--suffix Mega --suffix Tera" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("sysinfo: `--suffix` cannot be specified multiple times\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_suffix() {
	let run = testbin! { BIN, "--suffix Mega" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_ne! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_suffix_short_opt() {
	// sysinfo --suffix giga
	let run = testbin! { BIN, "-S giga" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_ne! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_suffix_short_opt_error() {
	// sysinfo --suffix tera
	let run = testbin! { BIN, "-S tera" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("sysinfo: invalid size suffix 'tera' found for `-S`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}
