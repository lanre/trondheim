// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod sysinfo;

use common::cli::getopt;
use common::cli::HasArg;
use common::cli::version;
use common::error::ExitCode;
use common::ret;
use common::stdoutln;

use std::ffi::OsString;

use sysinfo::generate_output;

#[doc(hidden)]
pub const HELP: &str =
"sysinfo - Display system hardware information.

Usage:
    sysinfo [OPTION]

Options:
    -h, --help
          Display this help message and exit.

    -S, --suffix <SUFFIX>
          Display values with the specified size SUFFIX. Valid SUFFIX values are
          mega, m, giga, g.

    -V, --version
          Display version information and exit.

You can report bugs by opening a new issue at <https://codeberg.org/lanre/trondheim/>
or by sending an email to <koladeolanrewaju@tutanota.com>.";

/// Struct used to process supported options in `sysinfo`.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[doc(hidden)]
pub struct Options {
	/// `-`S` | `--suffix`.
	pub suffix: Option<SizeSuffix>,
}

/// Custom type for representing supported size suffixes.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[doc(hidden)]
pub enum SizeSuffix {
	/// giga | gibi | g.
	Giga,

	/// mega | mebi | m.
	Mega,
}

#[doc(hidden)]
#[inline]
pub fn try_main(user_input: Vec<OsString>) -> common::Result<()> {
	const OPTIONS: &[(&str, &str, HasArg)] = &[
		( "--help",    "-h", HasArg::False ),
		( "--suffix",  "-S", HasArg::True  ),
		( "--version", "-V", HasArg::False ),
	];

	let ((options, arguments), mut sysinfo) = (getopt(user_input, OPTIONS, "sysinfo")?, Options::default());

	for (option, argument) in options {
		match option.as_str() {
			"--help" | "-h" => {
				stdoutln! { "{}", HELP };
				return Ok(());
			},
			"--suffix" | "-S" if sysinfo.suffix.is_some() => ret! {
				ExitCode::Failure, "sysinfo: `{}` cannot be specified multiple times", option
			},
			"--suffix" | "-S" => sysinfo.suffix = Some(match argument.to_lowercase().as_str() {
				"g" | "giga" | "gibi" => {
					SizeSuffix::Giga
				},
				"m" | "mega" | "mebi" => {
					SizeSuffix::Mega
				},
				_ => ret! {
					ExitCode::Failure, "sysinfo: invalid size suffix '{}' found for `{}`", argument, option
				},
			}),
			"--version" | "-V" => {
				stdoutln! { "{}", version("sysinfo", env! { "CARGO_PKG_VERSION" }) };
				return Ok(());
			},
			_ => ret! {
				ExitCode::Failure, "sysinfo: fatal error: Unhandled option `{}`", option
			},
		}
	}

	if let Some(argument) = arguments.first() {
		ret! { ExitCode::Failure, "sysinfo: invalid argument `{}`", argument };
	}

	generate_output(sysinfo)
}
