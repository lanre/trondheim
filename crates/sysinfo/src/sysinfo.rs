// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::OVERFLOW_ERR_CODE;
use common::error::ExitCode;
use common::error;
use common::fs::readfile;
use common::ret;
use common::stdoutln;
use common::sys::getuid;
use common::sys::sysinfo;
use common::sys::SysInfo;
use common::sys::uname;
use common::sys::userinfo;
use common::sys::UserInfo;
use common::sys::UtsName;

use crate::Options;
use crate::SizeSuffix;

use std::env;
use std::fmt::Write;
use std::fs;
use std::mem;
use std::path::Path;

// Get device name and operating system name.
#[cfg(target_os = "android")]
fn get_device_info() -> Option<(String, String)> {
	let (mut version, mut device, mut vendor, file) = ("", "", "", readfile("/vendor/odm_dlkm/etc/build.prop")
		.map_or_else(|_| readfile("/vendor/odm/etc/build.prop").ok(), Some)?
		.to_lossy_string());

	file.lines().for_each(|line| if line.starts_with("ro.product.odm_dlkm.manufacturer=") {
		mem::swap(&mut line.trim_start_matches("ro.product.odm_dlkm.manufacturer=").trim(), &mut vendor);
	}
	else if line.starts_with("ro.product.odm.manufacturer=") {
		mem::swap(&mut line.trim_start_matches("ro.product.odm.manufacturer=").trim(), &mut vendor);
	}
	else if line.starts_with("ro.product.odm_dlkm.model=") {
		mem::swap(&mut line.trim_start_matches("ro.product.odm_dlkm.model=").trim(), &mut device);
	}
	else if line.starts_with("ro.product.odm.model=") {
		mem::swap(&mut line.trim_start_matches("ro.product.odm.model=").trim(), &mut device);
	}
	else if line.starts_with("ro.odm_dlkm.build.version.release=") {
		mem::swap(&mut line.trim_start_matches("ro.odm_dlkm.build.version.release=").trim(), &mut version);
	}
	else if line.starts_with("ro.odm.build.version.release=") {
		mem::swap(&mut line.trim_start_matches("ro.odm.build.version.release=").trim(), &mut version);
	});

	(!vendor.is_empty() && !device.is_empty() && !version.is_empty()).then(|| {
		(format! { "{} {}", vendor, device }, format! { "Android {} {}", version, env::consts::ARCH })
	})
}

// Get device name and operating system name.
#[cfg(not(target_os = "android"))]
fn get_device_info() -> Option<(String, String)> {
	let (mut version, file, vendor) = ("", readfile("/etc/os-release").ok()?.to_lossy_string(), readfile("/sys/devices/virtual/dmi/id/product_name")
		.map_or_else(|_| readfile("/sys/firmware/devicetree/base/model").ok(), Some)?
		.to_lossy_string());

	for line in file.lines() {
		if line.starts_with("PRETTY_NAME=") {
			mem::swap(&mut line.trim_start_matches("PRETTY_NAME=\"").trim_end_matches('"').trim(), &mut version);
			break;
		}
	}

	(!vendor.is_empty() && !version.is_empty()).then(|| (vendor, format! { "{} {}", version, env::consts::ARCH }))
}

// Get name of the init process.
fn get_init() -> Option<String> {
	let (mut init, file) = ("", readfile("/proc/1/status").ok()?.to_lossy_string());

	for line in file.lines() {
		if line.starts_with("Name:") {
			mem::swap(&mut line.trim_start_matches("Name:").trim(), &mut init);
			break;
		}
	}

	(!init.is_empty()).then(|| String::from(init))
}

// Get number of installed applications.
#[cfg(target_os = "android")]
fn get_package_info() -> Option<String> {
	Some(format! { "{} apks", fs::read_dir("/data/data").ok()?.count() })
}

// Get installed package count.
#[cfg(not(target_os = "android"))]
fn get_package_info() -> Option<String> {
	macro_rules! count_packages {
		($file: expr, $parser: expr, $package_manager: expr) => (Some(format! {
			"{} {}", $file.to_lossy_string().lines().filter(|line| line.contains($parser)).count(), $package_manager
		}));
	}

	if let Ok(void_linux) = readfile("/var/db/xbps/pkgdb-0.38.plist") {
		return count_packages! { void_linux, "<string>installed", "xbps" };
	}

	if let Ok(debian_linux) = readfile("/var/lib/dpkg/status") {
		return count_packages! { debian_linux, "install ok installed", "dpkg" };
	}

	if let Ok(alpine_linux) = readfile("/etc/apk/world") {
		return count_packages! { alpine_linux, "\n", "(world)" };
	}

	if let Ok(arch_linux) = fs::read_dir("/var/lib/pacman/local") {
		return Some(format! { "{} pacman", arch_linux.count() });
	}

	// All other package managers.
	None
}

// Get system locale.
fn get_locale() -> Option<String> {
	env::var_os("LANG").map(|locale| (!locale.is_empty()).then(|| String::from(locale.to_string_lossy())))?
}

// Get desktop environment and display server.
fn get_desktop() -> Option<String> {
	let desktop = env::var_os("XDG_CURRENT_DESKTOP").map_or_else(|| None, |res| (!res.is_empty()).then(|| String::from(res.to_string_lossy())))?;

	if let Some(server) = env::var_os("XDG_SESSION_TYPE") {
		if !server.is_empty() {
			return Some(format! { "{} ({})", desktop, server.to_string_lossy() });
		}
	}

	if env::var_os("DISPLAY").is_some() {
		return Some(format! { "{} (x11)", desktop });
	}

	if env::var_os("WAYLAND_DISPLAY").is_some() {
		return Some(format! { "{} (wayland)", desktop });
	}

	Some(desktop)
}

// Get CPU name, core count and current clock speed.
fn get_cpu_info(suffix: SizeSuffix) -> Option<String> {
	let (mut cpu_cores, mut cpu_threads, mut cpu_name, cpuinfo) = (None, 0, "", readfile("/proc/cpuinfo").ok()?.to_lossy_string());

	for line in cpuinfo.lines().rev() {
		if line.starts_with("processor\t:") {
			mem::swap(&mut line.trim_start_matches("processor\t:").trim().parse::<u64>().ok()?.checked_add(1)?, &mut cpu_threads);
			break;
		}
	}

	for line in cpuinfo.lines() {
		if line.starts_with("cpu cores\t:") {
			mem::swap(&mut line.trim_start_matches("cpu cores\t:").trim().parse::<u64>().ok(), &mut cpu_cores);
			break;
		}
	}

	'outerloop: for line in cpuinfo.lines() {
		for tag in ["Hardware\t:", "model name\t:", "cpu model\t:", "chip type\t:", "cpu type\t:", "isa\t:"] {
			if line.starts_with(tag) {
				mem::swap(&mut line.trim_start_matches(tag).split('@').next()?.trim(), &mut cpu_name);
				break 'outerloop;
			}
		}
	}

	let cpuinfo = (!cpu_name.is_empty()).then(|| format! { "{} ({}/{})", cpu_name, cpu_cores.unwrap_or(cpu_threads), cpu_threads })?;

	if let Ok(clock) = readfile("/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq") {
		return Some(match suffix {
			SizeSuffix::Giga => format! {
				"{} @ {:.2} GHz", cpuinfo, clock.parse::<f64>().map(|res| res / 1_000_000.0).ok()?
			},
			SizeSuffix::Mega => format! {
				"{} @ {:.2} MHz", cpuinfo, clock.parse::<f64>().map(|res| res / 1_000.0).ok()?
			},
		});
	}

	Some(cpuinfo)
}

// Get GPU name and clock speed.
fn get_gpu_info(suffix: SizeSuffix) -> Option<String> {
	if let Ok(gpu_name) = readfile("/sys/class/kgsl/kgsl-3d0/gpu_model") {
		let gpu_name = gpu_name.to_lossy_string();

		if gpu_name.is_empty() {
			return None;
		}

		if let Ok(gpu_clock) = readfile("/sys/class/kgsl/kgsl-3d0/gpuclk") {
			return Some(match suffix {
				SizeSuffix::Giga => format! {
					"{} @ {:.02} GHz", gpu_name, gpu_clock.parse::<f64>().map(|res| res / 1_000_000_000.0).ok()?
				},
				SizeSuffix::Mega => format! {
					"{} @ {:.02} MHz", gpu_name, gpu_clock.parse::<f64>().map(|res| res / 1_000_000.0).ok()?
				},
			});
		}

		return Some(gpu_name);
	}

	for device in fs::read_dir("/sys/class/hwmon/").ok()? {
		let device = device.ok()?.path();

		if let Ok(device_name) = readfile(device.join("name")) {
			let device_name = device_name.to_lossy_string();

			if device_name.to_lowercase().contains("gpu") {
				if let Ok(gpu_clock) = readfile(device.join("freq1_input")) {
					return Some(match suffix {
						SizeSuffix::Giga => format! {
							"{} @ {:.02} GHz", device_name, gpu_clock.parse::<f64>().map(|res| res / 1_000_000_000.0).ok()?
						},
						SizeSuffix::Mega => format! {
							"{} @ {:.02} MHz", device_name, gpu_clock.parse::<f64>().map(|res| res / 1_000_000.0).ok()?
						},
					});
				}

				return Some(device_name);
			}
		}
	}

	None
}

// Get battery status and health information.
fn get_battery_info() -> Option<String> {
	let battery = if matches! { fs::exists("/sys/class/power_supply/battery"), Ok(true) } {
		Path::new("/sys/class/power_supply/battery")
	} else {
		Path::new("/sys/class/power_supply/BAT0")
	};

	// Current battery state (Discharging, Full...).
	let status = readfile(battery.join("status")).ok()?.to_lossy_string();

	// Current battery charge level.
	let capacity = readfile(battery.join("capacity")).ok()?.to_lossy_string();

	// Battery charge / discarge current.
	let current = readfile(battery.join("current_now")).ok()?.parse::<f64>().map(|res| res / 1_000_000.0).ok()?;

	// Battery charge / discarge voltage.
	let voltage = readfile(battery.join("voltage_now")).ok()?.parse::<f64>().map(|res| res / 1_000_000.0).ok()?;

	// Battery maximum charge level.
	let full_charge = readfile(battery.join("charge_full")).ok()?.parse::<f64>().map(|res| res * 100.0).ok()?;

	// Battery maximum charge level from the factory.
	let full_design = readfile(battery.join("charge_full_design")).ok()?.parse::<f64>().ok()?;

	Some(format! { "{} @ {:.2} W, {}% - ({:.2}% Health)", status, current * voltage, capacity, full_charge / full_design })
}

// Get CPU temperature.
fn get_cpu_temp() -> Option<f64> {
	for dirent in fs::read_dir("/sys/class/thermal/").ok()? {
		let dirent = dirent.ok()?;

		// Skip checking `cooling_device*`.
		if dirent.file_name().to_string_lossy().starts_with("cooling_device") {
			continue;
		}

		let thermalzone = readfile(dirent.path().join("type")).ok()?.to_lossy_string().to_lowercase();

		for tag in ["cpu", "acpitz", "coretemp"] {
			if thermalzone.starts_with(tag) {
				return readfile(dirent.path().join("temp")).ok()?.parse::<f64>().map(|res| res / 1000.0).ok();
			}
		}
	}

	None
}

// Get GPU temperature.
fn get_gpu_temp() -> Option<f64> {
	if let Ok(gputemp) = readfile("/sys/class/kgsl/kgsl-3d0/temp") {
		return gputemp.parse::<f64>().map(|res| res / 1000.0).ok();
	}

	for dirent in fs::read_dir("/sys/class/hwmon/").ok()? {
		let device = dirent.ok()?.path();

		if let Ok(device_name) = readfile(device.join("name")) {
			if device_name.to_lossy_string().to_lowercase().contains("gpu") {
				return readfile(device.join("temp1_input")).ok()?.parse::<f64>().map(|res| res / 1000.0).ok();
			}
		}
	}

	None
}

// Get battery temperature.
fn get_battery_temp() -> Option<f64> {
	if let Ok(battery_temp) = readfile("/sys/class/power_supply/battery/temp").map_err(|_| readfile("/sys/class/power_supply/BAT0/temp")) {
		return battery_temp.parse::<f64>().map(|res| res / 10.0).ok();
	}

	for dirent in fs::read_dir("/sys/class/thermal/").ok()? {
		let dirent = dirent.ok()?;

		// Skip checking `cooling_device*`.
		if dirent.file_name().to_string_lossy().starts_with("cooling_device") {
			continue;
		}

		if readfile(dirent.path().join("type")).ok()?.to_lossy_string().starts_with("battery") {
			return readfile(dirent.path().join("temp")).ok()?.parse::<f64>().map(|res| res / 1000.0).ok();
		}
	}

	None
}

// Get GPU VRAM information.
fn get_gpu_vram(suffix: SizeSuffix) -> Option<(String, f64)> {
	for dirent in fs::read_dir("/sys/bus/pci/devices/").ok()? {
		let device = dirent.ok()?.path();

		// Check for `PCI_BASE_CLASS_DISPLAY`.
		if readfile(device.join("class")).ok()?.to_lossy_string().starts_with("0x03") {
			let (total, used, suffix) = match suffix {
				SizeSuffix::Giga => (
					readfile(device.join("mem_info_vram_total")).ok()?.parse::<f64>().ok()? / 1_073_741_824.0,
					readfile(device.join("mem_info_vram_used")).ok()?.parse::<f64>().ok()? / 1_073_741_824.0,
					"GiB"
				),
				SizeSuffix::Mega => (
					readfile(device.join("mem_info_vram_total")).ok()?.parse::<f64>().ok()? / 1_048_576.0,
					readfile(device.join("mem_info_vram_used")).ok()?.parse::<f64>().ok()? / 1_048_576.0,
					"MiB"
				),
			};

			return Some((format! { "{:.02} / {:.02} {}", used, total, suffix }, if total == 0.0 { 0.0 } else { (used * 100.0) / total }));
		}
	}

	None
}

// Get total memory and used memory percentage.
#[expect(clippy::cast_precision_loss)]
fn get_memory_info(total: u64, free: u64, mem_unit: u64, suffix: SizeSuffix) -> Option<(String, f64)> {
	let (total, used, suffix) = match suffix {
		SizeSuffix::Giga => {
			let total = total.checked_div(mem_unit)? as f64 / 1_073_741_824.0;
			(total, total - (free.checked_div(mem_unit)? as f64 / 1_073_741_824.0), "GiB")
		},
		SizeSuffix::Mega => {
			let total = total.checked_div(mem_unit)? as f64 / 1_048_576.0;
			(total, total - (free.checked_div(mem_unit)? as f64 / 1_048_576.0), "MiB")
		},
	};

	Some((format! { "{:.02} / {:.02} {}", used, total, suffix }, if total == 0.0 { 0.0 } else { (used * 100.0) / total }))
}

fn get_uptime(system_uptime: i64) -> Option<String> {
	// System uptime in days.
	let days = system_uptime.checked_div(86400).map(|res| format! {
		"{} {}", res, if res == 1 { "day" } else { "days" }
	})?;

	// System uptime expressed in relation to the current day.
	let hours = system_uptime.checked_div(3600)?.checked_rem(24).map(|res| format! {
		"{} {}", res, if res == 1 { "hour" } else { "hours" }
	})?;

	// System uptime expressed in relation to the current hour.
	let minutes = system_uptime.checked_div(60)?.checked_rem(60).map(|res| format! {
		"{} {}", res, if res == 1 { "minute" } else { "minutes" }
	})?;

	// System uptime expressed in relation to the current minute.
	let seconds = system_uptime.checked_rem(60).map(|res| format! {
		"{} {}", res, if res == 1 { "second" } else { "seconds" }
	})?;

	Some(format! { "{}, {}, {}, {}", days, hours, minutes, seconds })
}

// Return progress bar as a String.
fn usage_bar(val: f64) -> String {
	let (val, mut output) = (val / 5.0, String::with_capacity(20));

	(0..20).for_each(|number| if f64::from(number) < val {
		output.push('■');
	} else {
		output.push('-');
	});

	format! { "[ {} ] ", output }
}

// Run the required `common::sys::*` functions for retrieving device info.
fn get_deviceinfo() -> common::Result<(SysInfo, UtsName, UserInfo)> {
	let sysinfo = sysinfo().map_err(|error| error! {
		ExitCode::Failure, "sysinfo: fatal error: Failed to retrieve system information: {}", error
	})?;

	let uname = uname().map_err(|error| error! {
		ExitCode::Failure, "sysinfo: fatal error: Failed to retrieve device information: {}", error
	})?;

	let userinfo = userinfo(getuid()).map_err(|error| error! {
		ExitCode::Failure, "sysinfo: fatal error: Failed to retrieve user information: {}", error
	})?;

	Ok((sysinfo, uname, userinfo))
}

#[doc(hidden)]
#[inline]
pub fn generate_output(options: Options) -> common::Result<()> {
	let ((sysinfo, uname, userinfo), mut output, suffix) = (get_deviceinfo()?, Vec::with_capacity(30), options.suffix.unwrap_or(SizeSuffix::Giga));

	macro_rules! output {
		($header: expr, $($var: tt)*) => {
			let space = 10_usize.checked_sub($header.len()).ok_or_else(|| error! {
				OVERFLOW_ERR_CODE, "sysinfo: fatal error: Integer overflow occured while generating '{}' output", $header
			})?;

			output.push(format! { " {}{}{}", $header, " ".repeat(space), format_args! { $($var)* } });
		};
	}

	output.push(format! { " {}@{}\n ------------------", userinfo.username(), uname.hostname().to_string_lossy() });

	if let Some((device, os)) = get_device_info() {
		output! { "Device", "{}", device };
		output! { "OS", "{}", os };
	}

	output! { "Kernel", "{} {}", uname.os_name().to_string_lossy(), uname.os_version().to_string_lossy() };

	if let Some(init) = get_init() {
		output! { "Init", "{}", init };
	}

	output! { "Procs", "{} processes", sysinfo.active_processes() };

	if let Some(packages) = get_package_info() {
		output! { "Packages", "{}", packages };
	}

	output! { "Shell", "{}", userinfo.shell() };

	if let Some(locale) = get_locale() {
		output! { "Locale", "{}", locale };
	}

	if let Some(desktop) = get_desktop() {
		output! { "Desktop", "{}", desktop };
	}

	output! { "Load Avg", "{}", sysinfo.load_average().into_iter().fold(String::new(), |mut res, load| {
		#[expect(clippy::cast_precision_loss)]
		let _ = write! { res, "{:.02} ", load as f64 / 65536.0 };
		res
	}) };

	if let Some(cpu) = get_cpu_info(suffix) {
		output! { "CPU", "{}", cpu };
	}

	if let Some(gpu) = get_gpu_info(suffix) {
		output! { "GPU", "{}", gpu };
	}

	if let Some(battery) = get_battery_info() {
		output! { "Battery", "{}", battery };
	}

	if let Some(temp) = get_cpu_temp() {
		output! { "CPU Temp", "{}{:.02} °C", usage_bar(temp), temp };
	}

	if let Some(temp) = get_gpu_temp() {
		output! { "GPU Temp", "{}{:.02} °C", usage_bar(temp), temp };
	}

	if let Some(temp) = get_battery_temp() {
		output! { "BAT Temp", "{}{:.02} °C", usage_bar(temp), temp };
	}

	if let Some((vram, vram_used)) = get_gpu_vram(suffix) {
		output! { "VRAM", "{}{}", usage_bar(vram_used), vram };
	}

	if let Some((ram, ram_used)) = get_memory_info(sysinfo.total_ram(), sysinfo.free_ram(), sysinfo.mem_unit(), suffix) {
		output! { "RAM", "{}{}", usage_bar(ram_used), ram };
	}

	if let Some((swap, swap_used)) = get_memory_info(sysinfo.total_swap(), sysinfo.free_swap(), sysinfo.mem_unit(), suffix) {
		output! { "Swap", "{}{}", usage_bar(swap_used), swap };
	}

	if let Some(uptime) = get_uptime(sysinfo.uptime()) {
		output! { "Uptime", "{}", uptime };
	}

	if output.is_empty() {
		ret! { ExitCode::Failure, "sysinfo: fatal error: Failed to retrieve any system information" };
	}

	stdoutln! { "\n{}\n", output.join("\n") };
	Ok(())
}
