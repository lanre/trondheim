// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod trondheim;

use common::cli::getopt;
use common::cli::HasArg;
use common::cli::version;
use common::error::ExitCode;
use common::error;
use common::ret;
use common::stdoutln;

use std::env;
use std::ffi::OsStr;
use std::ffi::OsString;
use std::mem;

use trondheim::create_symlink;

#[doc(hidden)]
pub const HELP: &str =
"trondheim - Execute a built-in PROGRAM with the given ARGUMENTs.

Usage:
    trondheim -- PROGRAM [ARGUMENT...]
    trondheim [OPTION <DIRECTORY>]
    PROGRAM [ARGUMENT...]

Options:
    -h, --help
          Display this help message and exit.

    -i, --install <DIRECTORY>
          Create symbolic links of all built-in PROGRAMs in DIRECTORY.

    -l, --list
          List all available built-in PROGRAMs.

    -V, --version
          Display version information and exit.

    -v, --verbose
          Show what is being done. Errors will always be printed to the standard
          error stream, even when this option is not specified.

Create links to trondheim for each built-in PROGRAM, and trondheim will act like
whatever it was invoked as. Use `trondheim -- PROGRAM --help` to learn about any
available built-in PROGRAM.

You can report bugs by opening a new issue at <https://codeberg.org/lanre/trondheim/>
or by sending an email to <koladeolanrewaju@tutanota.com>.";

/// List of built-in programs.
const APPLETS: &[&str] = &[
	#[cfg(feature = "athena")]
	"athena",

	#[cfg(feature = "hyperion")]
	"hyperion",

	#[cfg(feature = "mkcp")]
	"mkcp",

	#[cfg(feature = "mkmv")]
	"mkmv",

	#[cfg(feature = "mktouch")]
	"mktouch",

	#[cfg(feature = "sysinfo")]
	"sysinfo",
];

#[doc(hidden)]
#[inline]
pub fn try_main<C: AsRef<OsStr>>(current_exe: C, user_input: Vec<OsString>) -> common::Result<()> {
	let current_exe = current_exe.as_ref().to_string_lossy();

	if current_exe == "trondheim" {
		return trondheim(user_input);
	}

	#[cfg(feature = "athena")]
	if current_exe == "athena" {
		return athena::try_main(user_input);
	}

	#[cfg(feature = "hyperion")]
	if current_exe == "hyperion" {
		return hyperion::try_main(user_input);
	}

	#[cfg(feature = "mkcp")]
	if current_exe == "mkcp" {
		return mkcp::try_main(user_input);
	}

	#[cfg(feature = "mkmv")]
	if current_exe == "mkmv" {
		return mkmv::try_main(user_input);
	}

	#[cfg(feature = "mktouch")]
	if current_exe == "mktouch" {
		return mktouch::try_main(user_input);
	}

	#[cfg(feature = "sysinfo")]
	if current_exe == "sysinfo" {
		return sysinfo::try_main(user_input);
	}

	ret! { ExitCode::Failure, "trondheim: `{}` is not a built in program", current_exe };
}

/// Struct used to process supported options in `trondheim`.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
#[doc(hidden)]
pub struct Options<'trondheim> {
	/// `-i` | `--install`.
	pub install: &'trondheim str,

	/// `-v` | `--verbose`.
	pub verbose: bool,
}

#[doc(hidden)]
#[inline]
pub fn trondheim(user_input: Vec<OsString>) -> common::Result<()> {
	const OPTIONS: &[(&str, &str, HasArg)] = &[
		( "--help",    "-h", HasArg::False ),
		( "--install", "-i", HasArg::True  ),
		( "--list",    "-l", HasArg::False ),
		( "--verbose", "-v", HasArg::False ),
		( "--version", "-V", HasArg::False ),
	];

	let ((options, arguments), mut install, mut trondheim) = (getopt(user_input.clone(), OPTIONS, "trondheim")?, String::new(), Options::default());

	// Handles main program `trondheim` and built-in programs run directly i.e `trondheim -- mkcp`.
	for (option, mut argument) in options {
		match option.as_str() {
			"--help" | "-h" => {
				stdoutln! { "{}", HELP };
				return Ok(());
			},
			"--install" | "-i" if !install.is_empty() => ret! {
				ExitCode::Failure, "trondheim: `{}` cannot be specified multiple times", option
			},
			"--install" | "-i" => {
				install = mem::take(&mut argument);
			},
			"--list" | "-l" => {
				stdoutln! { "Built-in programs ({}):\n{}", APPLETS.len(), APPLETS.join(", ") };
				return Ok(());
			},
			"--verbose" | "-v" => {
				trondheim.verbose = true;
			},
			"--version" | "-V" => {
				stdoutln! { "{}", version("trondheim", env! { "CARGO_PKG_VERSION" }) };
				return Ok(());
			},
			_ => ret! {
				ExitCode::Failure, "trondheim: fatal error: Unhandled option `{}`", option
			},
		}
	}

	// `--install` was specified.
	if !install.is_empty() {
		trondheim.install = &install;
		return create_symlink(trondheim, APPLETS);
	}

	match arguments.first() {
		None => ret! {
			ExitCode::Failure, "trondheim: missing command line arguments"
		},
		// Prevent built-in program recursion i.e `trondheim -- trondheim`.
		Some(current_exe) if current_exe == "trondheim" => ret! {
			ExitCode::Failure, "trondheim: `trondheim` is not a built in program"
		},
		Some(current_exe) => {
			// Since `arguments` is lossy, it might not completely match with `user_input`.
			let index = user_input.iter().position(|arg| arg == current_exe.as_str()).ok_or_else(|| error! {
				ExitCode::Failure, "trondheim: `{}` is not a built in program", current_exe
			})?;

			let user_input = user_input.get(index..).ok_or_else(|| error! {
				ExitCode::Failure, "trondheim: fatal error: Failed to get user input from command line"
			})?;

			// Run built in programs.
			try_main(current_exe, user_input.to_vec())
		},
	}
}
