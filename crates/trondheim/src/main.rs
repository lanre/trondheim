// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::stderrln;

use std::env::args_os;
use std::ffi::OsString;
use std::path::Path;
use std::process::ExitCode;
use std::process::Termination;

use trondheim::try_main;

fn main() -> ExitCode {
	let user_input = args_os().collect::<Vec<OsString>>();

	let current_exe = match user_input.first() {
		None => {
			stderrln! { "trondheim: fatal error: Failed to get program name from command line" };
			return ExitCode::FAILURE;
		},
		Some(bin) => {
			Path::new(bin).file_name().unwrap_or(bin).to_owned()
		},
	};

	try_main(current_exe, user_input).map_or_else(|error| error.report(), |()| ExitCode::SUCCESS)
}
