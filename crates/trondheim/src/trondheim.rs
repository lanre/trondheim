// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::constants::ENOENT;
use common::error::ExitCode;
use common::error;
use common::fs::IsSameFile;
use common::ret;
use common::stdoutln;

use crate::Options;

use std::env;
use std::fs;
use std::os::unix::fs::symlink;
use std::path::Path;

#[doc(hidden)]
#[inline]
pub fn create_symlink(trondheim: Options, applets: &[&str]) -> common::Result<()> {
	let dir = Path::new(trondheim.install);

	if applets.is_empty() {
		ret! { ExitCode::Failure, "trondheim: fatal error: This binary was compiled without built-in programs" };
	}

	if let Err(error) = fs::create_dir_all(dir) {
		ret! { ExitCode::Failure, "trondheim: failed to create target directory '{}': {}", dir.display(), error };
	}

	let source = env::current_exe().map_err(|error| error! {
		ExitCode::Failure, "trondheim: failed to get program name from command line: {}", error
	})?;

	// Set install file path.
	let dest = dir.join("trondheim");

	// Only copy binary if the source and destination are not the same.
	if matches! { source.is_samefile(&dest), Ok(false) } {
		if let Err(error) = fs::copy(&source, &dest) {
			ret! { ExitCode::Failure, "trondheim: failed to copy '{}' to '{}': {}", source.display(), dir.display(), error };
		}

		if trondheim.verbose {
			stdoutln! { "'{}' -> '{}'", source.display(), dest.display() };
		}
	}

	// Create built-in program symlinks in the destination directory.
	for app in applets {
		let link_dst = dir.join(app);

		// Try to remove file if it already exists.
		if let Err(error) = fs::remove_file(&link_dst) {
			if error.raw_os_error() != Some(ENOENT) {
				ret! { ExitCode::Failure, "trondheim: failed to create symbolic link '{}' to '{}': {}", link_dst.display(), dest.display(), error };
			}
		}

		if let Err(error) = symlink("trondheim", &link_dst) {
			ret! { ExitCode::Failure, "trondheim: failed to create symbolic link '{}' to '{}': {}", link_dst.display(), dest.display(), error };
		}

		if trondheim.verbose {
			stdoutln! { "'{}' -> '{}'", dest.display(), link_dst.display() };
		}
	}

	Ok(())
}
