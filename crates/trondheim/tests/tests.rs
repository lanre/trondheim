// Copyright (C) 2024-2025, Kolade Ayomide Olanrewaju
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use common::cli::version;
use common::constants::EACCES;
use common::constants::ENOENT;
use common::constants::S_IFLNK;
use common::constants::S_IFMT;
use common::constants::S_IFREG;
use common::error::Error;
use common::fs::IsSameFile;
use common::fs::is_samefile;
use common::testbin;

use std::ffi::OsStr;
use std::fs;
use std::os::unix::ffi::OsStrExt;
use std::os::unix::fs::PermissionsExt;
use std::os::unix::fs::symlink;
use std::path::Path;
use std::thread;
use std::time::Duration;

// Trondheim test binary.
const BIN: &str = env! { "CARGO_BIN_EXE_trondheim" };

// Trondheim build dir.
const TESTDIR: &str = env! { "CARGO_MANIFEST_DIR" };

// Trondheim version.
const VERSION: &str = env! { "CARGO_PKG_VERSION" };

#[test]
fn test_help() {
	// trondheim --help
	let run = testbin! { BIN, "--help" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", trondheim::HELP }
	};
}

#[test]
fn test_help_short_opt() {
	// trondheim --help
	let run = testbin! { BIN, "-h" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", trondheim::HELP }
	};
}

#[test]
fn test_version() {
	// trondheim --version
	let run = testbin! { BIN, "--version" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", version("trondheim", VERSION) }
	};
}

#[test]
fn test_version_short_opt() {
	// trondheim --version
	let run = testbin! { BIN, "-V" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", version("trondheim", VERSION) }
	};
}

#[test]
fn test_help_positional() {
	// trondheim -- --help
	let run = testbin! { BIN, "-- --help" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `--help` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_help_positional_ext() {
	// trondheim -- --help -- dsd
	let run = testbin! { BIN, "-- --help -- dsd" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `--help` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_verbose_only() {
	// trondheim --verbose
	let run = testbin! { BIN, "--verbose" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: missing command line arguments\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_verbose_only_short_opts() {
	// trondheim --verbose
	let run = testbin! { BIN, "-v" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: missing command line arguments\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_invalid_utf8() {
	// trondheim $'\xFF'
	let run = testbin! { BIN, "{}", Path::new(OsStr::from_bytes(&[255])).display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `�` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_no_command() {
	// trondheim
	let run = testbin! { BIN };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: missing command line arguments\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_no_command_positional() {
	// trondheim --
	let run = testbin! { BIN, "--" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: missing command line arguments\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_no_command_positional_ext() {
	// trondheim -- --
	let run = testbin! { BIN, "-- --" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `--` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_no_command_positional_hypen() {
	// trondheim -- -
	let run = testbin! { BIN, "-- -" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `-` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_invalid_short_opts() {
	// trondheim -esd
	let run = testbin! { BIN, "-esd" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: invalid option -- `e`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_invalid_short_opts_positional() {
	// trondheim -- -esd
	let run = testbin! { BIN, "-- -esd" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `-esd` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_unrecognized_option() {
	// trondheim --esd
	let run = testbin! { BIN, "--esd" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: unrecognized option `--esd`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_unrecognized_option_positional() {
	// trondheim --esd -- --fesss
	let run = testbin! { BIN, "--esd -- --fesss" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: unrecognized option `--esd`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_direct_false_applet() {
	// trondheim dogfood
	let run = testbin! { BIN, "dogfood" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `dogfood` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_direct_false_applet_() {
	// trondheim dogfood --install
	let run = testbin! { BIN, "dogfood --install" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: an argument is required for `--install` but none was supplied\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_direct_false_applet_ext() {
	// trondheim dogfood -- --install
	let run = testbin! { BIN, "dogfood -- --install" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `dogfood` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_direct_false_applet_positional() {
	// trondheim -- dogfood --install
	let run = testbin! { BIN, "-- dogfood --install" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `dogfood` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_direct_false_applet_positional_ext() {
	// trondheim dogfood -- --esd
	let run = testbin! { BIN, "dogfood -- --esd" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `dogfood` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_direct_applet_recursive() {
	// trondheim trondheim
	let run = testbin! { BIN, "trondheim" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `trondheim` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_direct_applet_recursive_positional() {
	// trondheim -- trondheim
	let run = testbin! { BIN, "-- trondheim" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `trondheim` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_direct_applet() {
	// trondheim mkmv --help
	let run = testbin! { BIN, "mkmv --help" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", trondheim::HELP }
	};
}

#[test]
fn test_direct_applet_positional() {
	// trondheim -- mkmv --help
	let run = testbin! { BIN, "-- mkmv --help" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		format! { "{}\n", mkmv::HELP }
	};
}

#[test]
fn test_direct_applet_positional_ext() {
	// trondheim -- mkmv -- --help
	let run = testbin! { BIN, "-- mkmv -- --help" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mkmv: missing destination file operand after '--help'\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_direct_applet_error() {
	// trondheim mkmv --does-not-exist
	let run = testbin! { BIN, "mkmv --does-not-exist" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: unrecognized option `--does-not-exist`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_direct_applet_error_ext() {
	// trondheim -- mkmv --does-not-exist
	let run = testbin! { BIN, "-- mkmv --does-not-exist" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mkmv: unrecognized option `--does-not-exist`\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_applet() {
	// trondheim mkmv
	let run = testbin! { BIN, "mkmv" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("mkmv: missing file operand\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

// Test fake symlinks to `trondheim`
#[test]
fn test_false_applet() {
	let dest = &Path::new(TESTDIR).join("fake_applet");
	drop((fs::remove_file(dest), symlink(BIN, dest).unwrap()));

	// fake_applet --help
	let run = testbin! { dest, "--help" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `fake_applet` is not a built in program\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	fs::remove_file(dest).unwrap();
}

#[test]
fn test_install() {
	let dir = &Path::new(TESTDIR).join("trondheim_install_test");
	drop(fs::remove_dir_all(dir));

	// trondheim --install {dir}
	let run = testbin! { BIN, "--install {}", dir.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dir.join("mkcp").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("mkmv").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("trondheim").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFREG
	};
	assert_eq! {
		dir.join("trondheim").is_samefile(dir.join("mkcp")).map_err(Error::from),
		Ok(true)
	};

	fs::remove_dir_all(dir).unwrap();
}

#[test]
fn test_install_ext() {
	let dir = &Path::new(TESTDIR).join("trondheim_install_test_ext");
	drop(fs::remove_dir_all(dir));

	// trondheim --install {dir} --verbose
	let run = testbin! { BIN, "--install {} --verbose", dir.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert! {
		String::from_utf8(run.stdout).unwrap().starts_with(
			&format! { "'{}' -> '{}'", BIN, dir.join("trondheim").display() })
	};

	assert_eq! {
		dir.join("mkcp").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("mkmv").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("trondheim").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFREG
	};
	assert_eq! {
		is_samefile(dir.join("trondheim"), dir.join("mkcp")).map_err(Error::from),
		Ok(true)
	};

	fs::remove_dir_all(dir).unwrap();
}

#[test]
fn test_install_verbose_error() {
	let dir = &Path::new(TESTDIR).join("trondheim_install_verbose_error");
	drop(fs::remove_dir_all(dir));

	// trondheim --install --verbose {dir}
	let run = testbin! { BIN, "--install --verbose {}", dir.display() };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: an argument is required for `--install` but none was supplied\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		fs::symlink_metadata(dir).unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
}

#[test]
fn test_install_shortopts() {
	let dir = &Path::new(TESTDIR).join("trondheim_test_install_shortopts");
	drop(fs::remove_dir_all(dir));

	// trnondheim --verbose --install {dir}
	let run = testbin! { BIN, "-vi {}", dir.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert! {
		String::from_utf8(run.stdout).unwrap().starts_with(
			&format! { "'{}' -> '{}'", BIN, dir.join("trondheim").display() })
	};

	assert_eq! {
		dir.join("mkcp").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("mkmv").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("trondheim").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFREG
	};
	assert_eq! {
		is_samefile(dir.join("trondheim"), dir.join("mkcp")).map_err(Error::from),
		Ok(true)
	};

	fs::remove_dir_all(dir).unwrap();
}

#[test]
fn test_install_shortopts_ext() {
	let dir = &Path::new(TESTDIR).join("v");
	drop(fs::remove_dir_all(dir));

	// trondheim --install v
	let run = testbin! { BIN, "-iv" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dir.join("mkcp").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("mkmv").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("trondheim").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFREG
	};
	assert_eq! {
		dir.join("trondheim").is_samefile(dir.join("mkcp")).map_err(Error::from),
		Ok(true)
	};

	fs::remove_dir_all(dir).unwrap();
}

#[test]
fn test_install_positional() {
	// trondheim --install -- --install
	let run = testbin! { BIN, "--install -- --install" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: an argument is required for `--install` but none was supplied\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		fs::symlink_metadata("--install").unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
}

#[test]
fn test_install_positional_ext() {
	let dir = &Path::new(TESTDIR).join("--help");
	drop(fs::remove_dir_all(dir));

	// trondheim --install=--help --
	let run  = testbin! { BIN, "--install=--help --" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dir.join("mkcp").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("mkmv").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("trondheim").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFREG
	};
	assert_eq! {
		dir.join("trondheim").is_samefile(dir.join("mkcp")).map_err(Error::from),
		Ok(true)
	};

	fs::remove_dir_all(dir).unwrap();
}

#[test]
fn test_install_overwrite() {
	let dir = &Path::new(TESTDIR).join("trondheim_lib_test2");
	drop(fs::remove_dir_all(dir));

	/////////////////////////////////////////////////////////////////////////////////
	// trondheim --install {dir}
	/////////////////////////////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--install {}", dir.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dir.join("mkcp").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("mkmv").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("trondheim").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFREG
	};
	assert_eq! {
		is_samefile(dir.join("trondheim"), dir.join("mkcp")).map_err(Error::from),
		Ok(true)
	};

	/////////////////////////////////////////////////////////////////////////////////
	// trondheim --install {dir}
	/////////////////////////////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--install {}", dir.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dir.join("mkcp").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("mkmv").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("trondheim").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFREG
	};
	assert_eq! {
		dir.join("trondheim").is_samefile(dir.join("mkcp")).map_err(Error::from),
		Ok(true)
	};

	fs::remove_dir_all(dir).unwrap();
}

#[test]
fn test_install_no_destination() {
	// trondheim --install
	let run = testbin! { BIN, "--install" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: an argument is required for `--install` but none was supplied\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}

#[test]
fn test_install_no_destination_positional() {
	// trondheim --install --
	let run = testbin! { BIN, "--install --" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: an argument is required for `--install` but none was supplied\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		fs::symlink_metadata("--").unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
}

// NOTE: Trondheim will handle all its options before running a built in program.
// So this should run successfully even though `test_trondheim_new2` is not
// a built in a program.
#[test]
fn test_install_multiple_destination() {
	let dir = &Path::new(TESTDIR).join("test_install_multiple_destination");
	drop(fs::remove_dir_all(dir));

	// trondheim --install {dir} test_trondheim_new2
	let run = testbin! { BIN, "--install {} test_trondheim_new2", dir.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dir.join("mkcp").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("mkmv").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("trondheim").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFREG
	};
	assert_eq! {
		dir.join("trondheim").is_samefile(dir.join("mkcp")).map_err(Error::from),
		Ok(true)
	};

	fs::remove_dir_all(dir).unwrap();
}

#[test]
fn test_install_multiple_destination_ext() {
	// trondheim --install test_trondheim_new --install test_trondheim_new2
	let run = testbin! { BIN, "--install test_trondheim_new --install test_trondheim_new2" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: `--install` cannot be specified multiple times\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		fs::symlink_metadata("test_trondheim_new").unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
}

#[test]
fn test_install_multiple_destination_positional() {
	// trondheim --install -- test_dest1 test_dest2
	let run = testbin! { BIN, "--install -- test_dest1 test_dest2" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: an argument is required for `--install` but none was supplied\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		fs::symlink_metadata("test_dest1").unwrap_err().raw_os_error(),
		Some(ENOENT)
	};
}

#[test]
fn test_install_missing_permissions() {
	// trondheim --install /root/trondheim_install_test
	let run = testbin! { BIN, "--install /root/trondheim_install_test" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: failed to create target directory '/root/trondheim_install_test': Permission denied (os error 13)\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		fs::symlink_metadata("/root/trondheim_install_test").unwrap_err().raw_os_error(),
		Some(EACCES)
	};
}

#[test]
fn test_install_missing_permissions_ext() {
	// trondheim --install=/root/trondheim_install_test
	let run = testbin! { BIN, "--install=/root/trondheim_install_test" };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: failed to create target directory '/root/trondheim_install_test': Permission denied (os error 13)\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		fs::symlink_metadata("/root/trondheim_install_test").unwrap_err().raw_os_error(),
		Some(EACCES)
	};
}

#[test]
fn test_install_missing_permissions_positional() {
	// trondheim --install -- /root/trondheim_install_test
	let run = testbin! { BIN, "--install -- /root/trondheim_install_test" };

	assert_eq! {
		run.status.code(),
		Some(2)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: an argument is required for `--install` but none was supplied\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		fs::symlink_metadata("/root/trondheim_install_test").unwrap_err().raw_os_error(),
		Some(EACCES)
	};
}

const OVERLOAD: &str = "trondheim_stack/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new/new\
/new/new/new/new/new/new/new/new/new/new/new";

#[test]
fn test_stack_overflow() {
	let dir = &Path::new(TESTDIR).join(OVERLOAD);
	drop(fs::remove_dir_all(dir));

	/////////////////////////////////////////////////////////////////////////////////
	// trondheim --install {dir} --verbose
	/////////////////////////////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--install {} --verbose", dir.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert! {
		String::from_utf8(run.stdout).unwrap().starts_with(
			&format! { "'{}' -> '{}'", BIN, dir.join("trondheim").display() })
	};

	assert_eq! {
		dir.join("mkcp").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("mkmv").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("trondheim").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFREG
	};
	assert_eq! {
		is_samefile(dir.join("trondheim"), dir.join("mkcp")).map_err(Error::from),
		Ok(true)
	};

	/////////////////////////////////////////////////////////////////////////////////
	// trondheim --install {dir}
	/////////////////////////////////////////////////////////////////////////////////
	let run = testbin! { BIN, "--install {}", dir.display() };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};

	assert_eq! {
		dir.join("mkcp").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("mkmv").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFLNK
	};
	assert_eq! {
		dir.join("trondheim").symlink_metadata().unwrap().permissions().mode() & S_IFMT,
		S_IFREG
	};
	assert_eq! {
		is_samefile(dir.join("trondheim"), dir.join("mkcp")).map_err(Error::from),
		Ok(true)
	};

	fs::remove_dir_all("trondheim_stack").unwrap();
}

#[test]
fn test_conditional_compilation() {
	thread::sleep(Duration::from_secs(2));

	// cargo run --no-default-features --feature mkcp --package trondheim -- --list
	let run = testbin! { "cargo", "--quiet run --no-default-features --features mkcp --package trondheim -- --list" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::from("Built-in programs (1):\nmkcp\n")
	};
}

#[test]
fn test_conditional_compilation_ext() {
	thread::sleep(Duration::from_secs(2));

	// cargo run --no-default-features --feature mkcp,athena,mkmv --package trondheim -- --list
	let run = testbin! { "cargo", "--quiet run --no-default-features --features mkcp,athena,mkmv --package trondheim -- --list" };

	assert_eq! {
		run.status.code(),
		Some(0)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::new()
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::from("Built-in programs (3):\nathena, mkcp, mkmv\n")
	};
}

#[test]
fn test_conditional_compilation_error() {
	thread::sleep(Duration::from_secs(2));

	let dir = &Path::new(TESTDIR).join("test_conditional_compilation_error");
	let _   = fs::remove_dir_all(dir);

	// cargo run --no-default-features --feature mkcp --package trondheim -- --list
	let run = testbin! { "cargo", "--quiet run --no-default-features --package trondheim -- --install {}", dir.display() };

	assert_eq! {
		run.status.code(),
		Some(1)
	};
	assert_eq! {
		String::from_utf8(run.stderr).unwrap(),
		String::from("trondheim: fatal error: This binary was compiled without built-in programs\n")
	};
	assert_eq! {
		String::from_utf8(run.stdout).unwrap(),
		String::new()
	};
}
