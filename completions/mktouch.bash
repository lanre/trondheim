_mktouch() {
	if [[ "$cur" == "--" ]]; then
		_filedir
	fi

	local OPTS="
		-a
		-c
		-h
		-m
		-r
		-r=
		-V
		--access-time
		--help
		--mod-time
		--no-create
		--reference
		--reference=
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	case "$prev" in
		-h | --help | -V | --version)
			return
		;;
		-r | --reference)
			_filedir
		;;
		*)
			# Suggest files before options.
			_filedir

			if [[ "$cur" == -* ]]; then
				COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
			fi
		;;
	esac
}

complete -F _mktouch mktouch
