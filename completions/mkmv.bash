_mkmv() {
	if [[ "$cur" == "--" ]]; then
		_filedir
	fi

	local OPTS="
		-b
		-f
		-h
		-i
		-N
		-n
		-S
		-S=
		-T
		-u
		-V
		-v
		--backup
		--force
		--help
		--interactive
		--no-clobber
		--no-copy
		--no-target-directory
		--suffix
		--suffix=
		--update
		--verbose
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	case "$prev" in
		-h | --help | -V | --version)
			return
		;;
		*)
			# Suggest files before options.
			_filedir

			if [[ "$cur" == -* ]]; then
				COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
			fi
		;;
	esac
}

complete -F _mkmv mkmv
