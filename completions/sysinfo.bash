_sysinfo() {
	local OPTS="
		-h
		-S
		-S=
		-V
		--help
		--suffix
		--suffix=
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	case "$prev" in
		-h | --help | -V | --version)
			return
		;;
		-s | -s=* | --suffix | --suffix=*)
			local SUFFIX="
				giga
				gibi
				mega
				mebi"

			COMPREPLY=($(compgen -W "${SUFFIX[*]}" -- "${cur#*=}"))
		;;
		*)
			COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
		;;
	esac
}

complete -F _sysinfo sysinfo
