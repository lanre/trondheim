_trondheim_builtin="athena hyperion mkcp mkmv mktouch sysinfo"

_athena() {
	if [[ "$cur" == "--" ]]; then
		_filedir
	fi

	local OPTS="
		-d
		-d=
		-f
		-h
		-p
		-p=
		-s
		-s=
		-S
		-V
		-v
		--discard
		--discard=
		--force
		--help
		--priority
		--priority=
		--size
		--size=
		--sparse-file
		--verbose
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	case "$prev" in
		-d | -d=* | --discard | --discard=*)
			local PRESERVE="
				once
				pages"

			COMPREPLY=($(compgen -W "${PRESERVE[*]}" -- "${cur#*=}"))
		;;
		-h | --help | -V | --version)
			return
		;;
		-p | -p=* | --priority | --priority=*)
			local PRIORITY="
				{0..9}
				32767"

			COMPREPLY=($(compgen -W "${PRIORITY[*]}" -- "${cur#*=}"))
		;;
		*)
			# Suggest files before options.
			_filedir

			if [[ "$cur" == -* ]]; then
				COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
			fi
		;;
	esac
}

_hyperion() {
	if [[ "$cur" == "--" ]]; then
		_filedir
	fi

	local OPTS="
		-a
		-d
		-d=
		-h
		-o
		-o=
		-s
		-s=
		-S
		-V
		--all
		--delay
		--delay=
		--help
		--oom-score
		--oom-score=
		--service
		--service=
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	case "$prev" in
		-d | -d=* | --delay | --delay=*)
			local DELAY="
				1000
				2000
				3000
				4000
				6000
				7000
				8000
				9000"

			COMPREPLY=($(compgen -W "${DELAY[*]}" -- "${cur#*=}"))
		;;
		-h | --help | -V | --version)
			return
		;;
		-o | -o=* | --oom-score | --oom-score=*)
			COMPREPLY=($(compgen -W "{0..9} 1000" -- ${cur#*=}))
		;;
		-s | -s=* | --service | --service=*)
			_filedir
		;;
		*)
			COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
		;;
	esac
}

_mkcp() {
	if [[ "$cur" == "--" ]]; then
		_filedir
	fi

	local OPTS="
		-A
		-b
		-F
		-f
		-h
		-i
		-l
		-n
		-p
		-p=
		-R
		-r
		-S
		-S=
		-s
		-T
		-u
		-V
		-v
		--attributes-only
		--backup
		--follow
		--force
		--help
		--interactive
		--link
		--no-clobber
		--preserve
		--preserve=
		--recursive
		--suffix
		--suffix=
		--symbolic-link
		--no-target-directory
		--update
		--verbose
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	case "$prev" in
		-h | --help | -V | --version)
			return
		;;
		-p | -p=* | --preserve | --preserve=*)
			local PRESERVE="
				all
				filetype
				ownership
				permissions
				timestamps"

			COMPREPLY=($(compgen -W "${PRESERVE[*]}" -- "${cur#*=}"))
		;;
		*)
			# Suggest files before options.
			_filedir

			if [[ "$cur" == -* ]]; then
				COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
			fi
		;;
	esac
}

_mkmv() {
	if [[ "$cur" == "--" ]]; then
		_filedir
	fi

	local OPTS="
		-b
		-f
		-h
		-i
		-N
		-n
		-S
		-S=
		-T
		-u
		-V
		-v
		--backup
		--force
		--help
		--interactive
		--no-clobber
		--no-copy
		--no-target-directory
		--suffix
		--suffix=
		--update
		--verbose
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	case "$prev" in
		-h | --help | -V | --version)
			return
		;;
		*)
			# Suggest files before options.
			_filedir

			if [[ "$cur" == -* ]]; then
				COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
			fi
		;;
	esac
}

_mktouch() {
	if [[ "$cur" == "--" ]]; then
		_filedir
	fi

	local OPTS="
		-a
		-c
		-h
		-m
		-r
		-r=
		-V
		--access-time
		--help
		--mod-time
		--no-create
		--reference
		--reference=
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	case "$prev" in
		-h | --help | -V | --version)
			return
		;;
		-r | --reference)
			_filedir
		;;
		*)
			# Suggest files before options.
			_filedir

			if [[ "$cur" == -* ]]; then
				COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
			fi
		;;
	esac
}

_sysinfo() {
	local OPTS="
		-h
		-S
		-S=
		-V
		--help
		--suffix
		--suffix=
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	case "$prev" in
		-h | --help | -V | --version)
			return
		;;
		-s | -s=* | --suffix | --suffix=*)
			local SUFFIX="
				giga
				gibi
				mega
				mebi"

			COMPREPLY=($(compgen -W "${SUFFIX[*]}" -- "${cur#*=}"))
		;;
		*)
			COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
		;;
	esac
}

_trondheim() {
	if [[ "$cur" == "--" ]]; then
		COMPREPLY=($(compgen -W "${_trondheim_builtin[*]}" -- "$cur"))
	fi

	local OPTS="
		-h
		-i
		-i=
		-l
		-V
		-v
		--help
		--install
		--install=
		--list
		--verbose
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	for ((var=1; var < $COMP_CWORD; ++var)); do
		if [[ $var -eq 1 ]]; then
			prev="${COMP_WORDS[var]}"
		fi
	done

	if [[ ${COMP_WORDS[1]} == "--" ]]; then
		prev="${COMP_WORDS[2]}"
	fi

	case "$prev" in
		athena)
			_athena
		;;
		hyperion)
			_hyperion
		;;
		mkcp)
			_mkcp
		;;
		mkmv)
			_mkmv
		;;
		mktouch)
			_mktouch
		;;
		sysinfo)
			_sysinfo
		;;
		-i | --install)
			_filedir
		;;
		-h | --help | -V | --version)
			return
		;;
		*)
			# Suggest built in programs first.
			COMPREPLY=($(compgen -W "${_trondheim_builtin[*]}" -- "$cur"))

			if [[ "$cur" == -* ]]; then
				COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
			fi
		;;
	esac
}

complete -F _trondheim trondheim
