_hyperion() {
	if [[ "$cur" == "--" ]]; then
		_filedir
	fi

	local OPTS="
		-a
		-d
		-d=
		-h
		-o
		-o=
		-s
		-s=
		-S
		-V
		--all
		--delay
		--delay=
		--help
		--oom-score
		--oom-score=
		--service
		--service=
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	case "$prev" in
		-d | -d=* | --delay | --delay=*)
			local DELAY="
				1000
				2000
				3000
				4000
				6000
				7000
				8000
				9000"

			COMPREPLY=($(compgen -W "${DELAY[*]}" -- "${cur#*=}"))
		;;
		-h | --help | -V | --version)
			return
		;;
		-o | -o=* | --oom-score | --oom-score=*)
			COMPREPLY=($(compgen -W "{0..9} 1000" -- ${cur#*=}))
		;;
		-s | -s=* | --service | --service=*)
			_filedir
		;;
		*)
			COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
		;;
	esac
}

complete -F _hyperion hyperion
