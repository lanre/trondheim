_athena() {
	if [[ "$cur" == "--" ]]; then
		_filedir
	fi

	local OPTS="
		-d
		-d=
		-f
		-h
		-p
		-p=
		-s
		-s=
		-S
		-V
		-v
		--discard
		--discard=
		--force
		--help
		--priority
		--priority=
		--size
		--size=
		--sparse-file
		--verbose
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	case "$prev" in
		-d | -d=* | --discard | --discard=*)
			local PRESERVE="
				once
				pages"

			COMPREPLY=($(compgen -W "${PRESERVE[*]}" -- "${cur#*=}"))
		;;
		-h | --help | -V | --version)
			return
		;;
		-p | -p=* | --priority | --priority=*)
			local PRIORITY="
				{0..9}
				32767"

			COMPREPLY=($(compgen -W "${PRIORITY[*]}" -- "${cur#*=}"))
		;;
		*)
			# Suggest files before options.
			_filedir

			if [[ "$cur" == -* ]]; then
				COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
			fi
		;;
	esac
}

complete -F _athena athena
