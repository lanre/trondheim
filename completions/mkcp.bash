_mkcp() {
	if [[ "$cur" == "--" ]]; then
		_filedir
	fi

	local OPTS="
		-A
		-b
		-F
		-f
		-h
		-i
		-l
		-n
		-p
		-p=
		-R
		-r
		-S
		-S=
		-s
		-T
		-u
		-V
		-v
		--attributes-only
		--backup
		--follow
		--force
		--help
		--interactive
		--link
		--no-clobber
		--preserve
		--preserve=
		--recursive
		--suffix
		--suffix=
		--symbolic-link
		--no-target-directory
		--update
		--verbose
		--version"

	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	local cur="${COMP_WORDS[COMP_CWORD]}"

	case "$prev" in
		-h | --help | -V | --version)
			return
		;;
		-p | -p=* | --preserve | --preserve=*)
			local PRESERVE="
				all
				filetype
				ownership
				permissions
				timestamps"

			COMPREPLY=($(compgen -W "${PRESERVE[*]}" -- "${cur#*=}"))
		;;
		*)
			# Suggest files before options.
			_filedir

			if [[ "$cur" == -* ]]; then
				COMPREPLY=($(compgen -W "${OPTS[*]}" -- "$cur"))
			fi
		;;
	esac
}

complete -F _mkcp mkcp
