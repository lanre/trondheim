# trondheim

trondheim is a multi-call binary that combines many useful utilities into a single executable.

## Building

To build `trondheim`, you need to have `git`, `rust` and `cargo` installed.

```shell
# Clone this repo.
$ git clone https://codeberg.org/lanre/trondheim.git --depth 1 && cd trondheim

# Build trondheim with all its built-in programs.
$ cargo build --release --bin trondheim

# Build trondheim with PROGRAMs as the only built-in programs.
$ cargo build --release --no-default-features --features <PROGRAM...> --bin trondheim

# Build only PROGRAM and build it as a standalone program.
$ cargo build --release --bin <PROGRAM>

# Build trondheim with all its built-in programs as a static binary.
$ RUSTFLAGS="-C target-feature=+crt-static" cargo build --release --bin trondheim

# Build trondheim with all its built-in programs as a static binary for Android.
$ RUSTFLAGS="-C linker=/path/to/ANDROID_NDK -C target-feature=+crt-static" cargo build --release --bin trondheim --target aarch64-linux-android
```

## Installation

After building, create symlinks to `trondheim` for each built-in program.

```shell
# Copy trondheim and create symbolic links for all its built-in programs in DESTINATION.
$ ./target/release/trondheim --install <DESTINATION>

# View trondheim help message.
$ trondheim --help

# Run a built-in PROGRAM directly.
$ trondheim -- <PROGRAM>

# List all built-in programs.
$ trondheim --list
```

Ensure that `DESTINATION` is in $PATH

## Built-in programs

| Program | Details |
| --- | --- |
**athena** | Usage: `athena OPTION... FILE` <p> Create and setup a `FILE` for paging and swapping. It will create the `FILE` if it doesn't already exist. Run `athena --help` for more details. |
**mkcp** | Usage: `mkcp [OPTION...] SOURCE... DESTINATION` <p> Copy `SOURCE` or multiple `SOURCEs` to the specified `DESTINATION`. It will create the `DESTINATION` if it doesn't already exist. Run `mkcp --help` for more details. |
**mkmv** | Usage: `mkmv [OPTION...] SOURCE... DESTINATION` <p> Move `SOURCE` or multiple `SOURCEs` to the specified `DESTINATION`. It will create the `DESTINATION` if it doesn't already exist. Run `mkmv --help` for more details. |
**mktouch** | Usage: `mktouch [OPTION...] FILE...` <p> Update the access and modification time of each FILE. It will create the `FILE` if it doesn't already exist. Run `mktouch --help` for more details. |
**sysinfo** | Usage: `sysinfo [OPTION]` <p> Displays system hardware information. Run `sysinfo --help` for more details. |

## Support

`trondheim` officially supports the following operating systems and corresponding architectures:

* Linux (`x86_64`, `aarch64`)
* Android (`aarch64`)

`trondheim` has been tested to work on these platforms and any issues or bugs discovered on these platforms will be fixed as soon as possible.

## Contributing

To contribute to `trondheim`, create a pull request. You can also report bugs or make feature requests by opening a new [ISSUE](https://codeberg.org/lanre/trondheim/issues/new) or by sending an email to 
<<koladeolanrewaju@tutanota.com>>.

## License

trondheim and all its component / built-in programs are licensed under [GPL-3.0-or-later](../master/LICENSE).

Copyright (c) 2024-2025, Kolade Ayomide Olanrewaju <<koladeolanrewaju@tutanota.com>>.
